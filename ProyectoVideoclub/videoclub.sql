-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema videoclub
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema videoclub
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `videoclub` DEFAULT CHARACTER SET utf8 ;
USE `videoclub` ;

-- -----------------------------------------------------
-- Table `videoclub`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`cliente` (
  `idcliente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  `apellidos` VARCHAR(100) NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idcliente`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`actor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`actor` (
  `idactor` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `fechanac` DATE NOT NULL,
  PRIMARY KEY (`idactor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`pelicula`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`pelicula` (
  `idpelicula` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(100) NOT NULL,
  `sinopsis` VARCHAR(1000) NOT NULL,
  `preciodia` INT NOT NULL,
  PRIMARY KEY (`idpelicula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`genero` (
  `idgenero` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idgenero`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`ejemplar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`ejemplar` (
  `idejemplar` INT NOT NULL AUTO_INCREMENT,
  `fechacompra` DATE NULL,
  `idpelicula` INT NOT NULL,
  PRIMARY KEY (`idejemplar`),
  INDEX `fk_ejemplar_pelicula1_idx` (`idpelicula` ASC),
  CONSTRAINT `fk_ejemplar_pelicula1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `videoclub`.`pelicula` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`tarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`tarifa` (
  `idtarifa` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `porcentajedto` INT NULL,
  `fechadesde` DATE NULL,
  `fechahasta` DATE NULL,
  PRIMARY KEY (`idtarifa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`alquiler`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`alquiler` (
  `idejemplar` INT NOT NULL,
  `idcliente` INT NOT NULL,
  `idtarifa` INT NOT NULL,
  `fechaalquiler` DATE NOT NULL,
  `dias` INT NOT NULL,
  `fechadevolucion` DATE NULL,
  PRIMARY KEY (`idejemplar`, `idcliente`, `idtarifa`, `fechaalquiler`),
  INDEX `fk_alquiler_cliente1_idx` (`idcliente` ASC),
  INDEX `fk_alquiler_tarifa1_idx` (`idtarifa` ASC),
  CONSTRAINT `fk_alquiler_ejemplar1`
    FOREIGN KEY (`idejemplar`)
    REFERENCES `videoclub`.`ejemplar` (`idejemplar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alquiler_cliente1`
    FOREIGN KEY (`idcliente`)
    REFERENCES `videoclub`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alquiler_tarifa1`
    FOREIGN KEY (`idtarifa`)
    REFERENCES `videoclub`.`tarifa` (`idtarifa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`pertenece`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`pertenece` (
  `idgenero` INT NOT NULL,
  `idpelicula` INT NOT NULL,
  PRIMARY KEY (`idgenero`, `idpelicula`),
  INDEX `fk_genero_has_pelicula_pelicula1_idx` (`idpelicula` ASC),
  INDEX `fk_genero_has_pelicula_genero1_idx` (`idgenero` ASC),
  CONSTRAINT `fk_genero_has_pelicula_genero1`
    FOREIGN KEY (`idgenero`)
    REFERENCES `videoclub`.`genero` (`idgenero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_genero_has_pelicula_pelicula1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `videoclub`.`pelicula` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `videoclub`.`actua`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `videoclub`.`actua` (
  `idactor` INT NOT NULL,
  `idpelicula` INT NOT NULL,
  PRIMARY KEY (`idactor`, `idpelicula`),
  INDEX `fk_actor_has_pelicula_pelicula1_idx` (`idpelicula` ASC),
  INDEX `fk_actor_has_pelicula_actor1_idx` (`idactor` ASC),
  CONSTRAINT `fk_actor_has_pelicula_actor1`
    FOREIGN KEY (`idactor`)
    REFERENCES `videoclub`.`actor` (`idactor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_actor_has_pelicula_pelicula1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `videoclub`.`pelicula` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;