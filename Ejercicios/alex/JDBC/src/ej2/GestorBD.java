package ej2;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestorBD {

	Connection con;
	ArrayList<String> lista = new ArrayList<>();

	/**
	 * Metodo conectar. En este metodo nos conectamos a la BBDD que tengamos en la
	 * IP que se nos pasa por parametro con su usuario y contraseña.
	 * 
	 * @param IP
	 * @param usu
	 * @param pass
	 * @throws SQLException
	 */
	public void conectar(String IP, String usu, String pass) throws SQLException {
		// Comprobamos el driver de mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://" + IP + "?serverTimezone=UTC", usu, pass);
		} catch (ClassNotFoundException ex) {
			System.err.println("No se encontro el Driver MySQL para JDBC.");
		}
	}

	/**
	 * Metodo listarBases. En este metodo recorreremos las bbdd que haya en nuestro
	 * SGBD y las aniadiremos a un ArrayList de string.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> listarBases() throws SQLException {
		// Vaciamos el Array de otras consultas.
		lista.clear();
		DatabaseMetaData dbmd = con.getMetaData();
		ResultSet rs = dbmd.getCatalogs();
		while (rs.next()) {
			// Aniadimos las BBDD al ArrayList
			lista.add(rs.getString(1));
		}
		return lista;
	}

	/**
	 * Metodo listarTablas. En este metodo vamos la listar las tablas de la BBDD que
	 * se nos pasa como parametro.
	 * 
	 * @param base
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> listarTablas(String base) throws SQLException {
		// Vaciamos el Array de otras consultas.
		lista.clear();
		if (existeBD(base)) {
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet rs = dbmd.getTables(base, null, "%", null);
			while (rs.next()) {
				// Aniadimos las tablas al ArrayList
				lista.add(rs.getString(3));
			}
			return lista;
		} else {
			System.err.println("La base de datos no existe.");
		}
		return lista;
	}

	/**
	 * Metodo listarColumnas. En este metodo vamos a listar las columnas con su
	 * nombre y tipo de columna.
	 * 
	 * @param base
	 * @param tabla
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Columna> listarColumnas(String base, String tabla) throws SQLException {
		ArrayList<Columna> lista = new ArrayList<>();
		if (existeBD(base)) {
			if (existeTabla(base, tabla)) {
				DatabaseMetaData dbmd = con.getMetaData();
				ResultSet rs = dbmd.getColumns(base, null, tabla, null);
				while (rs.next()) {
					Columna columna = new Columna(rs.getString(4), rs.getString(6));
					lista.add(columna);
				}
				return lista;
			} else {
				System.err.println("La tabla no existe.");
			}
		} else {
			System.err.println("La base de datos no existe.");
		}
		return lista;
	}
	

	/**
	 * Metodo existeTabla. Comprobamos si la tabla existe, lo compruebo con un
	 * equals si la tabla que busco es igual la que entra como parametro.
	 * 
	 * @param base
	 * @param tabla
	 * @return
	 * @throws SQLException
	 */
	boolean existeTabla(String base, String tabla) throws SQLException {
		DatabaseMetaData dbmd = con.getMetaData();
		ResultSet rs = dbmd.getTables(base, null, tabla, null);
		String table = "";
		while (rs.next()) {
			table = rs.getString(3);
			// Importante que vaya probando todos
			if (table.equals(tabla)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo existeBD.Comprobamos si la base de datos existe, lo compruebo con un
	 * equals si la BD que busco es igual la que entra como parametro.
	 * 
	 * @param base
	 * @return
	 * @throws SQLException
	 */
	boolean existeBD(String base) throws SQLException {
		DatabaseMetaData dbmd = con.getMetaData();
		ResultSet rs = dbmd.getCatalogs();
		String catalogo = null;
		while (rs.next()) {
			catalogo = rs.getString(1);
			// Importante que vaya probando todos
			if (catalogo.equals(base)) {
				return true;
			}
		}
		return false;
	}
}
