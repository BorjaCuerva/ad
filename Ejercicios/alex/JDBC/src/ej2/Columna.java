package ej2;

public class Columna {
	// Atributos
	private String nombre;
	private String tipo;

	// toString
	@Override
	public String toString() {
		return "Columna\t" + nombre + "\t|Tipo\t" + tipo;
	}

	// Constructor
	public Columna(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
	}

	// Getters and Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
