package ej2;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utilidades.Utilidades;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

/**
 * Desarrollar una aplicación Java que permita obtener, a partir del catálogo de
 * un SGBD MySQL la lista de bases de datos, tablas y columnas que contiene. Los
 * parámetros de acceso al SGBD se tomarán de un archivo llamado CFG.INI que
 * contendrá 3 líneas:
 * 
 * IP del servidor
 * 
 * MySQL Usuario MySQL
 * 
 * Contraseña MySQL
 * 
 * @author Alex
 * @version 20/11/2018
 */
public class InterfazUsuarioCatalogos extends JFrame {

	// Parametros de conexion
	static String IP;
	static String usu;
	static String pass;
	// Gestor BD
	static GestorBD bd = new GestorBD();
	// Utilidades
	static Utilidades utilidades;

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnListarBbdd;
	private JComboBox<String> comboBoxBBDD;
	private JTextArea textAreaResultado;
	private JButton btnMostrarColumnas;
	private JComboBox<String> comboBoxTablas;
	private JButton btnSalir;
	private JButton btnListarTablas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			obtenerDatosCFG();
			bd.conectar(IP, usu, pass);
		} catch (Exception e1) {
			utilidades.notificarError(null, "Error en la conexión con la BBDD.", "Error en la BBDD", e1);
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioCatalogos frame = new InterfazUsuarioCatalogos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Metodo obtenerDatosCFG. Obtenemos los parametros para el acceso al SGBD.
	 */
	private static void obtenerDatosCFG() {
		IP = "";
		usu = "";
		pass = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("cfg2.ini")));
			String linea;
			while ((linea = br.readLine()) != null) {
				String[] campo = linea.split("[;]");
				switch (campo[0]) {
				case "IP":
					IP = campo[1];
					break;
				case "usu":
					usu = campo[1];
					break;
				case "pass":
					pass = campo[1];
					break;
				default:
					break;
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			utilidades.notificarError(null, "Error en el fichero.", "Error en el fichero", e);
		} catch (IOException e) {
			utilidades.notificarError(null, "Error en la lectura del fichero.", "Error en la lectura", e);
		}

	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioCatalogos() {
		setTitle("Gestión de BBDD");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 338);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnListarBbdd = new JButton("Listar BBDD");
		btnListarBbdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonListarBBDD();
			}
		});
		btnListarBbdd.setBounds(12, 12, 135, 25);
		contentPane.add(btnListarBbdd);

		comboBoxBBDD = new JComboBox<String>();
		comboBoxBBDD.setBounds(12, 49, 135, 24);
		contentPane.add(comboBoxBBDD);

		btnListarTablas = new JButton("Listar Tablas");
		btnListarTablas.setEnabled(false);
		btnListarTablas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonListarTablas();
			}
		});
		btnListarTablas.setBounds(159, 12, 135, 25);
		contentPane.add(btnListarTablas);

		comboBoxTablas = new JComboBox<String>();
		comboBoxTablas.setBounds(159, 49, 135, 24);
		contentPane.add(comboBoxTablas);

		btnMostrarColumnas = new JButton("Columnas");
		btnMostrarColumnas.setEnabled(false);
		btnMostrarColumnas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonColumnas();
			}
		});
		btnMostrarColumnas.setBounds(306, 12, 130, 25);
		contentPane.add(btnMostrarColumnas);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 85, 424, 177);
		contentPane.add(scrollPane);

		textAreaResultado = new JTextArea();
		textAreaResultado.setEditable(false);
		scrollPane.setViewportView(textAreaResultado);

		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonSalir();
			}
		});
		btnSalir.setBounds(319, 274, 117, 25);
		contentPane.add(btnSalir);
	}

	/**
	 * Metodo botonSalir. Salimos de la aplicacion.
	 */
	protected void botonSalir() {
		System.exit(0);

	}

	/**
	 * Metodo botonColumnas. En este metodo mostramos las columnas y los tipos de la
	 * tabla en el textArea.
	 */
	protected void botonColumnas() {
		// Array de columnas con el nombre de la columna y el tipo
		ArrayList<Columna> lista = new ArrayList<>();
		lista.clear();
		textAreaResultado.setText("");
		// Base obtenida del comboBoxBBDD
		String base = (String) comboBoxBBDD.getSelectedItem();
		// Tabla obtenida del comboBoxTablas
		String tabla = (String) comboBoxTablas.getSelectedItem();
		try {
			lista = bd.listarColumnas(base, tabla);
			// Mostramos los resultados
			for (Columna columna : lista) {
				textAreaResultado.setText(textAreaResultado.getText() + columna.toString() + "\n");
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnMostrarColumnas.setEnabled(false);
		comboBoxTablas.setEnabled(false);
		comboBoxBBDD.setEnabled(true);
	}

	/**
	 * Metodo botonListarTablas. En este metodo vamos a mostrar las tablas de la
	 * bbdd introducida en un comboBox.
	 */
	protected void botonListarTablas() {
		// Array para mostrar las tablas
		ArrayList<String> lista = new ArrayList<>();
		lista.clear();
		String base = (String) comboBoxBBDD.getSelectedItem();
		// Eliminamos los Item del comboBoxTablas para que este vacio
		comboBoxTablas.removeAllItems();
		try {
			// Obtenemos las tablas
			lista = bd.listarTablas(base);
			for (String tabla : lista) {
				// Cargamos las tablas en el comboBox
				comboBoxTablas.addItem(tabla.toString());
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnMostrarColumnas.setEnabled(true);
		comboBoxBBDD.setEnabled(false);
		comboBoxTablas.setEnabled(true);
	}

	/**
	 * Metodo botonListarBBDD. En este metodo vamos a mostrar las BBDD en un
	 * comboBox.
	 */
	protected void botonListarBBDD() {
		// Array para mostrar las BBDD
		ArrayList<String> lista = new ArrayList<>();
		lista.clear();
		try {
			lista = bd.listarBases();
			for (String bbdd : lista) {
				// Cargamos las BBDD en el comboBox
				comboBoxBBDD.addItem(bbdd.toString());
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnListarBbdd.setEnabled(false);
		btnListarTablas.setEnabled(true);
	}
}
