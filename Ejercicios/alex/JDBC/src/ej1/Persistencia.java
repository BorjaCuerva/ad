package ej1;

import java.util.ArrayList;

/**
 * Interface Persistencia. Incluye los metodos para la clase PersistenciaBD.
 * 
 * @author Alex
 * @version 13/11/2018
 */
public interface Persistencia {

	/**
	 * Conecta la BD.
	 * 
	 * @param IP
	 * @param usu
	 * @param pass
	 * @param bd
	 * @throws Exception
	 */
	void conectarDB(String IP, String usu, String pass, String bd) throws Exception;

	/**
	 * Desconecta la BD.
	 * 
	 * @throws Exception
	 */
	void desconectarDB() throws Exception;

	/**
	 * Devuelve un listado de personas.
	 * 
	 * @param tabla
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	ArrayList<Persona> listadoPersonas(String tabla, String orderBy) throws Exception;

	/**
	 * Guarda la persona.
	 * 
	 * @param tabla
	 * @param p
	 * @throws Exception
	 */
	void guardarPersona(String tabla, Persona p) throws Exception;

	/**
	 * Borra la persona.
	 * 
	 * @param tabla
	 * @param email
	 * @throws Exception
	 */
	void borrarPersona(String tabla, String email) throws Exception;

	/**
	 * Consulta la persona si existe.
	 * 
	 * @param tabla
	 * @param email
	 * @return
	 * @throws Exception
	 */
	Persona consultarPersona(String tabla, String email) throws Exception;

}
