package ej1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * 
 * @author Alex
 * @version 13/11/2018
 */
public class PersistenciaBD implements Persistencia {

	Connection con;

	@Override
	public void conectarDB(String IP, String usu, String pass, String bd) throws Exception {
		// Comprobamos el driver de mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://" + IP + "/" + bd + "?serverTimezone=UTC", usu, pass);
		} catch (ClassNotFoundException ex) {
			System.err.println("No se encontro el Driver MySQL para JDBC.");
		}

	}

	@Override
	public void desconectarDB() throws Exception {
		con.close();
	}

	@Override
	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) throws Exception {
		ArrayList<Persona> personas = new ArrayList<>();
		String sql = "SELECT * FROM " + tabla + " ORDER BY " + orderBy;
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery(sql);
		while (rs.next()) {
			personas.add(new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
					rs.getString("email")));
		}
		rs.close();
		ps.close();
		return personas;
	}

	@Override
	public void guardarPersona(String tabla, Persona p) throws Exception {
		Persona persona = consultarPersona(tabla, p.getEmail());
		if (persona == null) {
			String sql = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getCP());
			ps.setString(3, p.getPais());
			ps.setString(4, p.getEmail());
			ps.executeUpdate();
			ps.close();
			JOptionPane.showMessageDialog(null, "La persona se ha guardado correctamente.", "Guardar",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			String sql = "UPDATE " + tabla + " SET nombre=?, CP=?, pais=? WHERE email=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getCP());
			ps.setString(3, p.getPais());
			ps.setString(4, p.getEmail());
			ps.executeUpdate();
			ps.close();
			JOptionPane.showMessageDialog(null, "La persona se ha modificado correctamente.", "Guardar",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	@Override
	public void borrarPersona(String tabla, String email) throws Exception {
		Persona persona = consultarPersona(tabla, email);
		if (persona != null) {
			String sql = "DELETE FROM " + tabla + " WHERE email= ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.executeUpdate();
			ps.close();
		} else {
			JOptionPane.showMessageDialog(null, "La persona no existe.", "Borrar", JOptionPane.INFORMATION_MESSAGE);
		}

	}

	@Override
	public Persona consultarPersona(String tabla, String email) throws Exception {
		String sql = "SELECT * FROM " + tabla + " WHERE email=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, email);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Persona p = new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
					rs.getString("email"));
			return p;
		} else {
			return null;
		}
	}

}
