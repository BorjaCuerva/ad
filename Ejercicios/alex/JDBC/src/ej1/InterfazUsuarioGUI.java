package ej1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utilidades.Utilidades;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class InterfazUsuarioGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldCP;
	private JButton btnAltaPersona;
	private JButton btnBajaPersona;
	private JPanel panelDatos;
	private JTextField textFieldPais;
	private JTextField textFieldEmail;
	private JButton btnListado;
	private JButton btnListadoPorCp;
	private JButton btnListadoPorEmail;
	private JButton btnListadoPorNombre;
	private JButton btnListadoPorPais;
	private JButton btnBorrar;
	private JButton btnSalir;
	private JButton btnGuardar;
	private JTextArea textArea;
	// Parametros para conectarnos a la BBDD
	static String tabla;
	static String IP;
	static String usu;
	static String pass;
	static String bd;
	// Persistencia
	static Persistencia per = new PersistenciaBD();
	// Utilidades
	static Utilidades utilidades = new Utilidades();
	// Array Listado
	ArrayList<Persona> personas = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			obtenerDatosCFG();
			per.conectarDB(IP, usu, pass, bd);
		} catch (Exception e1) {
			utilidades.notificarError(null, "Error en la conexión con la BBDD.", "Error en la BBDD", e1);
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.getStackTrace();
				}
			}
		});
	}

	/**
	 * Metodo obtenerDatosCFG. En este metodo leemos el fichero cfg.ini con la
	 * configuracion de la BBDD.
	 */
	private static void obtenerDatosCFG() {
		tabla = "";
		IP = "";
		usu = "";
		pass = "";
		bd = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("cfg.ini")));
			String linea;
			while ((linea = br.readLine()) != null) {
				String[] campo = linea.split("[;]");
				switch (campo[0]) {
				case "tabla":
					tabla = campo[1];
					break;
				case "bd":
					bd = campo[1];
					break;
				case "ip":
					IP = campo[1];
					break;
				case "usuario":
					usu = campo[1];
					break;
				case "password":
					pass = campo[1];
					break;
				default:
					break;
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			utilidades.notificarError(null, "Error en el fichero.", "Error en el fichero", e);
		} catch (IOException e) {
			utilidades.notificarError(null, "Error en la lectura del fichero.", "Error en la lectura", e);
		}
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle("Gestión de personas");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 453, 579);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnAltaPersona = new JButton("Alta persona");
		btnAltaPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonAlta();
			}
		});
		btnAltaPersona.setMnemonic('A');
		btnAltaPersona.setBounds(12, 12, 162, 25);
		contentPane.add(btnAltaPersona);

		btnBajaPersona = new JButton("Baja persona");
		btnBajaPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonBaja();
			}
		});
		btnBajaPersona.setMnemonic('B');
		btnBajaPersona.setBounds(12, 49, 162, 25);
		contentPane.add(btnBajaPersona);

		panelDatos = new JPanel();
		panelDatos.setVisible(false);
		panelDatos.setEnabled(false);
		panelDatos.setBounds(186, 12, 250, 120);
		contentPane.add(panelDatos);
		panelDatos.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 12, 70, 15);
		panelDatos.add(lblNombre);

		JLabel lblCP = new JLabel("CP");
		lblCP.setBounds(12, 39, 70, 15);
		panelDatos.add(lblCP);

		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(12, 66, 70, 15);
		panelDatos.add(lblPais);

		JLabel lblEmail = new JLabel("E-Mail");
		lblEmail.setBounds(12, 93, 70, 15);
		panelDatos.add(lblEmail);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(100, 10, 114, 19);
		panelDatos.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		textFieldCP = new JTextField();
		textFieldCP.setBounds(100, 37, 114, 19);
		panelDatos.add(textFieldCP);
		textFieldCP.setColumns(10);

		textFieldPais = new JTextField();
		textFieldPais.setBounds(100, 64, 114, 19);
		panelDatos.add(textFieldPais);
		textFieldPais.setColumns(10);

		textFieldEmail = new JTextField();
		textFieldEmail.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent evt) {
				consultarEmail(evt);
			}
		});
		textFieldEmail.setText("");
		textFieldEmail.setBounds(100, 91, 114, 19);
		panelDatos.add(textFieldEmail);
		textFieldEmail.setColumns(10);

		btnListado = new JButton("Listado");
		btnListado.setMnemonic('L');
		btnListado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonListado();
			}
		});
		btnListado.setBounds(12, 153, 162, 25);
		contentPane.add(btnListado);

		btnListadoPorCp = new JButton("Listado por CP");
		btnListadoPorCp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonListadoCP();
			}
		});
		btnListadoPorCp.setMnemonic('C');
		btnListadoPorCp.setBounds(12, 195, 198, 25);
		contentPane.add(btnListadoPorCp);

		btnListadoPorEmail = new JButton("Listado por E-Mail");
		btnListadoPorEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonListadoEmail();
			}
		});
		btnListadoPorEmail.setMnemonic('E');
		btnListadoPorEmail.setBounds(238, 195, 198, 25);
		contentPane.add(btnListadoPorEmail);

		btnListadoPorNombre = new JButton("Listado por Nombre");
		btnListadoPorNombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonListadoNombre();
			}
		});
		btnListadoPorNombre.setMnemonic('N');
		btnListadoPorNombre.setBounds(12, 231, 198, 25);
		contentPane.add(btnListadoPorNombre);

		btnListadoPorPais = new JButton("Listado por Pais");
		btnListadoPorPais.setMnemonic('P');
		btnListadoPorPais.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonListadoPais();
			}
		});
		btnListadoPorPais.setBounds(238, 231, 198, 25);
		contentPane.add(btnListadoPorPais);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(11, 267, 425, 242);
		contentPane.add(scrollPane);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);

		JSeparator separator = new JSeparator();
		separator.setBounds(9, 189, 427, 2);
		contentPane.add(separator);

		btnSalir = new JButton("Salir de la aplicación");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonSalir();
			}
		});
		btnSalir.setMnemonic('S');
		btnSalir.setBounds(237, 521, 199, 23);
		contentPane.add(btnSalir);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(12, 85, 162, 23);
		contentPane.add(btnGuardar);
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('G');

		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(12, 119, 162, 23);
		contentPane.add(btnBorrar);
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('B');
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonBorrar();
			}
		});
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonGuardar();
			}
		});
	}

	/*
	 * Metodo colsultarEmail. En este metoto consultamos con el email si la persona
	 * existe o no.
	 */
	protected void consultarEmail(KeyEvent evt) {
		if (evt.getKeyCode() == 10) {
			try {
				Persona persona = per.consultarPersona(tabla, textFieldEmail.getText());
				if (persona != null) {
					textFieldNombre.setText(persona.getNombre());
					textFieldCP.setText(persona.getCP());
					textFieldPais.setText(persona.getPais());
					btnBorrar.setVisible(true);
				} else {
					utilidades.notificarInformacion(null, "La persona no existe.", "Consultar persona");
					botonBaja();
				}
			} catch (Exception e) {
				utilidades.notificarError(null, "Error al consultar la persona.", "Error en la BBDD", e);
			}
		}

	}

	/**
	 * Metodo botonSalir. En este metodo desconectamos la conexion con la BBDD y
	 * salimos de la aplicacion.
	 */
	protected void botonSalir() {
		try {
			per.desconectarDB();
		} catch (Exception e) {
			utilidades.notificarError(null, "Error al salir de la BBDD.", "Error en la BBDD", e);
		}
		System.exit(0);

	}

	/**
	 * Metodo botonBorrar. En este metodo preguntamos si queremos borrar la persona
	 * y la borramos.
	 */
	protected void botonBorrar() {
		int opcion = JOptionPane.showConfirmDialog(null, "Esta seguro que desea borrar la persona.", "Borrar",
				JOptionPane.OK_CANCEL_OPTION);
		switch (opcion) {
		case JOptionPane.OK_OPTION:
			try {
				per.borrarPersona(tabla, textFieldEmail.getText());
				utilidades.notificarInformacion(null, "La persona se ha borrado correctamente.", "Borrar");
				botonCancelar();
			} catch (Exception e) {
				utilidades.notificarError(null, "Error al borrar la persona.", "Error en la BBDD", e);
			}
			break;
		case JOptionPane.CANCEL_OPTION:
			botonBaja();
			break;
		}

	}

	/**
	 * Metodo botonGuardar. En este metodo guardamos la persona.
	 */
	protected void botonGuardar() {
		Persona persona = new Persona(textFieldNombre.getText(), textFieldCP.getText(), textFieldPais.getText(),
				textFieldEmail.getText());
		try {
			per.guardarPersona(tabla, persona);
			botonCancelar();
		} catch (Exception e) {
			utilidades.notificarError(null, "Error al guardar la persona.", "Error en la BBDD", e);
		}
	}

	/**
	 * Metodo botonListadoPais. En este metodo mostraremos el listado y le pasaremos
	 * como parametro el pais.
	 */
	protected void botonListadoPais() {
		panelDatos.setVisible(false);
		mostrarListado("pais");
	}

	/**
	 * Metodo listadoNombre. En este metodo mostraremos el listado y le pasaremos
	 * como parametro el nombre.
	 */
	protected void botonListadoNombre() {
		panelDatos.setVisible(false);
		mostrarListado("nombre");
	}

	/**
	 * Metodo botonListadoEmail. En este metodo mostraremos el listado y le
	 * pasaremos como parametro el email.
	 */
	protected void botonListadoEmail() {
		panelDatos.setVisible(false);
		mostrarListado("email");
	}

	/**
	 * Metodo botonListadoCP. En este metodo mostraremos el listado y le pasaremos
	 * como parametro el CP.
	 */
	protected void botonListadoCP() {
		panelDatos.setVisible(false);
		mostrarListado("CP");
	}

	/**
	 * Metodo botonListado. En este metodo mostraremos el listado y le pasaremos
	 * como parametro el email.
	 */
	protected void botonListado() {
		panelDatos.setVisible(false);
		botonCancelar();
		habilitarBotonesListado(true);
		mostrarListado("email");

	}

	/**
	 * Metodo mostrarListado. En este metodo mostramos la el listado ordenado por el
	 * parametro que recibimos.
	 * 
	 * @param orderBy
	 */
	private void mostrarListado(String orderBy) {
		textArea.setText("");
		try {
			personas = per.listadoPersonas(tabla, orderBy);
			for (Persona persona : personas) {
				textArea.setText(textArea.getText() + persona.toString() + "\n");
			}
		} catch (Exception e) {
			utilidades.notificarError(null, "Error en el listado de las personas.", "Error en la BBDD", e);
		}

	}

	/**
	 * Metodo botonBaja. En este metodo mostramos el panel con los campos de la
	 * persona y habilitamos el campo de email para que introduzca un email y
	 * comprobar si la persona existe o no y si exite la muestra.
	 */
	protected void botonBaja() {
		panelDatos.setVisible(true);
		habilitarBotonesListado(false);
		botonCancelar();
		textFieldEmail.grabFocus();
		textFieldNombre.setEnabled(false);
		textFieldCP.setEnabled(false);
		textFieldPais.setEnabled(false);

	}

	/**
	 * Metodo botonAlta. En este metodo mostramos el panel para introducir los datos
	 * de la persona y mostramos el botoon guardar.
	 */
	protected void botonAlta() {
		panelDatos.setVisible(true);
		habilitarBotonesListado(false);
		botonCancelar();
		btnGuardar.setVisible(true);
		textFieldNombre.grabFocus();

	}

	/**
	 * Metodo habilitarBotonesListado. En este metodo habilitamos o deshabilitamos
	 * los botones de listado de BBDD.
	 * 
	 * @param activo
	 */
	private void habilitarBotonesListado(boolean activo) {
		btnListadoPorCp.setEnabled(activo);
		btnListadoPorEmail.setEnabled(activo);
		btnListadoPorNombre.setEnabled(activo);
		btnListadoPorPais.setEnabled(activo);
	}

	/**
	 * Metodo botonCancelar. En este metodo ponemos los campos a null y los
	 * habilitamos. Ocultamos los botones de guardar y de borrar.
	 */
	private void botonCancelar() {
		textFieldNombre.setText(null);
		textFieldCP.setText(null);
		textFieldPais.setText(null);
		textFieldEmail.setText(null);
		textArea.setText(null);
		textFieldNombre.setEnabled(true);
		textFieldCP.setEnabled(true);
		textFieldPais.setEnabled(true);
		textFieldEmail.setEnabled(true);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
	}
}
