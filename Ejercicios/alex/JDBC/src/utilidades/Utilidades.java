package utilidades;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Utilidades {

	/**
	 * Metodo notificarError. En este metodo vamos a recibir los parametros y si la
	 * excepcion no es null implementaremos el getClass().getName() y el getMessage.
	 * 
	 * @param padre
	 * @param mensaje
	 * @param titulo
	 * @param excepcion
	 */
	public void notificarError(JPanel padre, String mensaje, String titulo, Exception excepcion) {
		String texto = "";
		if (excepcion != null) {
			texto += excepcion.getClass().getName() + "\n" + excepcion.getMessage(); // Nombre de la excepcion y mensaje
		}
		if (mensaje != null) {
			texto += "\n" + mensaje;
		}
		JOptionPane.showMessageDialog(padre, texto, titulo, JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Metodo notificarInfomacion. En este metodo vamos a mostrar un mensaje de
	 * informacion dependiendo de los parametros.
	 * 
	 * @param padre
	 * @param mensaje
	 * @param titulo
	 */
	public void notificarInformacion(JPanel padre, String mensaje, String titulo) {
		JOptionPane.showMessageDialog(padre, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
	}
}
