package ej3;

import java.awt.EventQueue;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import ej2.Columna;
import ej2.GestorBD;
import utilidades.Utilidades;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Desarrollar una aplicación Java con entorno gráfico que permita exportar
 * cualquier tabla de un SGBD a un fichero XML, e importar ficheros XML
 * similares a los generados en la exportación. Los datos (IP, usu, pass) para
 * conectarse al SGBD se solicitarán a través del GUI.
 * 
 * @author Alex
 * @version 23/11/2018
 */
public class InterfazUsuario extends JFrame {

	// Parametros de conexion
	static String IP;
	static String usu;
	static String pass;
	// Gestor BD
	static GestorBD bd = new GestorBD();
	// Utilidades
	static Utilidades utilidades;

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnListarBbdd;
	private JComboBox<String> comboBoxBBDD;
	private JTextArea textAreaResultado;
	private JButton btnMostrarColumnas;
	private JComboBox<String> comboBoxTablas;
	private JButton btnSalir;
	private JButton btnListarTablas;
	private JTextField textFieldIP;
	private JButton btnConectarBbdd;
	private JLabel lblUsuario;
	private JTextField textFieldUsuario;
	private JLabel lblPassword;
	private JTextField textFieldPassword;
	private JButton btnExportarXml;
	private JButton btnImportarXml;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuario frame = new InterfazUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuario() {
		setResizable(false);
		setTitle("Gestión de BBDD");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 435);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnListarBbdd = new JButton("Listar BBDD");
		btnListarBbdd.setEnabled(false);
		btnListarBbdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonListarBBDD();
			}
		});
		btnListarBbdd.setBounds(12, 107, 135, 25);
		contentPane.add(btnListarBbdd);

		comboBoxBBDD = new JComboBox<String>();
		comboBoxBBDD.setBounds(12, 144, 135, 24);
		contentPane.add(comboBoxBBDD);

		btnListarTablas = new JButton("Listar Tablas");
		btnListarTablas.setEnabled(false);
		btnListarTablas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonListarTablas();
			}
		});
		btnListarTablas.setBounds(159, 107, 135, 25);
		contentPane.add(btnListarTablas);

		comboBoxTablas = new JComboBox<String>();
		comboBoxTablas.setBounds(159, 144, 135, 24);
		contentPane.add(comboBoxTablas);

		btnMostrarColumnas = new JButton("Columnas");
		btnMostrarColumnas.setEnabled(false);
		btnMostrarColumnas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonColumnas();
			}
		});
		btnMostrarColumnas.setBounds(306, 144, 130, 25);
		contentPane.add(btnMostrarColumnas);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 180, 424, 177);
		contentPane.add(scrollPane);

		textAreaResultado = new JTextArea();
		textAreaResultado.setEditable(false);
		scrollPane.setViewportView(textAreaResultado);

		btnSalir = new JButton("Salir");
		btnSalir.setMnemonic('S');
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonSalir();
			}
		});
		btnSalir.setBounds(319, 369, 117, 25);
		contentPane.add(btnSalir);

		JLabel lblIpBbdd = new JLabel("IP BBDD");
		lblIpBbdd.setBounds(12, 12, 114, 15);
		contentPane.add(lblIpBbdd);

		textFieldIP = new JTextField();
		textFieldIP.setBounds(12, 39, 114, 19);
		contentPane.add(textFieldIP);
		textFieldIP.setColumns(10);

		btnConectarBbdd = new JButton("Conectar BBDD");
		btnConectarBbdd.setMnemonic('C');
		btnConectarBbdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonConectarBBDD();
			}
		});
		btnConectarBbdd.setBounds(12, 70, 170, 25);
		contentPane.add(btnConectarBbdd);

		lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(159, 12, 114, 15);
		contentPane.add(lblUsuario);

		textFieldUsuario = new JTextField();
		textFieldUsuario.setBounds(159, 39, 114, 19);
		contentPane.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);

		lblPassword = new JLabel("Contraseña");
		lblPassword.setBounds(306, 12, 130, 15);
		contentPane.add(lblPassword);

		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(306, 39, 114, 19);
		contentPane.add(textFieldPassword);
		textFieldPassword.setColumns(10);

		btnExportarXml = new JButton("Exportar XML");
		btnExportarXml.setMnemonic('E');
		btnExportarXml.setEnabled(false);
		btnExportarXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonExportarXML();
			}
		});
		btnExportarXml.setBounds(12, 369, 135, 25);
		contentPane.add(btnExportarXml);

		btnImportarXml = new JButton("Importar XML");
		btnImportarXml.setMnemonic('I');
		btnImportarXml.setEnabled(false);
		btnImportarXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonImportarXML();
			}
		});
		btnImportarXml.setBounds(156, 369, 151, 25);
		contentPane.add(btnImportarXml);
	}

	/**
	 * Metodo botonImportarXML.
	 */
	protected void botonImportarXML() {

	}

	/**
	 * Metodo botonExportarXML.
	 */
	protected void botonExportarXML() {
		try {
			Document doc = null;
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Metodo conectar. En este metodo nos conectamos a la BBDD que tengamos en la
	 * IP que se nos pasa por parametro con su usuario y contraseña.
	 * 
	 * @param IP
	 * @param usu
	 * @param pass
	 * @throws SQLException
	 */
	protected void botonConectarBBDD() {
		try {
			// Si nos falta algun dato de conexion no continuamos
			if (!obtenerDatosConexion()) {
				return;
			}
			bd.conectar(IP, usu, pass);
			btnListarBbdd.setEnabled(true);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "No se pudo conectar con la BBDD.", "Error en la conexión",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Metodo obtenerDatosConexion. En este metodo vamos a obtener los parametros de
	 * conexion desde la interfaz de usuario. Comprobaremos que los campos tienen
	 * contenido y si no mostraremos un mensaje y pondremos el foco en el campo
	 * correspondiente.
	 */
	protected boolean obtenerDatosConexion() {
		if (textFieldIP.getText().length() == 0) {
			JOptionPane.showMessageDialog(null, "El campo IP no puede quedar vacio.", "Error en el campo IP",
					JOptionPane.ERROR_MESSAGE);
			textFieldIP.grabFocus();
			return false;
		}
		if (textFieldUsuario.getText().length() == 0) {
			JOptionPane.showMessageDialog(null, "El campo usuario no puede quedar vacio.", "Error en el campo usuario",
					JOptionPane.ERROR_MESSAGE);
			textFieldUsuario.grabFocus();
			return false;
		}
		if (textFieldPassword.getText().length() == 0) {
			JOptionPane.showMessageDialog(null, "El campo contraseña no puede quedar vacio.",
					"Error en el campo contraseña", JOptionPane.ERROR_MESSAGE);
			textFieldPassword.grabFocus();
			return false;
		}
		IP = textFieldIP.getText();
		usu = textFieldUsuario.getText();
		pass = textFieldPassword.getText();
		return true;
	}

	/**
	 * Metodo botonSalir. Salimos de la aplicacion.
	 */
	protected void botonSalir() {
		System.exit(0);
	}

	/**
	 * Metodo botonColumnas. En este metodo mostramos las columnas y los tipos de la
	 * tabla en el textArea.
	 */
	protected void botonColumnas() {
		// Array de columnas con el nombre de la columna y el tipo
		ArrayList<Columna> lista = new ArrayList<>();
		lista.clear();
		textAreaResultado.setText("");
		// Base obtenida del comboBoxBBDD
		String base = (String) comboBoxBBDD.getSelectedItem();
		// Tabla obtenida del comboBoxTablas
		String tabla = (String) comboBoxTablas.getSelectedItem();
		try {
			lista = bd.listarColumnas(base, tabla);
			// Mostramos los resultados
			for (Columna columna : lista) {
				textAreaResultado.setText(textAreaResultado.getText() + columna.toString() + "\n");
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnMostrarColumnas.setEnabled(false);
		comboBoxTablas.setEnabled(false);
		comboBoxBBDD.setEnabled(true);
		// Habilitamos los botones de XML
		btnExportarXml.setEnabled(true);
		btnImportarXml.setEnabled(true);
	}

	/**
	 * Metodo botonListarTablas. En este metodo vamos a mostrar las tablas de la
	 * bbdd introducida en un comboBox.
	 */
	protected void botonListarTablas() {
		// Array para mostrar las tablas
		ArrayList<String> lista = new ArrayList<>();
		lista.clear();
		String base = (String) comboBoxBBDD.getSelectedItem();
		// Eliminamos los Item del comboBoxTablas para que este vacio
		comboBoxTablas.removeAllItems();
		try {
			// Obtenemos las tablas
			lista = bd.listarTablas(base);
			for (String tabla : lista) {
				// Cargamos las tablas en el comboBox
				comboBoxTablas.addItem(tabla.toString());
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnMostrarColumnas.setEnabled(true);
		comboBoxBBDD.setEnabled(false);
		comboBoxTablas.setEnabled(true);
	}

	/**
	 * Metodo botonListarBBDD. En este metodo vamos a mostrar las BBDD en un
	 * comboBox.
	 */
	protected void botonListarBBDD() {
		// Array para mostrar las BBDD
		ArrayList<String> lista = new ArrayList<>();
		lista.clear();
		try {
			lista = bd.listarBases();
			for (String bbdd : lista) {
				// Cargamos las BBDD en el comboBox
				comboBoxBBDD.addItem(bbdd.toString());
			}
		} catch (SQLException e) {
			utilidades.notificarError(null, "Error al consultar la BBDD.", "Error en la BBDD", e);
		}
		btnListarBbdd.setEnabled(false);
		btnListarTablas.setEnabled(true);
	}
}
