package ejemplos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class EjemploJDBC {

	static Connection conexion;

	public static void main(String[] args) {

		// Comprobamos el driver de mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			System.err.println("No se encontro el Driver MySQL para JDBC.");
		}
		// Creamos la conexion
		try {
			conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1/ventas?serverTimezone=UTC", "root", "manager");
			Statement st = conexion.createStatement();
			
			String sqlDelete = "drop table ventas";
			st.executeUpdate(sqlDelete);
			String sqlCreate = "create table ventas(id integer,cantidad integer,precio float)";
			st.executeUpdate(sqlCreate);
			String sqlInsert = "insert into ventas values('1','20','25.60')";
			st.executeUpdate(sqlInsert);

			// Cerramos
			st.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
