package ej3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class PersistenciaDB {
	
	Connection con;
	
	public PersistenciaDB(String ip,String baseDatos,String usuario,String contrasenia) {
		
		String bd = "navidad"+baseDatos;
		String sql = "create database "+bd;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error con el driver");
		}
		try {
			con = DriverManager.getConnection("jdbc:mysql:/"+ip+"/accesoADatos",usuario,contrasenia);
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error al conectar a la BBDD");
		}
	}

}
