package ej1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.FieldPosition;
import java.awt.event.ActionEvent;

public class Procesador extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldRuta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Procesador frame = new Procesador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Procesador() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 464, 234);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRutaDeLa = new JLabel("Ruta de la carpeta que contiene los ficheros a procesar:");
		lblRutaDeLa.setBounds(12, 12, 417, 15);
		contentPane.add(lblRutaDeLa);
		
		textFieldRuta = new JTextField();
		textFieldRuta.setBounds(12, 39, 403, 19);
		contentPane.add(textFieldRuta);
		textFieldRuta.setColumns(10);
		
		/**
		 * BOTON PROCESAR
		 */
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				procesar();
				
			}

			
		});
		btnProcesar.setBounds(157, 93, 117, 25);
		contentPane.add(btnProcesar);
	}
	
	//METODOS
	
	/**
	 * Primero comprobamos que la ruta existe y que es una carpeta
	 */
	
	private void procesar() {
		
		String ruta = textFieldRuta.getText().toString();
		
		File fichero = new File(ruta);
		
		//Comprobamos que existe
		if(!fichero.exists()) {
			JOptionPane.showMessageDialog(null, "La ruta no existe!", "Error de ruta", 1);
		}else {
			//Comprobamos que sea un fichero
			if(!fichero.isDirectory()) {
				JOptionPane.showMessageDialog(null, ruta+" no es un fichero!", "Error de fichero", 1);
			}
			
			//Despues de comprobar si existe la ruta y si es un directorio
			
			try {
				crearXML(fichero);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}
	
	/**
	 * Metodo para crear los XML
	 * @throws IOException 
	 */
	private void crearXML(File fichero) throws IOException {
		
		String fecha = "";
		String venta = "";
		String listaFicheros []; //Array que contiene el nombre de los ficheros
		listaFicheros = fichero.list(); //Guardamos en el array el nombre de los ficheros
		String spliter [];
		
		
		for (int i = 0; i < listaFicheros.length; i++) {
			listaFicheros[i].toString();
			BufferedReader bfr = new BufferedReader(new FileReader(new File("datos/"+listaFicheros[i])));
			System.out.println(listaFicheros[i]);
			
			while((bfr.readLine())!=null) {
				fecha+=bfr.readLine();
				
			}
			System.out.println(fecha);
			bfr.close();
		}
		
	}
	
	
}
