package ejercicio2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class VerLineaLibro {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		//C:\Users\Borja\Desktop\biblioteca --> Casa
		// /home/alumno/biblioteca --> Clase
		String ruta = "C:\\Users\\Borja\\Desktop\\biblioteca\\";
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(ruta+"indice.xml");
		
		
		Scanner s = new Scanner(System.in);
		System.out.println("Introduzca titulo del libro: ");
		String titulo = s.nextLine();
		
		//expresiones xpath
		// //libro[@titulo="Pirata.txt"]//linea[@num="1"]/text()  -->sacamos la linea para usar con el RandomAccessFile
		
		//Busca libro con XPATH xpath admite las comillas simples ' ' , asi al construir el String es mas facil
				Element el = (Element) (XPathFactory.newInstance().newXPath().evaluate("//libro[@titulo='"+titulo+"']", doc,XPathConstants.NODE));
				if (el==null)
				{
					System.out.println("Libro no encontrado");
				}
				else{
					System.out.print("Introduzca numero de linea:");
					String num = s.next();
					el = (Element) (XPathFactory.newInstance().newXPath().evaluate("//libro[@titulo='"+titulo+"']//linea[@num='"+num+"']", doc,XPathConstants.NODE));
					if (el==null)
					{
						System.out.println("Linea no encontrada");
					}
					else{
						String pos=el.getTextContent();
						RandomAccessFile raf = new RandomAccessFile(ruta+"/"+titulo, "r");
						raf.seek(Integer.parseInt(pos));
						String texto = raf.readLine();
						System.out.println(pos+" --> "+texto);
						raf.close();
						
					}
				}
		
	}

}
