package ejemploXStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class PruebaXStream {

	public static void main(String[] args) {
		
		/**
		 * Lo primero que hay que hacer es arrastrar el .jar al proyecto
		 * Despues vamos a: boton derecho en el proyecto / build path / configure build path..
		 * Pulsamos en la opcion librerias / add Jars y añadimos el .jar que queramos para las librerias.
		 */
		
		//Instanciamos XStream
		XStream xstream = new XStream(new DomDriver()); //Instanciamos de esta forma, para que funcione con la libreria importada
		//Para que en la etiqueta XML salga person / phonenumber y no Person / PhoneNumber(selecciona el nombre de un .getClass)
		xstream.alias("person", Person.class);
		xstream.alias("phonenumber", PhoneNumber.class);
		
		//Creamos las personas que vamos a transformar en XML
		Person joe = new Person("Joe", "Walnes");
		joe.setPhone(new PhoneNumber(123, "1234-456"));
		joe.setFax(new PhoneNumber(123, "9999-999"));
		
		Person rloco = new Person("Rloco", "Rlocuras");
		rloco.setPhone(new PhoneNumber(123, "6666666"));
		rloco.setFax(new PhoneNumber(123, "9999-999"));
		
		//Guardamos en un String la persona
		//Este String es el que usamos para pasar la informacion
		String xml = xstream.toXML(joe);
		System.out.println(xml);
		
		System.out.println("");
		
		String xmlRloco = xstream.toXML(rloco);
		System.out.println(xmlRloco);

	}

}
