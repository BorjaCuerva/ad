package ejercicio3;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ModificadorDOM {

	public static void main(String[] args) {
		
		//Primero obtenemos la ruta del fichero xml que en este caso esta en el proyecto
		String ruta = "desayunos.xml";
		
		
		try {
			
			//Lo siguiente es construir el arbol DOM
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = factory.newDocumentBuilder();
			Document documento = docBuilder.parse(ruta);
			
			//Creamos una lista de los nodos descripcion, esto lo hacemos mediante xpath con //descripcion
			NodeList lista=(NodeList) (XPathFactory.newInstance().newXPath().evaluate("//descripcion", documento,XPathConstants.NODESET)); //Con NODE no funciona
			
			/**
			 * BORRAR LOS NODOS DESCRIPCION
			 */
			
			//Recorremos la lista para borrar todos los nodos con el nombre descripcion
			for (int i = 0; i < lista.getLength(); i++) {
				lista.item(i).getParentNode().removeChild(lista.item(i));
			}
			
			/**
			 * A�ADIR AL FINAL DEL DOCUMENTO UN ELEMENTO <total_calorias> QUE CONTENGA
			 * LA SUMA DE LAS CALORIAS DE TODOS LOS DESAYUNOS DEL DOCUMENTO XML
			 */
			
			//Lista de todos los elementos calorias
			NodeList listaDesayunos = (NodeList)(XPathFactory.newInstance().newXPath().evaluate("//calorias", documento,XPathConstants.NODESET)); //Hay que poner NODESET con NODE no funciona
			int sumaCalorias = 0;
			//sumamos todos los valores de los nodos calorias
			for (int i = 0; i < listaDesayunos.getLength(); i++) {
					sumaCalorias += Integer.parseInt(listaDesayunos.item(i).getTextContent());
			}
			
			//Creamos el elemento total_calorias
			Element raiz = documento.getDocumentElement();
			Element totalCalorias = documento.createElement("total_calorias");
			//Le a�adimos el valor de todas las calorias sumadas
			totalCalorias.setTextContent(String.valueOf(sumaCalorias));
			raiz.appendChild(totalCalorias);
			
			/**
			 * CAMBIAR LA MONEDA DE TODOS LOS PRECIOS A EURO, RECALCULANDO EL PRECIO-
			 * CREAR UN ARCHIVO Divisas.csv donde guardaremos los siguientes datos.
			 * 
			 * Divisas.csv: 
			 * libra esterlina;0.87
			 * peseta;166 
			 * dolar;1.12 
			 * 
			 * SI UNA MONEDA NO EXISTE EN ESTE ARCHIVO, NO SE MODIFICARA EN EL XML
			 * 
			 */
			
			
			
			
			
			
			/**
			 * PARA FINALIZAR ESCRIBIMOS EL ARBOL DOM MODIFICADO A UN NUEVO DOCUMENTO XML
			 * LLAMADO desayunos_modificado.xml
			 */
			//Ahora tenemos que escribir el arbol DOM a un documento XML
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(documento);
			StreamResult result = new StreamResult(new File("desayunos_modificado.xml"));
			transformer.transform(source, result);
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
