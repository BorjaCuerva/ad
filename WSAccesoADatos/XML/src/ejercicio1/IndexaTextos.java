package ejercicio1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IndexaTextos {

	public static void main(String[] args) throws ParserConfigurationException, IOException, TransformerException {
		
		//C:\Users\Borja\Desktop\biblioteca --> Casa
		// /home/alumno/biblioteca --> Clase
		String ruta = "C:\\Users\\Borja\\Desktop\\biblioteca\\";
		
		
		File file = new File(ruta);
		
		//Comprobamos que existe o que es un directorio
		if (!file.exists() || !file.isDirectory() ) {
			System.out.println("No es un directorio!!");
			System.exit(0);
		}
			
		//Borramos indice.xml
		File rutaIndice = new File(ruta+"indice.xml");
		rutaIndice.delete();
		
		//Creamos el arbol DOM
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		
		//Creamos elemento raiz
		Element raiz = doc.createElement("biblioteca");
		doc.appendChild(raiz);
		
		
		//Buscamos los libros
		File listaLibros [] = file.listFiles();
		for (int i = 0; i < listaLibros.length; i++) {
			//Crea elemento libro con su atributo titulo
			Element libro = doc.createElement("libro");
			libro.setAttribute("titulo", listaLibros[i].getName());
			raiz.appendChild(libro);
			
			RandomAccessFile raf = new RandomAccessFile(listaLibros[i], "r");
			int cnt = 0; //para el atributo num
			long pos = raf.getFilePointer(); //posicion inicial del cursor
			
			while (raf.readLine() != null) {
				cnt++;
				//Creamos el elemento linea con el atributo num
				Element linea = doc.createElement("linea");
				linea.setAttribute("num", String.valueOf(cnt)); //Convertimos cnt a String
				linea.setTextContent(String.valueOf(pos));
				libro.appendChild(linea);
				
				pos = raf.getFilePointer(); //Pasamos a la siguiente linea
			}
			raf.close(); //cerramos el stream
			System.out.println("Indexado --> " + listaLibros[i].getName());
		}
		
		//Escribimos arbol DOM a fichero XML
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,"yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(rutaIndice);
		transformer.transform(source, result);
		
		System.out.println("Fin.");
		

	}

}
