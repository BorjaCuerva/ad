package banco;

import java.util.Date;
import java.util.HashSet;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.mysql.fabric.xmlrpc.base.Data;

public class ClaseMain {

	public static void main(String[] args) {
		
		/**
		 * Estas tres lineas, hacen que no salgan los mensajes en rojo de hibernate
		 */
		LogManager.getLogManager().reset();
		Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
		globalLogger.setLevel(java.util.logging.Level.OFF);
		
		Date fecha = new Date(1989,03,30);
		Date fechaMov = new Date();
		
		Cuenta cuenta = new Cuenta(0, new HashSet<Cliente>(), new HashSet<Movimiento>());
		
		Cliente cliente = new Cliente("Borja", fecha, "Serafin", "70080010H", new HashSet<Cuenta>());
		
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		Session session = sessionFactory.openSession();
		
		cuenta.getClientes().add(cliente);
		
		session.beginTransaction();
		//Aniadimos la cuenta y no el cliente, porque la cuenta tiene el inverse a true
		//Los dos tienen el cascade all
		session.save(cuenta);
		session.getTransaction().commit();
		session.close();
		
		
		
		
	}

}
