package ejercicio7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import java.awt.event.ActionEvent;

public class ConvertirAMayusculas extends JFrame {

	private JPanel contentPane;
	private JButton btnBuscarArchivo;
	private JTextField textFieldCadena;
	private JButton btnConvertir;
	private File ruta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConvertirAMayusculas frame = new ConvertirAMayusculas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConvertirAMayusculas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnBuscarArchivo = new JButton("Seleccionar Texto");
		
		//EVENTO DEL BOTON BUSCAR ARCHIVO
		btnBuscarArchivo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				buscarArchivo();
				
			}

		});
		btnBuscarArchivo.setMnemonic('s');
		btnBuscarArchivo.setBounds(143, 28, 159, 25);
		contentPane.add(btnBuscarArchivo);
		
		JLabel lblCadenaAConvertir = new JLabel("Cadena a convertir");
		lblCadenaAConvertir.setBounds(12, 110, 147, 15);
		contentPane.add(lblCadenaAConvertir);
		
		textFieldCadena = new JTextField();
		textFieldCadena.setBounds(161, 108, 225, 19);
		contentPane.add(textFieldCadena);
		textFieldCadena.setColumns(10);
		
		btnConvertir = new JButton("Convertir");
		
		//EVENTO DEL BOTON CONVERTIR
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				convertir();
				
			}

		});
		btnConvertir.setMnemonic('c');
		btnConvertir.setBounds(143, 193, 159, 25);
		contentPane.add(btnConvertir);
	}
	
	public File buscarArchivo() {
		
		//Seleccionamos el archivo con JFileChooser			
		JFileChooser fc = new JFileChooser("/");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY); //Solo files
		fc.setDialogTitle("Seleccione archivo a abrir.");
		int opcion = fc.showOpenDialog(null);
		if (opcion == JFileChooser.APPROVE_OPTION) {
			
			ruta = fc.getSelectedFile();
			
		}
		
		return ruta;
	}
	
	private void convertir() {
		
		try {
			
			File ruta = null;
			
			RandomAccessFile raf = new RandomAccessFile(ruta, "rw");
			String linea;
			int index = Integer.parseInt(textFieldCadena.getText());
			long posicionInicial = 0;
			
			
			try {
				while((linea = raf.readLine())!=null) {
					
					if (linea.indexOf(index)!=-1) {
						linea = linea.replace(linea, linea.toUpperCase());
						raf.seek(posicionInicial);
						raf.writeChars(linea+"\n");
					}
					posicionInicial = raf.getFilePointer();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
