package ejercicio5;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerFicheros {

	public static void main(String[] args) throws IOException {
		
		String rutaTXT = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.txt";
		String rutaBIN = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.bin";
		String rutaOBJ = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.obj";
		
		try {
			BufferedReader bfr = new BufferedReader(new FileReader(new File(rutaTXT)));
			System.out.println("Fichero TXT:");
			leerFicheroTXT(bfr);
			System.out.println("\n");
			bfr.close();
			DataInputStream dis = new DataInputStream(new FileInputStream(new File(rutaBIN)));
			leerFicheroBIN(dis);
			dis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	private static void leerFicheroBIN(DataInputStream dis) throws IOException {
		
		
		
	}

	/**
	 * Metodo que lee un fichero txt y lo muestra por consola
	 * @param bfr BufferedReader
	 * @throws IOException
	 */
	private static void leerFicheroTXT(BufferedReader bfr) throws IOException {
		
		String linea;
		while ((linea = bfr.readLine())!=null) {
			linea+=bfr.readLine();
			System.out.println(linea);
		}
		
	}

}
