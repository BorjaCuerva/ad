package ejercicio5;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

public class GrabarFicheros {

	public static void main(String[] args) {
		
		Persona persona1 = new Persona(30, "Borja", 'M');
		Persona persona2 = new Persona(20, "Luis", 'M');
		Persona persona3 = new Persona(25, "Sara", 'F');
		
		String rutaTXT = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.txt";
		String rutaBIN = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.bin";
		String rutaOBJ = "/media/Datos/Borja/Repositorios/ad/Ejercicios/prueba/persona/Persona.obj";
		
		
		try {
			PrintWriter pw = new PrintWriter(new File(rutaTXT));
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(rutaBIN)));
			String titulo = "Edad"+";"+"Nombre"+";"+"Sexo\n";
			try {
				//PERSONAS OBJ
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(rutaOBJ)));
				oos.writeObject(titulo);
				grabarAOBJ(persona1,oos);
				grabarAOBJ(persona2,oos);
				grabarAOBJ(persona3,oos);
				oos.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			//PERSONAS TXT
			pw.println(titulo);
			grabarATXT(persona1, pw);
			grabarATXT(persona2, pw);
			grabarATXT(persona3, pw);
			pw.close();
			
			try {
				//PERSONAS BIN
				dos.writeUTF(titulo);
				grabarABIN(persona1,dos);
				grabarABIN(persona2,dos);
				grabarABIN(persona3,dos);
				dos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		

	}
	
	/**
	 * Metodo para grabar personas en archivos obj
	 * @param persona a grabar
	 * @param oos ObjectOutputStream
	 * @throws IOException
	 */
	private static void grabarAOBJ(Persona persona, ObjectOutputStream oos) throws IOException {
		
		oos.writeObject(persona);
		
	}

	/**
	 * Metodo para grabar personas en archivos binarios
	 * @param persona a grabar
	 * @param dos DataOutputStream
	 * @throws IOException
	 */
	private static void grabarABIN(Persona persona, DataOutputStream dos) throws IOException {
		
		dos.writeUTF(persona.getEdad()+";"+persona.getNombre()+";"+persona.getSexo()+"\n");
		
	}

	/**
	 * Metodo para grabar personas en archivos de texto
	 * @param persona a grabar
	 * @param pw PrintWriter con la ruta donde grabar el archivo
	 */
	public static void grabarATXT(Persona persona, PrintWriter pw) {
		
		pw.println(persona.getEdad()+";"+persona.getNombre()+";"+persona.getSexo()+"\n");
		
	}

}
