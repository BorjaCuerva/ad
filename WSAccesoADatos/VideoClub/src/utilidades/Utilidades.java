package utilidades;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utilidades {
	
	/**
	 * 
	 * @param padre
	 * @param titulo titulo de la excepcion
	 * @param e tipo de excepcion
	 * @param mensaje mensaje al usuario
	 */
	public static void notificarError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (mensaje != null)
			contenido += mensaje+"\n";
		//Muestra la excepci�n y su causas(excepciones anidadas)
		while (e != null) {
			contenido += "\n["+e.getClass().getName() + "] " + e.getMessage(); // Tipo y mensaje de la excepci�n
			e=(Exception)e.getCause();
		}
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}

}
