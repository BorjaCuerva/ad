package videoClub;
// Generated 05-feb-2020 11:51:27 by Hibernate Tools 4.0.1.Final

import java.util.HashSet;
import java.util.Set;

/**
 * Cliente generated by hbm2java
 */
public class Cliente implements java.io.Serializable {

	private Integer idcliente;
	private String nombre;
	private String apellidos;
	private String email;
	private Set alquilers = new HashSet(0);

	public Cliente() {
	}

	public Cliente(String email) {
		this.email = email;
	}

	public Cliente(String nombre, String apellidos, String email, Set alquilers) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
		this.alquilers = alquilers;
	}



	public Cliente(Integer idcliente, String nombre, String apellidos, String email) {
		super();
		this.idcliente = idcliente;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
	}
	
	public Cliente(String nombre, String apellidos, String email) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
	}

	public Integer getIdcliente() {
		return this.idcliente;
	}

	public void setIdcliente(Integer idcliente) {
		this.idcliente = idcliente;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set getAlquilers() {
		return this.alquilers;
	}

	public void setAlquilers(Set alquilers) {
		this.alquilers = alquilers;
	}

	@Override
	public String toString() {
		return "Cliente [idcliente=" + idcliente + ", nombre=" + nombre + ", apellidos=" + apellidos + ", email="
				+ email + ", alquilers=" + alquilers + "]";
	}
	
	
	
	

}
