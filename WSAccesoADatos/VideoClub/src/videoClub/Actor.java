package videoClub;
// Generated 05-feb-2020 11:51:27 by Hibernate Tools 4.0.1.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Actor generated by hbm2java
 */
public class Actor implements java.io.Serializable {

	private Integer idactor;
	private String nombre;
	private Date fechanac;
	private Set peliculas = new HashSet(0);

	public Actor() {
	}

	public Actor(String nombre, Date fechanac) {
		this.nombre = nombre;
		this.fechanac = fechanac;
	}

	public Actor(String nombre, Date fechanac, Set peliculas) {
		this.nombre = nombre;
		this.fechanac = fechanac;
		this.peliculas = peliculas;
	}
	
	
	

	public Actor(Integer idactor, String nombre) {
		super();
		this.idactor = idactor;
		this.nombre = nombre;
	}

	public Actor(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Actor(Integer idactor, String nombre, Date fechanac) {
		super();
		this.idactor = idactor;
		this.nombre = nombre;
		this.fechanac = fechanac;
	}

	public Integer getIdactor() {
		return this.idactor;
	}

	public void setIdactor(Integer idactor) {
		this.idactor = idactor;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechanac() {
		return this.fechanac;
	}

	public void setFechanac(Date fechanac) {
		this.fechanac = fechanac;
	}

	public Set getPeliculas() {
		return this.peliculas;
	}

	public void setPeliculas(Set peliculas) {
		this.peliculas = peliculas;
	}

	@Override
	public String toString() {
		return "Actor [idactor=" + idactor + ", nombre=" + nombre + ", fechanac=" + fechanac + "]";
	}
	
	
	
	

}
