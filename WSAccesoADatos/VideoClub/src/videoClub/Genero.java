package videoClub;
// Generated 05-feb-2020 11:51:27 by Hibernate Tools 4.0.1.Final

import java.util.HashSet;
import java.util.Set;

/**
 * Genero generated by hbm2java
 */
public class Genero implements java.io.Serializable {



	private Integer idgenero;
	private String descripcion;
	private Set peliculas = new HashSet(0);

	public Genero() {
	}

	public Genero(String descripcion) {
		this.descripcion = descripcion;
	}

	public Genero(String descripcion, Set peliculas) {
		this.descripcion = descripcion;
		this.peliculas = peliculas;
	}
	
	public Genero(int idgenero,String descripcion) {
		this.descripcion = descripcion;
		this.idgenero = idgenero;
	}

	public Integer getIdgenero() {
		return this.idgenero;
	}

	public void setIdgenero(Integer idgenero) {
		this.idgenero = idgenero;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set getPeliculas() {
		return this.peliculas;
	}

	public void setPeliculas(Set peliculas) {
		this.peliculas = peliculas;
	}
	
	@Override
	public String toString() {
		return "Genero [idgenero=" + idgenero + ", descripcion=" + descripcion + ", peliculas=" + peliculas + "]";
	}
	
	

}
