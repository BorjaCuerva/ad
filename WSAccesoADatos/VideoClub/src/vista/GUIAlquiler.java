package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;
import utilidades.Utilidades;
import videoClub.Actor;
import videoClub.Cliente;
import videoClub.Ejemplar;
import videoClub.Genero;
import videoClub.Pelicula;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUIAlquiler extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIDpelicula;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldTitulo;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnBorrar;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
	private ArrayList<Pelicula> alPeliculas = new ArrayList<>();
	private ArrayList<Genero> alGeneros = new ArrayList<>();
	private ArrayList<Actor> alActores = new ArrayList<>();

	Pelicula pelicula;
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */

	private JTable tablePeliculas;
	private JScrollPane scrollPane;
	private JTextField textFieldSinopsis;
	private JLabel lblNewLabel_2;
	private JTextField textFieldPrecioDia;
	private JLabel lblAsociarquitarGenero;
	private JTable tableGenero;
	private JLabel lblAsociarquitarActor;
	private JScrollPane scrollPaneActor;
	private JTable tableActor;
	private JButton btnAsociarActor;
	private JButton btnQuitarActor;
	private JButton btnAsociarGenero;
	private JButton btnQuitarGenero;
	private JTextField textFieldIDGenero;
	private JTextField textFieldDescripcionGenero;
	private JTextField textFieldIDActor;
	private JTextField textFieldNombreActor;
	private JScrollPane scrollPaneGenero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIAlquiler frame = new GUIAlquiler();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIAlquiler() {
		setResizable(false);
		setTitle("Peliculas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 858, 671);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(25, 47, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Titulo:");
		lblNewLabel_1.setBounds(25, 84, 93, 15);
		contentPane.add(lblNewLabel_1);

		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelo = (DefaultTableModel) tablePeliculas.getModel();

				btnBuscar.setEnabled(false);

				try {

					alPeliculas = per.listadoPeliculas(textFieldTitulo.getText());

					Object[] fila = new Object[4];

					if (alPeliculas.size() != 0) {
						for (Pelicula pelicula2 : alPeliculas) {

							fila[0] = pelicula2.getIdpelicula();
							fila[1] = pelicula2.getTitulo();
							fila[2] = pelicula2.getSinopsis();
							fila[3] = pelicula2.getPreciodia();

							modelo.addRow(fila); // Aniadimos el array

						}

						tablePeliculas.setModel(modelo);

						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						// Si no hay ninguna coincidencia con los clientes, da la opcion de guardarlo
					} else {

						// Expresion regular para el precio del dia
						if (textFieldPrecioDia.getText().matches("[0-9]*")) {

							if (!textFieldTitulo.getText().equals("")) {
								int opcion = JOptionPane.showConfirmDialog(null,
										"No existe ninguna pelicula que contenga esas letras.\n�quieres guardarla?",
										"Guardar pelicula", JOptionPane.INFORMATION_MESSAGE);
								// Si pulsamos si
								if (opcion == JOptionPane.YES_OPTION) {

									int precioDia;

									if (textFieldPrecioDia.getText().equals("")) {
										precioDia = 0;
									} else {
										precioDia = Integer.parseInt(textFieldPrecioDia.getText());
									}

									Pelicula p = new Pelicula(textFieldTitulo.getText(), textFieldSinopsis.getText(),
											precioDia);
									per.guardarPelicula(p);
									estadoInicial();
									// si pulsamos no
								}
								if (opcion == JOptionPane.NO_OPTION) {
									estadoInicial();
								}
								// Si pulsamos cancelar
								if (opcion == JOptionPane.CANCEL_OPTION) {
									estadoInicial();
								}
							} else {
								for (Pelicula pelicula2 : alPeliculas) {

									fila[0] = pelicula2.getIdpelicula();
									fila[1] = pelicula2.getTitulo();
									fila[2] = pelicula2.getSinopsis();
									fila[3] = pelicula2.getPreciodia();

									modelo.addRow(fila); // Aniadimos el array

								}
								tablePeliculas.setModel(modelo);

								btnBorrar.setVisible(true);
								btnGuardar.setVisible(true);
								btnModificar.setVisible(true);
							}
						} else {
							utilidades.Utilidades.notificarError(GUIAlquiler.this, "Campo precio", null,
									"Tiene que introducir un precio valido(solo numeros del 0 al 9)");
						}

					}
				} catch (Exception e) {
					System.err.println("Error al buscar la pelicula.");
					e.printStackTrace();
				}

				// RELLENAMOS LA TABLA GENEROS

				DefaultTableModel modeloGenero = (DefaultTableModel) tableGenero.getModel();
				try {
					alGeneros = per.listadoGeneros("");
					Object[] filaGenero = new Object[2];

					if (alGeneros.size() != 0) {
						for (Genero genero2 : alGeneros) {

							filaGenero[0] = genero2.getIdgenero();
							filaGenero[1] = genero2.getDescripcion();

							modeloGenero.addRow(filaGenero); // Aniadimos el array

						}

						tableGenero.setModel(modeloGenero);
					}
				} catch (SQLException e) {

					e.printStackTrace();
				}

				// RELLENAMOS LA TABLA ACTORES

				DefaultTableModel modeloActor = (DefaultTableModel) tableActor.getModel();
				try {
					alActores = per.listadoActores("");
					Object[] filaActor = new Object[2];

					if (alGeneros.size() != 0) {
						for (Actor actor2 : alActores) {

							filaActor[0] = actor2.getIdactor();
							filaActor[1] = actor2.getNombre();

							modeloActor.addRow(filaActor); // Aniadimos el array

						}

						tableActor.setModel(modeloActor);
					}
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(267, 71, 114, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estadoInicial();
			}

		});
		btnCancelar.setBounds(267, 135, 117, 41);
		contentPane.add(btnCancelar);

		textFieldIDpelicula = new JTextField();
		textFieldIDpelicula.setEditable(false);
		textFieldIDpelicula.setBounds(138, 44, 114, 19);
		contentPane.add(textFieldIDpelicula);
		textFieldIDpelicula.setColumns(10);

		textFieldTitulo = new JTextField();
		textFieldTitulo.setBounds(138, 81, 114, 19);
		contentPane.add(textFieldTitulo);
		textFieldTitulo.setColumns(10);

		// BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/guardar.png")));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					String filtro = textFieldTitulo.getText().trim().replace("[ ]{2,}", " ");
					textFieldTitulo.setText(filtro);
					pelicula = new Pelicula();

					// Vemos si el cliente existe
					pelicula = per.buscarPelicula(textFieldTitulo.getText());

					// Si la descripcion del cliente NO esta vacia
					if (!textFieldTitulo.getText().equals("")) {
						// Si el cliente no existe, le damos de alta
						if (pelicula == null) {

							// Si no es un cliente seleccionado de la tabla se guarda
							if (textFieldIDpelicula.getText().equals("")) {
								int precioDia;

								if (textFieldPrecioDia.getText().matches("[0-9]*")) {
									if (textFieldPrecioDia.getText().equals("")) {
										precioDia = 0;
									} else {
										precioDia = Integer.parseInt(textFieldPrecioDia.getText());
									}

									Pelicula p = new Pelicula(textFieldTitulo.getText(), textFieldSinopsis.getText(),
											precioDia);
									per.guardarPelicula(p);
									JOptionPane.showMessageDialog(padre, "La pelicula " + textFieldTitulo.getText()
											+ " se ha guardado correctamente");
									estadoInicial();
								} else {
									utilidades.Utilidades.notificarError(GUIAlquiler.this, "Campo precio", null,
											"Tiene que introducir un precio valido(solo numeros del 0 al 9)");
								}

							}

						} else {
							JOptionPane.showMessageDialog(padre,
									"La pelicula " + textFieldTitulo.getText() + " ya existe.");
							estadoInicial();

						}
					} else {
						JOptionPane.showMessageDialog(padre, "El titulo de la pelicula no puede estar vac�o");
					}

				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(25, 591, 117, 41);
		contentPane.add(btnGuardar);

		/**
		 * BOTON MODIDIFICAR
		 */
		btnModificar = new JButton("Modificar");
		btnModificar.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/derecha.png")));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					String filtro = textFieldTitulo.getText().trim().replace("[ ]{2,}", " ");
					int id = Integer.parseInt(textFieldIDpelicula.getText());
					textFieldTitulo.setText(filtro);

					int precioDia;

					if (textFieldPrecioDia.getText().matches("[0-9]*")) {

						if (textFieldPrecioDia.getText().equals("")) {
							precioDia = 0;
						} else {
							precioDia = Integer.parseInt(textFieldPrecioDia.getText());
						}
						pelicula = new Pelicula(id, textFieldTitulo.getText(), textFieldSinopsis.getText(), precioDia);

						if (pelicula != null) {

							per.modificarPelicula(pelicula);

							JOptionPane.showMessageDialog(padre,
									"La pelicula " + textFieldTitulo.getText() + " se ha modificado correctamente");
							estadoInicial();

						} else {
							JOptionPane.showMessageDialog(padre,
									"El cliente " + textFieldPrecioDia.getText() + " No se ha podido modificar.");
							estadoInicial();
						}

					} else {
						utilidades.Utilidades.notificarError(GUIAlquiler.this, "Campo precio", null,
								"Tiene que introducir un precio valido(solo numeros del 0 al 9)");
					}

				} catch (Exception e) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al modificar pelicula", null,
							"Debe seleccionar una pelicula.");
					e.printStackTrace();
				}
			}
		});
		btnModificar.setVisible(false);
		btnModificar.setMnemonic('m');
		btnModificar.setBounds(152, 591, 117, 41);
		contentPane.add(btnModificar);

		/**
		 * BOTON BORRAR
		 */
		btnBorrar = new JButton("Borrar");
		btnBorrar.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/borrar.png")));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int fila = tablePeliculas.getSelectedRow(); // Posicion en el al del cliente seleccionado

				// Si no tenemos seleccionada ninguna pelicula
				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al borrar pelicula", null,
							"Debe seleccionar una pelicula.");
				} else {
					// Mensaje informativo al usuario, sobre el borrado del cliente
					int opcion = JOptionPane.showConfirmDialog(null, "Se borrara la pelicula.\n�quieres borrarla?",
							"Borrar pelicula", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						Pelicula pelicula = alPeliculas.get(fila);
						borrarPelicula(pelicula);
						limpiarTabla(tablePeliculas);
						estadoInicial();

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}

		});
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('r');
		btnBorrar.setBounds(279, 591, 117, 41);
		contentPane.add(btnBorrar);

		JLabel lblListadoDeGneros = new JLabel("Listado de peliculas");
		lblListadoDeGneros.setBounds(25, 215, 174, 15);
		contentPane.add(lblListadoDeGneros);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(406, 591, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 243, 359, 138);
		contentPane.add(scrollPane);

		tablePeliculas = new JTable();
		tablePeliculas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tablePeliculas);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDPELICULA", "TITULO", "SINOPSIS", "PRECIO/DIA" }); // Asignamos
																										// columnas de
																										// los campos al
		// listado
		tablePeliculas.setModel(modelo);

		JLabel lblFechaNacimiento = new JLabel("Sinopsis");
		lblFechaNacimiento.setBounds(25, 124, 64, 14);
		contentPane.add(lblFechaNacimiento);

		textFieldSinopsis = new JTextField();
		textFieldSinopsis.setBounds(138, 121, 114, 20);
		contentPane.add(textFieldSinopsis);
		textFieldSinopsis.setColumns(10);

		lblNewLabel_2 = new JLabel("Precio / dia:");
		lblNewLabel_2.setBounds(25, 162, 70, 14);
		contentPane.add(lblNewLabel_2);

		textFieldPrecioDia = new JTextField();
		textFieldPrecioDia.setBounds(138, 159, 114, 20);
		contentPane.add(textFieldPrecioDia);
		textFieldPrecioDia.setColumns(10);

		JLabel lblPeliculas = new JLabel("Peliculas");
		lblPeliculas.setFont(new Font("Dialog", Font.BOLD, 16));
		lblPeliculas.setBounds(291, 11, 79, 14);
		contentPane.add(lblPeliculas);

		lblAsociarquitarGenero = new JLabel("Asociar/Quitar genero");
		lblAsociarquitarGenero.setBounds(507, 47, 166, 14);
		contentPane.add(lblAsociarquitarGenero);

		scrollPaneGenero = new JScrollPane();
		scrollPaneGenero.setBounds(420, 83, 281, 138);
		contentPane.add(scrollPaneGenero);

		/**
		 * MOUSE CLICKED TABLA GENERO
		 */
		tableGenero = new JTable();
		tableGenero.setVisible(false);
		tableGenero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				mostrarGeneros();

			}
		});
		scrollPaneGenero.setViewportView(tableGenero);
		tableGenero.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		DefaultTableModel modeloGenero = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modeloGenero.setColumnIdentifiers(new Object[] { "IDGENERO", "DESCRIPCION" }); // Asignamos columnas de los
																						// campos al
		// listado
		tableGenero.setModel(modeloGenero);

		lblAsociarquitarActor = new JLabel("Asociar/Quitar Actor");
		lblAsociarquitarActor.setBounds(495, 300, 166, 14);
		contentPane.add(lblAsociarquitarActor);

		scrollPaneActor = new JScrollPane();
		scrollPaneActor.setBounds(420, 325, 281, 138);
		contentPane.add(scrollPaneActor);

		/**
		 * MOUSE CLICKED TABLA ACTORES
		 */
		tableActor = new JTable();
		tableActor.setVisible(false);
		tableActor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				mostrarActores();

			}

		});
		tableActor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneActor.setViewportView(tableActor);

		DefaultTableModel modeloActor = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modeloActor.setColumnIdentifiers(new Object[] { "IDACTOR", "NOMBRE" }); // Asignamos columnas de los campos al
		// listado
		tableActor.setModel(modeloActor);

		/**
		 * BOTON ASOCIAR ACTOR
		 */
		btnAsociarActor = new JButton("Asociar");
		btnAsociarActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int fila = tableActor.getSelectedRow(); // Posicion en el al del actor seleccionado

				// Si no tenemos seleccionada ningun genero
				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al asociar actor", null,
							"Debe seleccionar un actor.");
				} else {

					// Creamos los dos objetos para insertar en la tabla actua
					Actor actor = new Actor(Integer.parseInt(textFieldIDActor.getText()),
							textFieldNombreActor.getText());
					Pelicula pelicula = new Pelicula(Integer.parseInt(textFieldIDpelicula.getText()),
							textFieldTitulo.getText());

					try {
						per.actuaAniadir(pelicula, actor);
						JOptionPane.showMessageDialog(padre, "Se ha asociado el actor " + actor.getNombre()
								+ " a la pelicula " + pelicula.getTitulo());
						estadoInicial();
					} catch (SQLException e1) {
						utilidades.Utilidades.notificarError(padre, "Error tabla pertenece", null, "el g�nero "
								+ actor.getNombre() + " ya pertenece a la pelicula " + pelicula.getTitulo());
						estadoInicial();
						// e1.printStackTrace();
					}

				}

			}
		});
		btnAsociarActor.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/aceptar.png")));
		btnAsociarActor.setEnabled(false);
		btnAsociarActor.setMnemonic('a');
		btnAsociarActor.setBounds(420, 474, 117, 51);
		contentPane.add(btnAsociarActor);

		/**
		 * BOTON QUITAR ACTOR
		 */
		btnQuitarActor = new JButton("Quitar");
		btnQuitarActor.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/borrar.png")));
		btnQuitarActor.setEnabled(false);
		btnQuitarActor.setMnemonic('q');
		btnQuitarActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int fila = tableActor.getSelectedRow(); // Posicion en el al del genero seleccionado

				// Si no tenemos seleccionada ningun genero
				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al quitar actor", null,
							"Debe seleccionar un actor.");
				} else {

					int opcion = JOptionPane.showConfirmDialog(null, "�Est�s seguro de que quieres quitar el actor?.\n",
							"eliminar actor", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {

						// Creamos los dos objetos para quitar en la tabla actua
						Actor actor = new Actor(Integer.parseInt(textFieldIDActor.getText()),
								textFieldNombreActor.getText());
						Pelicula pelicula = new Pelicula(Integer.parseInt(textFieldIDpelicula.getText()),
								textFieldTitulo.getText());

						try {
							per.actuaQuitar(actor, pelicula);
							JOptionPane.showMessageDialog(padre, "Se ha quitado el actor " + actor.getNombre()
									+ " de la pelicula " + pelicula.getTitulo());
							estadoInicial();
						} catch (SQLException e1) {
							utilidades.Utilidades.notificarError(padre, "Error tabla actua", e1, "el actor "
									+ actor.getNombre() + " no pertenece a la pelicula " + pelicula.getTitulo() + "\n");
							estadoInicial();
							// e1.printStackTrace();
						}

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}
		});
		btnQuitarActor.setBounds(582, 474, 117, 51);
		contentPane.add(btnQuitarActor);

		/**
		 * BOTON ASOCIAR GENERO
		 */
		btnAsociarGenero = new JButton("Asociar");
		btnAsociarGenero.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/aceptar.png")));
		btnAsociarGenero.setEnabled(false);
		btnAsociarGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int fila = tableGenero.getSelectedRow(); // Posicion en el al del genero seleccionado

				// Si no tenemos seleccionada ningun genero
				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al asociar genero", null,
							"Debe seleccionar un genero.");
				} else {

					// Creamos los dos objetos para insertar en la tabla pertenece
					Pelicula pelicula = new Pelicula(Integer.parseInt(textFieldIDpelicula.getText()),
							textFieldTitulo.getText());
					Genero genero = new Genero(Integer.parseInt(textFieldIDGenero.getText()),
							textFieldDescripcionGenero.getText());

					try {
						per.perteneceAniadir(pelicula, genero);
						JOptionPane.showMessageDialog(padre, "Se ha asociado el g�nero " + genero.getDescripcion()
								+ " a la pelicula " + pelicula.getTitulo());
						estadoInicial();
					} catch (SQLException e1) {
						utilidades.Utilidades.notificarError(padre, "Error tabla pertenece", null,
								"el g�nero " + genero.getDescripcion() + " ya pertenece a la pelicula "
										+ pelicula.getTitulo() + "\n");
						estadoInicial();
						// e1.printStackTrace();
					}

				}

			}

		});
		btnAsociarGenero.setMnemonic('a');
		btnAsociarGenero.setBounds(420, 243, 117, 46);
		contentPane.add(btnAsociarGenero);

		/**
		 * MOUSE CLICKED QUITAR GENERO
		 */
		btnQuitarGenero = new JButton("Quitar");
		btnQuitarGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnQuitarGenero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				int fila = tableGenero.getSelectedRow(); // Posicion en el al del genero seleccionado

				// Si no tenemos seleccionada ningun genero
				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIAlquiler.this, "Error al quitar genero", null,
							"Debe seleccionar un genero.");
				} else {

					int opcion = JOptionPane.showConfirmDialog(null,
							"�Est�s seguro de que quieres quitar el genero?.\n", "eliminar genero",
							JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						// Creamos el objeto para borrar de la tabla pertenece
						Genero genero = new Genero(Integer.parseInt(textFieldIDGenero.getText()),
								textFieldDescripcionGenero.getText());
						Pelicula pelicula = new Pelicula(Integer.parseInt(textFieldIDpelicula.getText()),
								textFieldTitulo.getText());
						try {
							per.perteneceQuitar(genero, pelicula);
							JOptionPane.showMessageDialog(padre, "Se ha quitado el g�nero " + genero.getDescripcion()
									+ " de la pelicula " + pelicula.getTitulo());
							estadoInicial();
						} catch (SQLException e1) {
							utilidades.Utilidades.notificarError(padre, "Error tabla pertenece", e1,
									"el g�nero " + genero.getDescripcion() + " no pertenece a la pelicula "
											+ pelicula.getTitulo() + "\n");
							estadoInicial();
							// e1.printStackTrace();
						}

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}
		});
		btnQuitarGenero.setIcon(new ImageIcon(GUIAlquiler.class.getResource("/iconos/borrar.png")));
		btnQuitarGenero.setEnabled(false);
		btnQuitarGenero.setMnemonic('q');
		btnQuitarGenero.setBounds(582, 243, 117, 46);
		contentPane.add(btnQuitarGenero);

		textFieldIDGenero = new JTextField();
		textFieldIDGenero.setEditable(false);
		textFieldIDGenero.setBounds(711, 119, 109, 20);
		contentPane.add(textFieldIDGenero);
		textFieldIDGenero.setColumns(10);

		textFieldDescripcionGenero = new JTextField();
		textFieldDescripcionGenero.setEditable(false);
		textFieldDescripcionGenero.setBounds(711, 187, 109, 20);
		contentPane.add(textFieldDescripcionGenero);
		textFieldDescripcionGenero.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("ID Genero");
		lblNewLabel_3.setBounds(727, 97, 93, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Descripcion");
		lblNewLabel_4.setBounds(727, 161, 93, 14);
		contentPane.add(lblNewLabel_4);

		JLabel lblIdActor = new JLabel("ID Actor");
		lblIdActor.setBounds(725, 337, 93, 14);
		contentPane.add(lblIdActor);

		textFieldIDActor = new JTextField();
		textFieldIDActor.setEditable(false);
		textFieldIDActor.setColumns(10);
		textFieldIDActor.setBounds(709, 359, 120, 20);
		contentPane.add(textFieldIDActor);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(725, 401, 93, 14);
		contentPane.add(lblNombre);

		textFieldNombreActor = new JTextField();
		textFieldNombreActor.setEditable(false);
		textFieldNombreActor.setColumns(10);
		textFieldNombreActor.setBounds(709, 427, 120, 20);
		contentPane.add(textFieldNombreActor);

		tablePeliculas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tablePeliculas.getSelectedRow() >= 0) {
					mostrarPeliculas();
					btnAsociarGenero.setEnabled(true);
					btnQuitarGenero.setEnabled(true);
					btnAsociarActor.setEnabled(true);
					btnQuitarActor.setEnabled(true);
					tableActor.setVisible(true);
					tableGenero.setVisible(true);

				}
			}
		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnModificar.setVisible(false);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
		btnBuscar.setEnabled(true);
		textFieldIDpelicula.setText("");
		textFieldTitulo.setText("");
		textFieldSinopsis.setText("");
		textFieldPrecioDia.setText("");
		textFieldTitulo.grabFocus();
		btnAsociarGenero.setEnabled(false);
		btnQuitarGenero.setEnabled(false);
		btnAsociarActor.setEnabled(false);
		btnQuitarActor.setEnabled(false);
		textFieldIDActor.setText("");
		textFieldDescripcionGenero.setText("");
		textFieldIDGenero.setText("");
		textFieldNombreActor.setText("");
		tableActor.setVisible(true);
		tableGenero.setVisible(true);

		// Limpiamos la tabla
		limpiarTabla(tablePeliculas);
		limpiarTabla(tableGenero);
		limpiarTabla(tableActor);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 */
	public void limpiarTabla(JTable tabla) {

		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
		}

		tabla.setModel(modelo);

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarPeliculas() {

		System.out.println(alPeliculas); // Mensaje para comprobar el contenido del objeto seleccionado de la tabla,
											// borrar al acabar

		int fila = tablePeliculas.getSelectedRow(); // Posicion en el al del cliente seleccionado

		System.out.println("Fila: " + fila);

		Pelicula pelicula = alPeliculas.get(fila);
		// Mostramos los datos
		textFieldIDpelicula.setText(pelicula.getIdpelicula() + "");
		textFieldTitulo.setText(pelicula.getTitulo() + "");
		textFieldSinopsis.setText(pelicula.getSinopsis() + "");
		textFieldPrecioDia.setText(pelicula.getPreciodia() + "");

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarGeneros() {

		System.out.println(alGeneros); // Mensaje para comprovar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar

		int fila = tableGenero.getSelectedRow(); // Posicion en el al del genero seleccionado

		System.out.println("Fila: " + fila);

		Genero genero = alGeneros.get(fila);
		// Mostramos los datos
		textFieldIDGenero.setText(genero.getIdgenero() + "");
		textFieldDescripcionGenero.setText(genero.getDescripcion());

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarActores() {

		System.out.println(alActores); // Mensaje para comprovar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar
		int fila = tableActor.getSelectedRow(); // Posicion en el al del genero seleccionado

		System.out.println("Fila: " + fila);

		Actor actor = alActores.get(fila);
		// Mostramos los datos
		textFieldIDActor.setText(actor.getIdactor() + "");
		textFieldNombreActor.setText(actor.getNombre() + "");

	}

	/**
	 * Metodo para borrar el cliente
	 * 
	 * @param pelicula
	 */
	private void borrarPelicula(Pelicula pelicula) {

		try {
			per.borrarPelicula(pelicula);
			limpiarTabla(tablePeliculas);
			JOptionPane.showMessageDialog(null, "Pelicula eliminada");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(GUIAlquiler.this, "", e,
					"Error al borrar la pelicula \n La pel�cula tiene ejemplares, actores o generos asociados. \n Si quieres borrarla, primero tienes que borrar todos los contenidos asociados con la misma. \n Mensaje de error: \n");

		}

	}

}
