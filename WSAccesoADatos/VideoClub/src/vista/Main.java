package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;
import utilidades.Utilidades;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Properties;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class Main extends javax.swing.JFrame {

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	boolean generoExiste;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per;

	private JPanel contentPane;
	private JMenu mnInformes;
	private JMenu mnMantenimiento;
	private JMenuItem mntmGenero;
	private JMenuItem mntmPelicula;
	private JMenuItem mntmInformeIngresos;
	private JMenuItem mntmInformeAlquileres;
	private JButton btnSalir;
	private JMenuBar menuBar;
	private JMenuItem mntmEjemplares;
	private JMenuItem mntmActores;
	private JMenuItem mntmClientes;
	private JMenuItem mntmTarifas;
	private JMenuItem mntmInformeEjemplares;
	private JMenuItem mntmAlquiler;
	private JMenu mnGestion;
	private JMenuItem mntmDevolucion;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				//Comprobamos configuracion				
				
				try {
					// Cargamos el tipo de persistencia
					String tipoPersistencia = null;
					Properties prop = new Properties();
					try {
						prop.load(new FileReader("src/CFG.INI"));
						tipoPersistencia = prop.getProperty("tipoPersistencia");

						/**
						 * Switch para comprobar si estamos usando mysql o hibernate Si usamos mysql, el
						 * objeto per va a referenciarse a mysql Si usamos hibernate, el objeto per va a
						 * referenciarse a hibernate
						 */
						switch (tipoPersistencia) {
						// Si tenemos mysql
						case "mysql":
							String servidor = prop.getProperty("mysqlJDBC.servidor");
							String baseDatos = prop.getProperty("mysqlJDBC.baseDatos");
							String usuario = prop.getProperty("mysqlJDBC.usuario");
							String password = prop.getProperty("mysqlJDBC.password");

							// Per
							per = new PersistenciaMYSQL(tipoPersistencia, servidor, baseDatos, usuario, password);

							break;
						// Si tenemos hibernate
						case "hibernate":
							// Hay que mirar como cargar el objeto per
							// per=new PersistenciaHibernate("CFG.INI");
							prop.load(new InputStreamReader(GUIGenero.class.getResourceAsStream("CFG.INI")));

							break;
						default:
							Utilidades.notificarError(padre, "Error de conexionn", null,
									" No se ha conectado a ninguina persistencia \n");
						}

					} catch (Exception e2) {
						utilidades.Utilidades.notificarError(padre, "Error", e2, "No existe el fichero\n");
						System.exit(0);
					}

					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setBackground(new Color(0, 153, 255));
		setResizable(false);

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 525, 517);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 509, 21);
		contentPane.add(menuBar);

		mnMantenimiento = new JMenu("Mantenimiento");
		mnMantenimiento.setMnemonic('m');
		menuBar.add(mnMantenimiento);

		mntmGenero = new JMenuItem("G\u00E9neros");
		mntmGenero.addActionListener(new ActionListener() {

			// ACTION PERFORMED GENEROS
			public void actionPerformed(ActionEvent e) {

				GUIGenero v = new GUIGenero();
				v.setVisible(true);

			}
		});
		mntmGenero.setMnemonic('g');
		mnMantenimiento.add(mntmGenero);

		mntmPelicula = new JMenuItem("Pel\u00EDculas");
		mntmPelicula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GUIPeliculas v = new GUIPeliculas();
				v.setVisible(true);
				
			}
		});
		mntmPelicula.setMnemonic('p');
		mnMantenimiento.add(mntmPelicula);
		
		mntmActores = new JMenuItem("Actores");
		mntmActores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GUIActores v = new GUIActores();
				v.setVisible(true);
				
			}
		});
		mntmActores.setMnemonic('a');
		mnMantenimiento.add(mntmActores);
		
		mntmEjemplares = new JMenuItem("Ejemplares");
		mntmEjemplares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GUIEjemplares v = new GUIEjemplares();
				v.setVisible(true);
				
			}
		});
		mntmEjemplares.setMnemonic('e');
		mnMantenimiento.add(mntmEjemplares);
		
		mntmClientes = new JMenuItem("Clientes");
		mntmClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GUIClientes v = new GUIClientes();
				v.setVisible(true);
				
			}
		});
		mntmClientes.setMnemonic('c');
		mnMantenimiento.add(mntmClientes);
		
		mntmTarifas = new JMenuItem("Tarifas");
		mntmTarifas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				GUITarifas v = new GUITarifas();
				v.setVisible(true);
				
			}
		});
		mntmTarifas.setMnemonic('t');
		mnMantenimiento.add(mntmTarifas);
		
		mnGestion = new JMenu("Gestion");
		mnGestion.setMnemonic('i');
		menuBar.add(mnGestion);
		
		mntmAlquiler = new JMenuItem("Alquiler");
		mntmAlquiler.setMnemonic('d');
		mnGestion.add(mntmAlquiler);
		
		mntmDevolucion = new JMenuItem("Devoluci\u00F3n");
		mntmDevolucion.setMnemonic('p');
		mnGestion.add(mntmDevolucion);
		
				mnInformes = new JMenu("Informes");
				mnInformes.setMnemonic('i');
				menuBar.add(mnInformes);
				
						mntmInformeIngresos = new JMenuItem("Ingresos");
						mntmInformeIngresos.setMnemonic('i');
						mnInformes.add(mntmInformeIngresos);
						
								mntmInformeAlquileres = new JMenuItem("Historial de alquileres");
								mntmInformeAlquileres.setMnemonic('h');
								mnInformes.add(mntmInformeAlquileres);
								
								mntmInformeEjemplares = new JMenuItem("Estado de ejemplares");
								mntmInformeEjemplares.setMnemonic('e');
								mnInformes.add(mntmInformeEjemplares);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(Main.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(392, 427, 117, 51);
		contentPane.add(btnSalir);
		
		btnNewButton = new JButton("");
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(Main.class.getResource("/iconos/videoclub.jpg")));
		btnNewButton.setBounds(10, 124, 499, 292);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("VIDEOCLUB BORJA");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 51, 102));
		lblNewLabel.setFont(new Font("Gill Sans Ultra Bold Condensed", Font.BOLD, 47));
		lblNewLabel.setBounds(10, 32, 499, 81);
		contentPane.add(lblNewLabel);
	}
}
