package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;
import utilidades.Utilidades;
import videoClub.Cliente;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUIClientes extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldID;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldNobre;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnBorrar;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
	private ArrayList<Cliente> alClientes = new ArrayList<>();

	Cliente cliente;
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */

	private JTable tableClientes;
	private JScrollPane scrollPane;
	private JTextField textFieldApellidos;
	private JLabel lblNewLabel_2;
	private JTextField textFieldEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIClientes frame = new GUIClientes();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIClientes() {
		setResizable(false);
		setTitle("Clientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGneros = new JLabel("Clientes");
		lblGneros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGneros.setBounds(291, 12, 102, 27);
		contentPane.add(lblGneros);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(25, 47, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(25, 84, 93, 15);
		contentPane.add(lblNewLabel_1);

		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelo = (DefaultTableModel) tableClientes.getModel();

				btnBuscar.setEnabled(false);

				try {

					alClientes = per.listadoClientes(textFieldEmail.getText());

					Object[] fila = new Object[4];

					if (alClientes.size() != 0) {
						for (Cliente cliente2 : alClientes) {

							fila[0] = cliente2.getIdcliente();
							fila[1] = cliente2.getNombre();
							fila[2] = cliente2.getApellidos();
							fila[3] = cliente2.getEmail();

							modelo.addRow(fila); // Aniadimos el array

						}

						tableClientes.setModel(modelo);

						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						// Si no hay ninguna coincidencia con los clientes, da la opcion de guardarlo
					} else {

						if (!textFieldEmail.getText().equals("")) {
							int opcion = JOptionPane.showConfirmDialog(null,
									"No existe ning�n cliente que contenga esas letras.\n�quieres guardarlo?",
									"Guardar cliente", JOptionPane.INFORMATION_MESSAGE);
							// Si pulsamos si
							if (opcion == JOptionPane.YES_OPTION) {
								Cliente c = new Cliente(textFieldNobre.getText(), textFieldApellidos.getText(), textFieldEmail.getText());
								per.guardarCliente(c);
								estadoInicial();
								// si pulsamos no
							}
							if (opcion == JOptionPane.NO_OPTION) {
								estadoInicial();
							}
							// Si pulsamos cancelar
							if (opcion == JOptionPane.CANCEL_OPTION) {
								estadoInicial();
							}
						} else {
							for (Cliente cliente2 : alClientes) {

								fila[0] = cliente2.getIdcliente();
								fila[1] = cliente2.getNombre();
								fila[2] = cliente2.getApellidos();
								fila[3] = cliente2.getEmail();

								modelo.addRow(fila); // Aniadimos el array

								
							}
							tableClientes.setModel(modelo);

							btnBorrar.setVisible(true);
							btnGuardar.setVisible(true);
							btnModificar.setVisible(true);
						}

					}
				} catch (Exception e) {
					System.err.println("Error al buscar el g�nero.");
					e.printStackTrace();
				}

			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(291, 71, 114, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estadoInicial();
			}

		});
		btnCancelar.setBounds(291, 132, 117, 41);
		contentPane.add(btnCancelar);

		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setBounds(138, 44, 114, 19);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		textFieldNobre = new JTextField();
		textFieldNobre.setBounds(138, 81, 114, 19);
		contentPane.add(textFieldNobre);
		textFieldNobre.setColumns(10);

		// BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/guardar.png")));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					String filtro = textFieldEmail.getText().trim().replace("[ ]{2,}", " ");
					textFieldEmail.setText(filtro);
					cliente = new Cliente();

					// Vemos si el cliente existe
					cliente = per.buscarCliente(textFieldEmail.getText());

					//Si la descripcion del cliente NO esta vacia
					if (!textFieldEmail.getText().equals("")) {
						// Si el cliente no existe, le damos de alta
						if (cliente == null) {

							// Si no es un cliente seleccionado de la tabla se guarda
							if (textFieldID.getText().equals("")) {
								
								Cliente clienteAGuardar = new Cliente(textFieldNobre.getText(), textFieldApellidos.getText(), textFieldEmail.getText());
								per.guardarCliente(clienteAGuardar);
								JOptionPane.showMessageDialog(padre, "El cliente " + textFieldEmail.getText()
										+ " se ha guardado correctamente");
								estadoInicial();

							}

						} else {
							JOptionPane.showMessageDialog(padre,
									"El cliente " + textFieldEmail.getText() + " ya existe.");
							estadoInicial();

						}
					}else {
						JOptionPane.showMessageDialog(padre,
								"El email del cliente no puede estar vac�o");
					}

				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(64, 396, 117, 41);
		contentPane.add(btnGuardar);

		/**
		 * BOTON MODIDIFICAR
		 */
		btnModificar = new JButton("Modificar");
		btnModificar.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/derecha.png")));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					String filtro = textFieldEmail.getText().trim().replace("[ ]{2,}", " ");
					int id = Integer.parseInt(textFieldID.getText());
					String nombre = textFieldNobre.getText();
					String apellidos = textFieldApellidos.getText();			
					textFieldEmail.setText(filtro);
					cliente = new Cliente(id,nombre, apellidos, filtro);

					if (cliente != null) {

						per.modificarCliente(cliente);
						JOptionPane.showMessageDialog(padre,
								"El cliente " + textFieldEmail.getText() + " se ha modificado correctamente");
						estadoInicial();

					} else {
						JOptionPane.showMessageDialog(padre,
								"El cliente " + textFieldEmail.getText() + " No se ha podido modificar.");
						estadoInicial();
					}

				} catch (Exception e) {
					utilidades.Utilidades.notificarError(GUIClientes.this, "Error al modificar cliente", null,
							"Debe seleccionar un cliente.");
				}
			}
		});
		btnModificar.setVisible(false);
		btnModificar.setMnemonic('m');
		btnModificar.setBounds(291, 396, 117, 41);
		contentPane.add(btnModificar);

		/**
		 * BOTON BORRAR
		 */
		btnBorrar = new JButton("Borrar");
		btnBorrar.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/borrar.png")));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int fila = tableClientes.getSelectedRow(); // Posicion en el al del cliente seleccionado

				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIClientes.this, "Error al borrar cliente", null,
							"Debe seleccionar un cliente.");
				} else {
					// Mensaje informativo al usuario, sobre el borrado del cliente
					int opcion = JOptionPane.showConfirmDialog(null, "Se borrara el cliente.\n�quieres borrarlo?",
							"Borrar cliente", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						Cliente cliente = alClientes.get(fila);
						borrarCliente(cliente);
						limpiarTabla(tableClientes);
						estadoInicial();

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}

		});
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('r');
		btnBorrar.setBounds(420, 396, 117, 41);
		contentPane.add(btnBorrar);

		JLabel lblListadoDeGneros = new JLabel("Listado de clientes");
		lblListadoDeGneros.setBounds(25, 215, 108, 15);
		contentPane.add(lblListadoDeGneros);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUIClientes.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(549, 396, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 243, 647, 138);
		contentPane.add(scrollPane);

		tableClientes = new JTable();
		tableClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableClientes);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDCLIENTE", "NOMBRE","APELLIDOS","EMAIL" }); // Asignamos columnas de los campos al
																					// listado
		tableClientes.setModel(modelo);
		
		JLabel lblFechaNacimiento = new JLabel("Apellidos");
		lblFechaNacimiento.setBounds(25, 124, 64, 14);
		contentPane.add(lblFechaNacimiento);
		
		textFieldApellidos = new JTextField();
		textFieldApellidos.setBounds(138, 121, 114, 20);
		contentPane.add(textFieldApellidos);
		textFieldApellidos.setColumns(10);
		
		lblNewLabel_2 = new JLabel("Email");
		lblNewLabel_2.setBounds(25, 162, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(138, 159, 114, 20);
		contentPane.add(textFieldEmail);
		textFieldEmail.setColumns(10);

		tableClientes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableClientes.getSelectedRow() >= 0) {
					mostrarClientes();
				}
			}
		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnModificar.setVisible(false);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
		btnBuscar.setEnabled(true);
		textFieldID.setText("");
		textFieldNobre.setText("");
		textFieldApellidos.setText("");
		textFieldEmail.setText("");
		textFieldNobre.grabFocus();
		// Limpiamos la tabla
		limpiarTabla(tableClientes);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 */
	public void limpiarTabla(JTable tabla) {

		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
		}

		tabla.setModel(modelo);

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarClientes() {

		System.out.println(alClientes); // Mensaje para comprobar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar

		int fila = tableClientes.getSelectedRow(); // Posicion en el al del cliente seleccionado

		System.out.println("Fila: " + fila);
		
		Cliente cliente = alClientes.get(fila);
		// Mostramos los datos
		textFieldID.setText(cliente.getIdcliente() + "");
		textFieldNobre.setText(cliente.getNombre() + "");
		textFieldApellidos.setText(cliente.getApellidos() + "");
		textFieldEmail.setText(cliente.getEmail() + "");


	}

	/**
	 * Metodo para borrar el cliente
	 * 
	 * @param cliente
	 */
	private void borrarCliente(Cliente cliente) {

		try {
			per.borrarCliente(cliente);
			limpiarTabla(tableClientes);
			JOptionPane.showMessageDialog(null, "Cliente eliminado");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(GUIClientes.this, "", e, "Error al borrar el cliente");

		}

	}
}
