package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;
import utilidades.Utilidades;
import videoClub.Genero;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUIGenero extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldID;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldDescripcion;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnBorrar;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	boolean generoExiste;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
	private ArrayList<Genero> alGeneros = new ArrayList<>();

	Genero genero;
=======
	private JScrollPane scrollPane;
<<<<<<< HEAD
	DefaultTableModel modelo;
	ArrayList<Genero> listaGeneros = new ArrayList<>();
	Object[] fila = new Object[2];
=======
	private ArrayList<Genero> listaGeneros = new ArrayList<>();
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java

	private JTable tableGeneros;
	private JScrollPane scrollPane;
=======
	
	DefaultTableModel modelo;
>>>>>>> master
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIGenero frame = new GUIGenero();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIGenero() {
		setResizable(false);
		setTitle("G�neros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGneros = new JLabel("G\u00E9neros");
		lblGneros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGneros.setBounds(291, 12, 102, 27);
		contentPane.add(lblGneros);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(25, 84, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Descripci\u00F3n:");
		lblNewLabel_1.setBounds(25, 132, 93, 15);
		contentPane.add(lblNewLabel_1);
		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java

				DefaultTableModel modelo = (DefaultTableModel) tableGeneros.getModel();

				btnBuscar.setEnabled(false);

				try {

					alGeneros = per.listadoGeneros(textFieldDescripcion.getText());

					Object[] fila = new Object[2];

					if (alGeneros.size() != 0) {
						for (Genero genero2 : alGeneros) {

							fila[0] = genero2.getIdgenero();
							fila[1] = genero2.getDescripcion();

=======
<<<<<<< HEAD

				try {
					String filtro = textFieldDescripcion.getText();
					Genero genero = per.buscarGenero(filtro);
					listaGeneros = per.listadoGeneros(filtro);
					if (genero != null) {
						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						

						for (Genero genero2 : listaGeneros) {

							fila[0] = genero2.getIdgenero();
							fila[1] = genero2.getDescripcion();
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
							modelo.addRow(fila); // Aniadimos el array

						}

<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
						tableGeneros.setModel(modelo);

						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						// Si no hay ninguna coincidencia con los generos, da la opcion de guardarlo
					} else {

						if (!textFieldDescripcion.getText().equals("")) {
							int opcion = JOptionPane.showConfirmDialog(null,
									"No existe ning�n g�nero que contenga esas letras.\n�quieres guardarlo?",
									"Guardar g�nero", JOptionPane.INFORMATION_MESSAGE);
							// Si pulsamos si
							if (opcion == JOptionPane.YES_OPTION) {
=======
					} else {
						int opcion = JOptionPane.showConfirmDialog(null,
								"No hay ning�n g�nero con ese nombre, quieres darle de alta?", "El g�nero no existe",
								JOptionPane.OK_CANCEL_OPTION);

						switch (opcion) {
						case JOptionPane.OK_OPTION:
=======
				// Modelo del jTable
				
//				try {
//					String filtro = textFieldDescripcion.getText();
//					Genero genero = per.buscarGenero(filtro);
//					
//
//					if (genero != null) {
//						btnBorrar.setVisible(true);
//						btnGuardar.setVisible(true);
//						btnModificar.setVisible(true);
//						listaGeneros = per.listadoGeneros(filtro);
//						modelo = (DefaultTableModel) tableGeneros.getModel();
//						
//						for (Genero genero2 : listaGeneros) {
//							
//							Object[] fila = new Object[2];
//							fila[0] = genero2.getIdgenero();
//							fila[1] = genero2.getDescripcion();
//
//							modelo.addRow(fila); // Aniadimos el array
//
//						}
//
//					} else {
//						int opcion = JOptionPane.showConfirmDialog(null,
//								"No hay ning�n g�nero con ese nombre, quieres darle de alta?", "El g�nero no existe",
//								JOptionPane.OK_CANCEL_OPTION);
//
//						switch (opcion) {
//						case JOptionPane.OK_OPTION:
//							btnGuardar.setVisible(true);
//							btnBuscar.setEnabled(false);
//							estadoInicial();
//							break;
//						case JOptionPane.CANCEL_OPTION:
//							estadoInicial();
//							break;
//						case JOptionPane.CLOSED_OPTION:
//							estadoInicial();
//							break;
//						}
//
//					}
//
////					textFieldDescripcion.grabFocus();
//
//				} catch (SQLException e1) {
//					utilidades.Utilidades.notificarError(null, "Filtro", e1, "Error en el filtro");
//
//				}
//				
				
				//limpiarTabla(tableGeneros);
				
				
				if (!textFieldDescripcion.getText().equals("")) {
					try {
						ArrayList<Genero> alGenero = new ArrayList<>();
						alGenero = per.listadoGeneros(textFieldDescripcion.getText());
						modelo = (DefaultTableModel) tableGeneros.getModel();
						if (alGenero.size() != 0) {
							for (Genero genero2 : listaGeneros) {
								
								Object[] fila = new Object[2];
								fila[0] = genero2.getIdgenero();
								fila[1] = genero2.getDescripcion();
	
								modelo.addRow(fila); // Aniadimos el array
	
							}
							btnBorrar.setVisible(true);
							btnGuardar.setVisible(true);
							btnModificar.setVisible(true);
						} else {
							int j = JOptionPane.showConfirmDialog(null,
									"No existe ning�n g�nero que contengan esas letras.\n�Deseas guardarlo?",
									"Atenci�n", JOptionPane.INFORMATION_MESSAGE);
							if (j == JOptionPane.YES_OPTION) {
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
								Genero g = new Genero(textFieldDescripcion.getText());
								per.guardarGenero(g);
								estadoInicial();
								// si pulsamos no
							}
							if (opcion == JOptionPane.NO_OPTION) {
								estadoInicial();
							}
							// Si pulsamos cancelar
							if (opcion == JOptionPane.CANCEL_OPTION) {
								estadoInicial();
							}
						} else {
							for (Genero genero2 : alGeneros) {

								fila[0] = genero2.getIdgenero();
								fila[1] = genero2.getDescripcion();

								modelo.addRow(fila); // Aniadimos el array

								tableGeneros.setModel(modelo);

								btnBorrar.setVisible(true);
								btnGuardar.setVisible(true);
								btnModificar.setVisible(true);
							}
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
=======
							btnBorrar.setVisible(true);
>>>>>>> master
							btnGuardar.setVisible(true);
							btnModificar.setVisible(true);
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
						}

					}
				} catch (Exception e) {
					System.err.println("Error al buscar el g�nero.");
					//e.printStackTrace();
				}

			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(291, 71, 114, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estadoInicial();
			}

		});
		btnCancelar.setBounds(291, 132, 117, 41);
		contentPane.add(btnCancelar);

		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setBounds(138, 82, 114, 19);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		textFieldDescripcion = new JTextField();
		textFieldDescripcion.setBounds(136, 133, 114, 19);
		contentPane.add(textFieldDescripcion);
		textFieldDescripcion.setColumns(10);

		// BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/guardar.png")));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java

					String filtro = textFieldDescripcion.getText().trim().replace("[ ]{2,}", " ");
					textFieldDescripcion.setText(filtro);
					genero = new Genero();

					// Vemos si el genero existe
					genero = per.buscarGenero(textFieldDescripcion.getText());

					//Si la descripcion del genero NO esta vacia
					if (!textFieldDescripcion.getText().equals("")) {
						// Si el genero no existe, le damos de alta
						if (genero == null) {

							// Si no es un genero seleccionado de la tabla se guarda
							if (textFieldID.getText().equals("")) {
								Genero GeneroAGuardar = new Genero(filtro);
								per.guardarGenero(GeneroAGuardar);
								JOptionPane.showMessageDialog(padre, "El genero " + textFieldDescripcion.getText()
										+ " se ha guardado correctamente");
								estadoInicial();

							}

						} else {
							JOptionPane.showMessageDialog(padre,
									"El g�nero " + textFieldDescripcion.getText() + " ya existe.");
							estadoInicial();

						}
					}else {
						JOptionPane.showMessageDialog(padre,
								"La descripci�n del g�nero no puede estar vacia");
=======
					// Este objeto genero esta null porque no existe el genero
					// genero = per.buscarGenero(filtro);
					genero = per.buscarGenero(filtro);

					if (genero == null) {
						Genero GeneroAGuardar = new Genero(filtro);
						per.guardarGenero(GeneroAGuardar);
						JOptionPane.showMessageDialog(padre, "El g�nero "+textFieldDescripcion.getText()+" se ha guardado correctamente");
						estadoInicial();
					} else {
						//AQUI MODIFICAMOS EL GENERO
						Genero GeneroModificar = new Genero(filtro);
						per.modificarGenero(GeneroModificar);
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
					}

				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(64, 396, 117, 41);
		contentPane.add(btnGuardar);

		/**
		 * BOTON MODIDIFICAR
		 */
		btnModificar = new JButton("Modificar");
		btnModificar.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/derecha.png")));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					String filtro = textFieldDescripcion.getText().trim().replace("[ ]{2,}", " ");
					int id = Integer.parseInt(textFieldID.getText());
					textFieldDescripcion.setText(filtro);
					genero = new Genero(id, filtro);

					if (genero != null) {

						per.modificarGenero(genero);
						JOptionPane.showMessageDialog(padre,
								"El genero " + textFieldDescripcion.getText() + " se ha modificado correctamente");
						estadoInicial();

					} else {
						JOptionPane.showMessageDialog(padre,
								"El g�nero " + textFieldDescripcion.getText() + " No se ha podido modificar.");
						estadoInicial();
					}

				} catch (Exception e) {
					
				}
			}
		});
		btnModificar.setVisible(false);
		btnModificar.setMnemonic('m');
		btnModificar.setBounds(291, 396, 117, 41);
		contentPane.add(btnModificar);

		/**
		 * BOTON BORRAR
		 */
		btnBorrar = new JButton("Borrar");
		btnBorrar.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/borrar.png")));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
				int fila = tableGeneros.getSelectedRow(); // Posicion en el al del genero seleccionado

				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIGenero.this, "Error al borrar g�nero", null,
							"Debe seleccionar un usuario.");
				} else {
					// Mensaje informativo al usuario, sobre el borrado del genero
					int opcion = JOptionPane.showConfirmDialog(null, "Se borrara el g�nero.\n�quieres borrarlo?",
							"Borrar g�nero", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						Genero genero = alGeneros.get(fila);
						borrarGenero(genero);
						limpiarTabla(tableGeneros);
						estadoInicial();

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}
=======
<<<<<<< HEAD
				String descripcion = textFieldDescripcion.getText();
				Genero generoBorrar = new Genero(descripcion);
				listaGeneros.clear();
=======
				String descripcion = String.valueOf(modelo.getValueAt(tableGeneros.getSelectedRow(),1));
				Genero genero = new Genero(descripcion);
>>>>>>> master
				try {
					per.borrarGenero(generoBorrar);
					estadoInicial();
				} catch (SQLException e) {
					utilidades.Utilidades.notificarError(padre, "Borrar g�nero", e, "Error al borrar el g�nero \n");
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java

				}

			}

		});
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('r');
		btnBorrar.setBounds(420, 396, 117, 41);
		contentPane.add(btnBorrar);

		JLabel lblListadoDeGneros = new JLabel("Listado de g\u00E9neros");
		lblListadoDeGneros.setBounds(25, 215, 156, 15);
		contentPane.add(lblListadoDeGneros);
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
=======
<<<<<<< HEAD
		modelo = (DefaultTableModel) tableGeneros.getModel();
=======
		
>>>>>>> master
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUIGenero.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(549, 396, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 243, 647, 138);
		contentPane.add(scrollPane);

		tableGeneros = new JTable();
		tableGeneros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableGeneros);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDGENERO", "DESCRIPCION" }); // Asignamos columnas de los campos al
																					// listado
		tableGeneros.setModel(modelo);

		tableGeneros.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableGeneros.getSelectedRow() >= 0) {
					mostrarGeneros();
				}
			}
		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnModificar.setVisible(false);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
		btnBuscar.setEnabled(true);
		textFieldID.setText("");
		textFieldDescripcion.setText("");
		textFieldDescripcion.grabFocus();
		// Limpiamos la tabla
		limpiarTabla(tableGeneros);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 */
	public void limpiarTabla(JTable tabla) {
<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java

		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
=======
		try {
<<<<<<< HEAD
			
=======
			//modelo = (DefaultTableModel) tabla.getModel();
>>>>>>> master
			int filas = tabla.getRowCount();
			for (int i = 0; filas > i; i++) {
				modelo.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
		}

		tabla.setModel(modelo);

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarGeneros() {
<<<<<<< HEAD
     
        Genero generoMostrar = new Genero((int) modelo.getValueAt(tableGeneros.getSelectedRow(), 0), (String) modelo.getValueAt(tableGeneros.getSelectedRow(), 1));
        // Mostramos los datos
        textFieldID.setText(generoMostrar.getIdgenero()+"");
        textFieldDescripcion.setText(generoMostrar.getDescripcion());
        limpiarTabla(tableGeneros);
=======

<<<<<<< HEAD:WSAccesoADatos/VideoClub/src/vista/GUIGenero.java
		System.out.println(alGeneros); // Mensaje para comprovar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar

		int fila = tableGeneros.getSelectedRow(); // Posicion en el al del genero seleccionado

		System.out.println("Fila: " + fila);

		Genero genero = alGeneros.get(fila);
		// Mostramos los datos
		textFieldID.setText(genero.getIdgenero() + "");
		textFieldDescripcion.setText(genero.getDescripcion());

	}

	/**
	 * Metodo para borrar el genero
	 * 
	 * @param genero
	 */
	private void borrarGenero(Genero genero) {

		try {
			per.borrarGenero(genero);
			limpiarTabla(tableGeneros);
			JOptionPane.showMessageDialog(null, "G�nero eliminado");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(GUIGenero.this, "", e, "Error al borrar al usuario");

		}

=======
		// Obtenemos el Model de la jTable para obtener los datos de la tabla
        Integer idGenero = (int)modelo.getValueAt(tableGeneros.getSelectedRow(), 0);
        String descripcion = (String) modelo.getValueAt(tableGeneros.getSelectedRow(), 1);
        
        Genero genero = new Genero(idGenero, descripcion);
        // Mostramos los datos
        textFieldID.setText(genero.getIdgenero()+"");
        textFieldDescripcion.setText(genero.getDescripcion());
        
>>>>>>> master
		
>>>>>>> master:WSAccesoADatos/VideoClub/src/interfaz/GUIGenero.java
	}

}
