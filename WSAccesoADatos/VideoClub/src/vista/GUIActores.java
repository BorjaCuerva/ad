package vista;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;

import videoClub.Actor;


import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUIActores extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldID;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldNombre;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnBorrar;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
	private ArrayList<Actor> alActores = new ArrayList<>();

	Actor actor;
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */

	private JTable tableActores;
	private JScrollPane scrollPane;
	private JTextField textFieldFechaNac;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIActores frame = new GUIActores();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIActores() {
		setResizable(false);
		setTitle("Actores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGneros = new JLabel("Actores");
		lblGneros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGneros.setBounds(291, 12, 102, 27);
		contentPane.add(lblGneros);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(25, 84, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(25, 120, 93, 15);
		contentPane.add(lblNewLabel_1);

		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelo = (DefaultTableModel) tableActores.getModel();

				btnBuscar.setEnabled(false);

				try {

					alActores= per.listadoActores(textFieldNombre.getText());

					Object[] fila = new Object[3];

					if (alActores.size() != 0) {
						for (Actor actor2 : alActores) {

							fila[0] = actor2.getIdactor();
							fila[1] = actor2.getNombre();
							fila[2] = actor2.getFechanac();

							modelo.addRow(fila); // Aniadimos el array

						}

						tableActores.setModel(modelo);

						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						// Si no hay ninguna coincidencia con los generos, da la opcion de guardarlo
					} else {

						if (!textFieldNombre.getText().equals("")) {
							int opcion = JOptionPane.showConfirmDialog(null,
									"No existe ning�n actor que contenga esas letras.\n�quieres guardarlo?",
									"Guardar actor", JOptionPane.INFORMATION_MESSAGE);
							// Si pulsamos si
							if (opcion == JOptionPane.YES_OPTION) {
								
								String fecha = textFieldFechaNac.getText();
								//Date fechaParse= null;
								SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
								
								try {
									java.util.Date n = formato.parse(fecha);
									Actor actorAGuardar = new Actor(textFieldNombre.getText(), n);
									per.guardarActor(actorAGuardar);
									JOptionPane.showMessageDialog(padre, "El actor " + textFieldNombre.getText()
											+ " se ha guardado correctamente");
									estadoInicial();
								} catch (ParseException e1) {
									utilidades.Utilidades.notificarError(padre, "Error en fecha", e1, "Introduce fecha valida (1999/01/01)");
									//e1.printStackTrace();
								}
								// si pulsamos no
							}
							if (opcion == JOptionPane.NO_OPTION) {
								estadoInicial();
							}
							// Si pulsamos cancelar
							if (opcion == JOptionPane.CANCEL_OPTION) {
								estadoInicial();
							}
						} else {
							for (Actor actor2 : alActores) {

								fila[0] = actor2.getIdactor();
								fila[1] = actor2.getNombre();
								fila[2] = actor2.getFechanac();

								modelo.addRow(fila); // Aniadimos el array

								tableActores.setModel(modelo);

								btnBorrar.setVisible(true);
								btnGuardar.setVisible(true);
								btnModificar.setVisible(true);
							}
						}

					}
				} catch (Exception e) {
					utilidades.Utilidades.notificarError(padre, "Error en actor", e, "Error al buscar el actor");
					//e.printStackTrace();
				}

			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(291, 71, 114, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				estadoInicial();
			}

		});
		btnCancelar.setBounds(291, 132, 117, 41);
		contentPane.add(btnCancelar);

		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setBounds(138, 82, 114, 19);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(138, 117, 114, 19);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		// BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/guardar.png")));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					String filtro = textFieldNombre.getText().trim().replace("[ ]{2,}", " ");
					textFieldNombre.setText(filtro);
					actor = new Actor();

					// Vemos si el actor existe
					actor = per.buscarActor(textFieldNombre.getText());

					//Si la descripcion del genero NO esta vacia
					if (!textFieldNombre.getText().equals("")) {
						
						if (textFieldFechaNac.getText().matches("\\\\d{4}-\\\\d{1,2}-\\\\d{1,2}")) {
							
							// Si el genero no existe, le damos de alta
							if (actor == null) {

								// Si no es un genero seleccionado de la tabla se guarda
								if (textFieldID.getText().equals("")) {
									
									String fecha = textFieldFechaNac.getText();
									//Date fechaParse= null;
									SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
									
									try {
										java.util.Date n = formato.parse(fecha);
										Actor actorAGuardar = new Actor(textFieldNombre.getText(), n);
										per.guardarActor(actorAGuardar);
										JOptionPane.showMessageDialog(padre, "El actor " + textFieldNombre.getText()
												+ " se ha guardado correctamente");
										estadoInicial();
									} catch (ParseException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}

								}

							} else {
								JOptionPane.showMessageDialog(padre,
										"El actor " + textFieldNombre.getText() + " ya existe.");
								estadoInicial();
							}
							
						}else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener /). Ejemplo [1999/10/20]");
							estadoInicial();							
						}
						

					}else {
						JOptionPane.showMessageDialog(padre,
								"El nombre del actor no puede estar vacio");
					}

				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(64, 396, 117, 41);
		contentPane.add(btnGuardar);

		/**
		 * BOTON MODIDIFICAR
		 */
		btnModificar = new JButton("Modificar");
		btnModificar.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/derecha.png")));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					String filtro = textFieldNombre.getText().trim().replace("[ ]{2,}", " ");
					int id = Integer.parseInt(textFieldID.getText());
					textFieldNombre.setText(filtro);
					Date formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd").parse(textFieldFechaNac.getText());
					
					java.sql.Date fecha = new java.sql.Date(formatoDelTexto.getTime());
					actor = new Actor(id,textFieldNombre.getText(),fecha);

					if (actor != null) {
						
						per.modificarActor(actor);
						JOptionPane.showMessageDialog(padre,
								"El actor " + textFieldNombre.getText() + " se ha modificado correctamente");
						estadoInicial();

					} else {
						JOptionPane.showMessageDialog(padre,
								"El actor " + textFieldNombre.getText() + " No se ha podido modificar.");
						estadoInicial();
					}

				} catch (Exception e) {
					utilidades.Utilidades.notificarError(GUIActores.this, "Error al modificar actor", null,
							"Debe seleccionar un actor.");
				}
				
			}
		});
		btnModificar.setVisible(false);
		btnModificar.setMnemonic('m');
		btnModificar.setBounds(291, 396, 117, 41);
		contentPane.add(btnModificar);

		/**
		 * BOTON BORRAR
		 */
		btnBorrar = new JButton("Borrar");
		btnBorrar.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/borrar.png")));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int fila = tableActores.getSelectedRow(); // Posicion en el al del genero seleccionado

				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUIActores.this, "Error al borrar actor", null,
							"Debe seleccionar un actor.");
				} else {
					// Mensaje informativo al usuario, sobre el borrado del genero
					int opcion = JOptionPane.showConfirmDialog(null, "Se borrara el actor.\n�quieres borrarlo?",
							"Borrar actor", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						Actor actor = alActores.get(fila);
						borrarActor(actor);
						limpiarTabla(tableActores);
						estadoInicial();

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}

		});
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('r');
		btnBorrar.setBounds(420, 396, 117, 41);
		contentPane.add(btnBorrar);

		JLabel lblListadoDeGneros = new JLabel("Listado de actores");
		lblListadoDeGneros.setBounds(25, 215, 108, 15);
		contentPane.add(lblListadoDeGneros);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUIActores.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(549, 396, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 243, 647, 138);
		contentPane.add(scrollPane);

		tableActores = new JTable();
		tableActores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableActores);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDACTOR", "NOMBRE","FECHANAC" }); // Asignamos columnas de los campos al
																					// listado
		tableActores.setModel(modelo);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento");
		lblFechaNacimiento.setBounds(25, 158, 93, 14);
		contentPane.add(lblFechaNacimiento);
		
		textFieldFechaNac = new JTextField();
		textFieldFechaNac.setBounds(138, 155, 114, 20);
		contentPane.add(textFieldFechaNac);
		textFieldFechaNac.setColumns(10);

		tableActores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableActores.getSelectedRow() >= 0) {
					mostrarActores();
				}
			}
		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnModificar.setVisible(false);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
		btnBuscar.setEnabled(true);
		textFieldID.setText("");
		textFieldNombre.setText("");
		textFieldFechaNac.setText("");
		textFieldNombre.grabFocus();
		// Limpiamos la tabla
		limpiarTabla(tableActores);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 */
	public void limpiarTabla(JTable tabla) {

		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
		}

		tabla.setModel(modelo);

	}

	
	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarActores() {

		System.out.println(alActores); // Mensaje para comprovar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar

		int fila = tableActores.getSelectedRow(); // Posicion en el al del genero seleccionado

		System.out.println("Fila: " + fila);

		Actor actor = alActores.get(fila);
		// Mostramos los datos
		textFieldID.setText(actor.getIdactor() + "");
		textFieldNombre.setText(actor.getNombre() + "");
		textFieldFechaNac.setText(actor.getFechanac() + "");

	}
	
	
	/**
	 * Metodo para borrar el genero
	 * 
	 * @param genero
	 */
	private void borrarActor(Actor actor) {

		try {
			per.borrarActor(actor);
			limpiarTabla(tableActores);
			JOptionPane.showMessageDialog(null, "Actor eliminado");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(GUIActores.this, "", e, "Error al borrar al usuario");

		}

	}
	
	
}
