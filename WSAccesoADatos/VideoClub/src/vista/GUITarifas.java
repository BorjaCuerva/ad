package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;

import videoClub.Actor;
import videoClub.Tarifa;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUITarifas extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldID;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldDescripcion;
	private JButton btnGuardar;
	private JButton btnModificar;
	private JButton btnBorrar;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
	private ArrayList<Tarifa> alTarifas = new ArrayList<>();

	Tarifa tarifa;
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */

	private JTable tableTarifas;
	private JScrollPane scrollPane;
	private JTextField textFieldDescuento;
	private JTextField textFieldFechaDesde;
	private JTextField textFieldFechaHasta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUITarifas frame = new GUITarifas();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUITarifas() {
		setResizable(false);
		setTitle("Tarifas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGneros = new JLabel("Tarifas");
		lblGneros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGneros.setBounds(291, 12, 102, 27);
		contentPane.add(lblGneros);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(25, 50, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Descripci\u00F3n");
		lblNewLabel_1.setBounds(25, 86, 93, 15);
		contentPane.add(lblNewLabel_1);

		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelo = (DefaultTableModel) tableTarifas.getModel();

				btnBuscar.setEnabled(false);

				try {

					alTarifas = per.listadoTarifas(textFieldDescripcion.getText());

					Object[] fila = new Object[5];

					if (alTarifas.size() != 0) {
						for (Tarifa tarifa2 : alTarifas) {

							fila[0] = tarifa2.getIdtarifa();
							fila[1] = tarifa2.getDescripcion();
							fila[2] = tarifa2.getPorcentajedto();
							fila[3] = tarifa2.getFechadesde();
							fila[4] = tarifa2.getFechahasta();

							modelo.addRow(fila); // Aniadimos el array

						}

						tableTarifas.setModel(modelo);

						btnBorrar.setVisible(true);
						btnGuardar.setVisible(true);
						btnModificar.setVisible(true);
						// Si no hay ninguna coincidencia con las tarifas, da la opcion de guardarlo
					} else {

						if (!textFieldDescripcion.getText().equals("")) {
							int opcion = JOptionPane.showConfirmDialog(null,
									"No existe ninguna tarifa que contenga esas letras.\n�quieres guardarla?",
									"Guardar tarifa", JOptionPane.INFORMATION_MESSAGE);
							// Si pulsamos si
							if (opcion == JOptionPane.YES_OPTION) {

								SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
								java.util.Date fechaDesde = null;
								java.util.Date fechaHasta = null;
								int descuento = 0;

								// Si la casilla descuento esta vacia, le asignamos 0
								if (textFieldDescuento.getText().equals("")) {
									descuento = 0;
									// Si esta rellena lo transformamos a string
								} else {

									if (textFieldDescuento.getText().matches("[0-9]*")) {
										descuento = Integer.parseInt(textFieldDescuento.getText());
									} else {
										JOptionPane.showMessageDialog(padre, "El descuento no es valido (ejemplo: 5)");
									}

								}

								// Si el campo fecha desde esta vacio, le damos una fecha por defecto
								if (textFieldFechaDesde.getText().equals("")) {

									fechaDesde = formato.parse("1970/1/1");

									// Si el usuario ha introducido datos, los comprobamos
								} else {

									if (textFieldFechaDesde.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

										fechaDesde = formato.parse(textFieldFechaDesde.getText());

									} else {
										JOptionPane.showMessageDialog(padre,
												"La fecha introducida no es valida(no puede contener /). Ejemplo [1999-10-20]");
									}

								}

								// Si el campo fecha hasta esta vacio, le damos una fecha por defecto
								if (textFieldFechaHasta.getText().equals("")) {

									fechaHasta = formato.parse("2050/1/1");

									// Si el usuario ha introducido datos, los comprobamos
								} else {

									if (textFieldFechaHasta.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

										fechaHasta = formato.parse(textFieldFechaHasta.getText());

									} else {
										JOptionPane.showMessageDialog(padre,
												"La fecha introducida no es valida(no puede contener /). Ejemplo [1999-10-20]");
									}

								}

								// comprobamos si la fecha hasta es posterior a la fecha desde
								/**
								 * CompareTo
								 * 
								 * fechas iguales devuelve 0 fecha desde es anterior a la fecha hasta devuelve
								 * <0 fecha desde es posterior a la fecha hasta devuelve >0
								 * 
								 */

								if (fechaDesde.compareTo(fechaHasta) >= 0) {
									JOptionPane.showMessageDialog(padre,
											"La fecha desde no puede ser igual o superior a la fecha hasta");
									estadoInicial();

									// Seguimos guardando la tarifa
								} else {

									Tarifa tarifaAGuardar = new Tarifa(textFieldDescripcion.getText(), descuento,
											fechaDesde, fechaHasta);

									per.guardarTarifa(tarifaAGuardar);
									JOptionPane.showMessageDialog(padre, "La tarifa " + textFieldDescripcion.getText()
											+ " se ha guardado correctamente");
									estadoInicial();
								}

								if (opcion == JOptionPane.NO_OPTION) {
									estadoInicial();
								}
								// Si pulsamos cancelar
								if (opcion == JOptionPane.CANCEL_OPTION) {
									estadoInicial();
								}
							} else {
								for (Tarifa tarifa2 : alTarifas) {

									fila[0] = tarifa2.getIdtarifa();
									fila[1] = tarifa2.getDescripcion();
									fila[2] = tarifa2.getPorcentajedto();
									fila[3] = tarifa2.getFechadesde();
									fila[4] = tarifa2.getFechahasta();

									modelo.addRow(fila); // Aniadimos el array

									tableTarifas.setModel(modelo);

									btnBorrar.setVisible(true);
									btnGuardar.setVisible(true);
									btnModificar.setVisible(true);
								}
							}

						}

					}
				} catch (Exception e) {
					utilidades.Utilidades.notificarError(padre, "Error en tarifa", e, "Error al buscar la tarifa");
					e.printStackTrace();
				}

			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(291, 71, 114, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				estadoInicial();
			}

		});
		btnCancelar.setBounds(291, 132, 117, 41);
		contentPane.add(btnCancelar);

		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setBounds(138, 48, 114, 19);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		textFieldDescripcion = new JTextField();
		textFieldDescripcion.setBounds(138, 83, 114, 19);
		contentPane.add(textFieldDescripcion);
		textFieldDescripcion.setColumns(10);

		// BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/guardar.png")));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					String filtro = textFieldDescripcion.getText().trim().replace("[ ]{2,}", " ");
					textFieldDescripcion.setText(filtro);
					tarifa = new Tarifa();

					// Vemos si la tarifa existe
					tarifa = per.buscarTarifa(textFieldDescripcion.getText());

					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date fechaDesde = null;
					java.util.Date fechaHasta = null;
					int descuento = 0;

					// Si la casilla descuento esta vacia, le asignamos 0
					if (textFieldDescuento.getText().equals("")) {
						descuento = 0;
						// Si esta rellena lo transformamos a string
					} else {

						if (textFieldDescuento.getText().matches("[0-9]*")) {
							descuento = Integer.parseInt(textFieldDescuento.getText());
						} else {
							JOptionPane.showMessageDialog(padre, "El descuento no es valido (ejemplo: 5)");
						}

					}

					// Si el campo fecha desde esta vacio, le damos una fecha por defecto
					if (textFieldFechaDesde.getText().equals("")) {

						fechaDesde = formato.parse("1970-1-1");

						// Si el usuario ha introducido datos, los comprobamos
					} else {

						if (textFieldFechaDesde.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

							fechaDesde = formato.parse(textFieldFechaDesde.getText());

						} else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener /). Ejemplo [20-10-1999]");
						}

					}

					// Si el campo fecha hasta esta vacio, le damos una fecha por defecto
					if (textFieldFechaHasta.getText().equals("")) {

						fechaHasta = formato.parse("2050-1-1");

						// Si el usuario ha introducido datos, los comprobamos
					} else {

						if (textFieldFechaHasta.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

							fechaHasta = formato.parse(textFieldFechaHasta.getText());

						} else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener /). Ejemplo [20-10-1999]");
						}

					}

					// comprobamos si la fecha hasta es posterior a la fecha desde

					/**
					 * CompareTo
					 * 
					 * fechas iguales devuelve 0 fecha desde es anterior a la fecha hasta devuelve
					 * <0 fecha desde es posterior a la fecha hasta devuelve >0
					 * 
					 */

					if (fechaDesde.compareTo(fechaHasta) >= 0) {
						JOptionPane.showMessageDialog(padre,
								"La fecha desde no puede ser igual o superior a la fecha hasta");

						// Seguimos guardando la tarifa
					} else {

						// Si la descripcion del genero NO esta vacia
						if (!textFieldDescripcion.getText().equals("")) {

							// Si el genero no existe, le damos de alta
							if (tarifa == null) {

								// Si no es un genero seleccionado de la tabla se guarda
								if (textFieldID.getText().equals("")) {

									Tarifa tarifaAGuardar = new Tarifa(textFieldDescripcion.getText(), descuento,
											fechaDesde, fechaHasta);
									per.guardarTarifa(tarifaAGuardar);
									JOptionPane.showMessageDialog(padre, "La tarifa " + textFieldDescripcion.getText()
											+ " se ha guardado correctamente");
									estadoInicial();

								}

							} else {
								JOptionPane.showMessageDialog(padre,
										"La tarifa " + textFieldDescripcion.getText() + " ya existe.");
								estadoInicial();
							}

						} else {
							JOptionPane.showMessageDialog(padre, "La descripcion de la tarifa no puede estar vacia");
						}

					}

				} catch (SQLException | ParseException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(59, 407, 117, 41);
		contentPane.add(btnGuardar);

		/**
		 * BOTON MODIDIFICAR
		 */
		btnModificar = new JButton("Modificar");
		btnModificar.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/derecha.png")));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					String filtro = textFieldDescripcion.getText().trim().replace("[ ]{2,}", " ");
					int id = Integer.parseInt(textFieldID.getText());
					textFieldDescripcion.setText(filtro);
					
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date fechaDesde = null;
					java.util.Date fechaHasta = null;
					int descuento = 0;

					// Si la casilla descuento esta vacia, le asignamos 0
					if (textFieldDescuento.getText().equals("")) {
						descuento = 0;
						// Si esta rellena lo transformamos a string
					} else {

						if (textFieldDescuento.getText().matches("[0-9]*")) {
							descuento = Integer.parseInt(textFieldDescuento.getText());
						} else {
							JOptionPane.showMessageDialog(padre, "El descuento no es valido (ejemplo: 5)");
						}

					}

					// Si el campo fecha desde esta vacio, le damos una fecha por defecto
					if (textFieldFechaDesde.getText().equals("")) {

						fechaDesde = formato.parse("1970-1-1");

						// Si el usuario ha introducido datos, los comprobamos
					} else {

						if (textFieldFechaDesde.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

							fechaDesde = formato.parse(textFieldFechaDesde.getText());

						} else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener -). Ejemplo [20/10/1999]");
						}

					}

					// Si el campo fecha hasta esta vacio, le damos una fecha por defecto
					if (textFieldFechaHasta.getText().equals("")) {

						fechaHasta = formato.parse("2050-1-1");

						// Si el usuario ha introducido datos, los comprobamos
					} else {

						if (textFieldFechaHasta.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

							fechaHasta = formato.parse(textFieldFechaHasta.getText());

						} else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener -). Ejemplo [20/10/1999]");
						}

					}

					// comprobamos si la fecha hasta es posterior a la fecha desde

					/**
					 * CompareTo
					 * 
					 * fechas iguales devuelve 0 fecha desde es anterior a la fecha hasta devuelve
					 * <0 fecha desde es posterior a la fecha hasta devuelve >0
					 * 
					 */

					if (fechaDesde.compareTo(fechaHasta) >= 0) {
						JOptionPane.showMessageDialog(padre,
								"La fecha desde no puede ser igual o superior a la fecha hasta");

						// Seguimos guardando la tarifa
					} else {

						tarifa = new Tarifa(Integer.parseInt(textFieldID.getText()), textFieldDescripcion.getText(), descuento, fechaDesde, fechaHasta);

						if (tarifa != null) {

							per.modificarTarifa(tarifa);
							JOptionPane.showMessageDialog(padre,
									"La tarifa " + textFieldDescripcion.getText() + " se ha modificado correctamente");
							estadoInicial();

						} else {
							JOptionPane.showMessageDialog(padre,
									"La tarifa " + textFieldDescripcion.getText() + " No se ha podido modificar.");
							estadoInicial();
						}
						
					}
					
				} catch (Exception e) {
					utilidades.Utilidades.notificarError(GUITarifas.this, "Error al modificar actor", null,
							"Debe seleccionar un actor.");
					//e.printStackTrace();
				}

			}
		});
		btnModificar.setVisible(false);
		btnModificar.setMnemonic('m');
		btnModificar.setBounds(286, 407, 117, 41);
		contentPane.add(btnModificar);

		/**
		 * BOTON BORRAR
		 */
		btnBorrar = new JButton("Borrar");
		btnBorrar.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/borrar.png")));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int fila = tableTarifas.getSelectedRow(); // Posicion en el al del genero seleccionado

				if (fila == -1) {
					utilidades.Utilidades.notificarError(GUITarifas.this, "Error al borrar tarifa", null,
							"Debe seleccionar una tarifa.");
				} else {
					// Mensaje informativo al usuario, sobre el borrado del genero
					int opcion = JOptionPane.showConfirmDialog(null, "Se borrara la tarifa.\n�quieres borrarlo?",
							"Borrar tarifa", JOptionPane.INFORMATION_MESSAGE);
					// Si pulsamos si
					if (opcion == JOptionPane.YES_OPTION) {
						
						Tarifa tarifa = alTarifas.get(fila);
						borrarTarifa(tarifa);
						limpiarTabla(tableTarifas);
						estadoInicial();

						// si pulsamos no
					}
					if (opcion == JOptionPane.NO_OPTION) {
						estadoInicial();
					}
					// Si pulsamos cancelar
					if (opcion == JOptionPane.CANCEL_OPTION) {
						estadoInicial();
					}

				}

			}

		});
		btnBorrar.setVisible(false);
		btnBorrar.setMnemonic('r');
		btnBorrar.setBounds(415, 407, 117, 41);
		contentPane.add(btnBorrar);

		JLabel lblListadoDeGneros = new JLabel("Listado de tarifas");
		lblListadoDeGneros.setBounds(25, 231, 108, 15);
		contentPane.add(lblListadoDeGneros);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUITarifas.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(544, 407, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 257, 647, 138);
		contentPane.add(scrollPane);

		tableTarifas = new JTable();
		tableTarifas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tableTarifas);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDTARIFA", "DESCRIPCION", "%DTO", "FECHADESDE", "FECHAHASTA" }); // Asignamos
																														// columnas
																														// de
																														// los
		// campos al
		// listado
		tableTarifas.setModel(modelo);

		JLabel lblFechaNacimiento = new JLabel("% DTO:");
		lblFechaNacimiento.setBounds(25, 124, 93, 14);
		contentPane.add(lblFechaNacimiento);

		textFieldDescuento = new JTextField();
		textFieldDescuento.setBounds(138, 121, 114, 20);
		contentPane.add(textFieldDescuento);
		textFieldDescuento.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Fecha desde:");
		lblNewLabel_2.setBounds(25, 159, 93, 14);
		contentPane.add(lblNewLabel_2);

		textFieldFechaDesde = new JTextField();
		textFieldFechaDesde.setBounds(138, 156, 114, 20);
		contentPane.add(textFieldFechaDesde);
		textFieldFechaDesde.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Fecha hasta");
		lblNewLabel_3.setBounds(25, 190, 93, 14);
		contentPane.add(lblNewLabel_3);

		textFieldFechaHasta = new JTextField();
		textFieldFechaHasta.setBounds(138, 187, 114, 20);
		contentPane.add(textFieldFechaHasta);
		textFieldFechaHasta.setColumns(10);

		tableTarifas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableTarifas.getSelectedRow() >= 0) {
					mostrarActores();
				}
			}
		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnModificar.setVisible(false);
		btnBorrar.setVisible(false);
		btnGuardar.setVisible(false);
		btnBuscar.setEnabled(true);
		textFieldID.setText("");
		textFieldDescripcion.setText("");
		textFieldDescuento.setText("");
		textFieldFechaDesde.setText("");
		textFieldFechaHasta.setText("");
		textFieldDescripcion.grabFocus();
		// Limpiamos la tabla
		limpiarTabla(tableTarifas);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 */
	public void limpiarTabla(JTable tabla) {

		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
		}

		tabla.setModel(modelo);

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarActores() {

		System.out.println(alTarifas); // Mensaje para comprovar el contenido del objeto seleccionado de la tabla,
										// borrar al acabar

		int fila = tableTarifas.getSelectedRow(); // Posicion en el al del genero seleccionado

		System.out.println("Fila: " + fila);

		Tarifa tarifa = alTarifas.get(fila);
		// Mostramos los datos
		textFieldID.setText(tarifa.getIdtarifa() + "");
		textFieldDescripcion.setText(tarifa.getDescripcion() + "");
		textFieldDescuento.setText(tarifa.getPorcentajedto() + "");
		textFieldFechaDesde.setText(tarifa.getFechadesde() + "");
		textFieldFechaHasta.setText(tarifa.getFechahasta() + "");

	}

	/**
	 * Metodo para borrar el genero
	 * 
	 * @param genero
	 */
	private void borrarTarifa(Tarifa tarifa) {

		try {
			per.borrarTarifa(tarifa);
			limpiarTabla(tableTarifas);
			JOptionPane.showMessageDialog(null, "Tarifa eliminada");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(GUITarifas.this, "", e, "Error al borrar la tarifa");

		}

	}
}
