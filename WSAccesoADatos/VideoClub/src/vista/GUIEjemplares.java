package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bbdd.Persistencia;
import bbdd.PersistenciaMYSQL;
import utilidades.Utilidades;
import videoClub.Actor;
import videoClub.Ejemplar;
import videoClub.Genero;
import videoClub.Pelicula;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class GUIEjemplares extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIDEjemplar;
	private JButton btnBuscar;
	private JButton btnCancelar;
	private JTextField textFieldFechaCompra;

	static PersistenciaMYSQL bdmysql = null;
	static javax.swing.JFrame padre;
	boolean generoExiste;
	// Objeto persistencia, este objeto lo vamos a usar para llamar a todos los
	// metodos
	static Persistencia per = Main.per;
	private JButton btnSalir;
	private ArrayList<Pelicula> alPeliculas = new ArrayList<>();
	private ArrayList<Ejemplar> alEjemplares = new ArrayList<>();

	Genero genero;
	boolean fechaCorrecta = false;
	/**
	 * Creamos un array, ya que para meter datos en un jTable hay que hacerlo
	 * mediante un array
	 */

	private JTable tablePeliculas;
	private JScrollPane scrollPane;
	private JTextField textFieldTituloPelicula;
	private JLabel lblIdPelicula;
	private JTextField textFieldIDPelicula;
	private JTable tableEjemplares;
	private JButton btnAniadirEjemplar;
	private JButton btnQuitarEjemplar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIEjemplares frame = new GUIEjemplares();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIEjemplares() {
		setResizable(false);
		setTitle("Ejemplares");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 688, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGneros = new JLabel("Ejemplares");
		lblGneros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblGneros.setBounds(291, 12, 102, 27);
		contentPane.add(lblGneros);

		JLabel lblNewLabel = new JLabel("ID Ejemplar:");
		lblNewLabel.setBounds(445, 75, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Fecha Compra");
		lblNewLabel_1.setBounds(445, 104, 101, 15);
		contentPane.add(lblNewLabel_1);

		// BOTON BUSCAR
		btnBuscar = new JButton("Buscar Pelicula");
		btnBuscar.setIcon(new ImageIcon(GUIEjemplares.class.getResource("/iconos/aceptar.png")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				textFieldTituloPelicula.setEnabled(false);
				DefaultTableModel modelo = (DefaultTableModel) tablePeliculas.getModel();

				btnBuscar.setEnabled(false);

				btnBuscar.setEnabled(false);

				try {

					alPeliculas = per.listadoPeliculas(textFieldTituloPelicula.getText());

					Object[] fila = new Object[4];

					if (alPeliculas.size() != 0) {
						for (Pelicula pelicula2 : alPeliculas) {

							fila[0] = pelicula2.getIdpelicula();
							fila[1] = pelicula2.getTitulo();
							fila[2] = pelicula2.getSinopsis();
							fila[3] = pelicula2.getPreciodia();

							modelo.addRow(fila); // Aniadimos el array

						}

						tablePeliculas.setModel(modelo);

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		btnBuscar.setMnemonic('b');
		btnBuscar.setBounds(10, 310, 191, 41);
		contentPane.add(btnBuscar);

		// BOTON CANCELAR
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(GUIEjemplares.class.getResource("/iconos/Cancelar.png")));
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estadoInicial();
			}

		});
		btnCancelar.setBounds(10, 362, 191, 41);
		contentPane.add(btnCancelar);

		textFieldIDEjemplar = new JTextField();
		textFieldIDEjemplar.setEditable(false);
		textFieldIDEjemplar.setBounds(558, 73, 112, 19);
		contentPane.add(textFieldIDEjemplar);
		textFieldIDEjemplar.setColumns(10);

		textFieldFechaCompra = new JTextField();
		textFieldFechaCompra.setBounds(556, 101, 114, 19);
		contentPane.add(textFieldFechaCompra);
		textFieldFechaCompra.setColumns(10);

		JLabel lblListadoDeGneros = new JLabel("Listado de pel\u00EDculas");
		lblListadoDeGneros.setBounds(10, 113, 143, 15);
		contentPane.add(lblListadoDeGneros);

		btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(GUIEjemplares.class.getResource("/iconos/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main i = new Main();
				i.setEnabled(true);
				// Cerramos la ventana actual
				dispose();

			}
		});
		btnSalir.setMnemonic('s');
		btnSalir.setBounds(549, 396, 117, 41);
		contentPane.add(btnSalir);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 141, 255, 138);
		contentPane.add(scrollPane);

		tablePeliculas = new JTable();
		tablePeliculas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tablePeliculas);

		DefaultTableModel modelo = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(new Object[] { "IDPELICULA", "TITULO" }); // Asignamos columnas de los campos al
																				// listado
		tablePeliculas.setModel(modelo);

		JLabel lblPelicula = new JLabel("Pelicula:");
		lblPelicula.setBounds(10, 76, 102, 14);
		contentPane.add(lblPelicula);

		textFieldTituloPelicula = new JTextField();
		textFieldTituloPelicula.setBounds(122, 73, 143, 20);
		contentPane.add(textFieldTituloPelicula);
		textFieldTituloPelicula.setColumns(10);

		lblIdPelicula = new JLabel("ID Pelicula:");
		lblIdPelicula.setBounds(146, 113, 70, 15);
		contentPane.add(lblIdPelicula);

		textFieldIDPelicula = new JTextField();
		textFieldIDPelicula.setEditable(false);
		textFieldIDPelicula.setColumns(10);
		textFieldIDPelicula.setBounds(219, 110, 46, 19);
		contentPane.add(textFieldIDPelicula);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(420, 141, 255, 138);
		contentPane.add(scrollPane_1);

		/**
		 * Mouse Clicked de la tabla ejemplares
		 */
		tableEjemplares = new JTable();
		tableEjemplares.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (tablePeliculas.getSelectedRow() >= 0) {

					mostrarEjemplares();
				}

			}

		});
		tableEjemplares.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(tableEjemplares);

		DefaultTableModel modeloEjemplares = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modeloEjemplares.setColumnIdentifiers(new Object[] { "IDEJEMPLAR", "IDPELICULA" }); // Asignamos columnas de los
																							// campos al
		// listado
		tableEjemplares.setModel(modeloEjemplares);

		/**
		 * BOTON ANIADIR EJEMPLARES
		 */
		btnAniadirEjemplar = new JButton("Aniadir");
		btnAniadirEjemplar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				java.util.Date fechaCompra = null;
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

					// Si el campo fecha esta vacio, le damos una fecha por defecto
					if (textFieldFechaCompra.getText().equals("")) {

						fechaCompra = null;
						fechaCorrecta = true; //Cuando la fecha sea correcta, guardamos el ejemplar

						// Si el usuario ha introducido datos, los comprobamos
					} else {

						if (textFieldFechaCompra.getText().matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {

							try {
								
								fechaCompra = formato.parse(textFieldFechaCompra.getText());
								fechaCorrecta = true;//Cuando la fecha sea correcta, guardamos el ejemplar
								
							} catch (ParseException e1) {
								e1.printStackTrace();
							}

						} else {
							JOptionPane.showMessageDialog(padre,
									"La fecha introducida no es valida(no puede contener /). Ejemplo [1999-10-20]");
							fechaCorrecta = false; 
						}
	

					}
										
					//Si la fecha introducida es correcta (formato yyyy-MM-dd o "" guardamos el ejemplar)
					if (fechaCorrecta) {
						Ejemplar ejemplar = new Ejemplar(fechaCompra);
						Pelicula pelicula = new Pelicula(Integer.parseInt(textFieldIDPelicula.getText()));
						try {
							per.aniadirEjemplar(ejemplar, pelicula);
							JOptionPane.showMessageDialog(padre, "Ejemplar creado correctamente.");
							estadoInicial();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(padre, "No se ha podido guardar el ejemplar.");
							e1.printStackTrace();
						}
					}

			}
		});
		btnAniadirEjemplar.setEnabled(false);
		btnAniadirEjemplar.setBounds(445, 290, 89, 41);
		contentPane.add(btnAniadirEjemplar);

		
		/**
		 * BOTON QUITAR EJEMPLAR
		 */
		btnQuitarEjemplar = new JButton("Quitar");
		btnQuitarEjemplar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					
					int fila = tableEjemplares.getSelectedRow(); // Posicion en el al del genero seleccionado�
					
					if (fila == -1) {
						utilidades.Utilidades.notificarError(GUIEjemplares.this, "Error al quitar ejemplar", null,
								"Debe seleccionar un ejemplar.");
					} else {
						
						int opcion = JOptionPane.showConfirmDialog(null,
								"�Est�s seguro de que quieres borrar el ejemplar?.\n",
								"eliminar ejemplar", JOptionPane.INFORMATION_MESSAGE);
						// Si pulsamos si
						if (opcion == JOptionPane.YES_OPTION) {

							Ejemplar ejemplar = new Ejemplar(Integer.parseInt(textFieldIDEjemplar.getText()));
							per.borrarEjemplar(ejemplar);
							estadoInicial();
							// si pulsamos no
						}
						if (opcion == JOptionPane.NO_OPTION) {
							estadoInicial();
						}
						// Si pulsamos cancelar
						if (opcion == JOptionPane.CANCEL_OPTION) {
							estadoInicial();
						}

					}

				} catch (SQLException e1) {
					utilidades.Utilidades.notificarError(padre, "Borrado ejemplar", e1, "Error al borrar ejemplar");
					e1.printStackTrace();
				}
				
				
			}
		});
		btnQuitarEjemplar.setEnabled(false);
		btnQuitarEjemplar.setBounds(566, 290, 89, 41);
		contentPane.add(btnQuitarEjemplar);

		/**
		 * MOUSE CLICKED TABLE PELICULAS
		 */
		JLabel lblNewLabel_2 = new JLabel("Listado Ejemplares:");
		lblNewLabel_2.setBounds(420, 126, 126, 14);
		contentPane.add(lblNewLabel_2);

		tablePeliculas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tablePeliculas.getSelectedRow() >= 0) {
					limpiarTablaEjemplar(tableEjemplares);

					mostrarPeliculas();

					btnAniadirEjemplar.setEnabled(true);
					btnQuitarEjemplar.setEnabled(true);

					int idPelicula = Integer.parseInt(textFieldIDPelicula.getText());

					Pelicula pelicula = new Pelicula(idPelicula);

					listarEjemplares(pelicula);

				}
			}

		});

	}

	/**
	 * Metodo que deja la aplicacion en el estado inicial
	 */
	private void estadoInicial() {

		btnBuscar.setEnabled(true);
		textFieldIDEjemplar.setText("");
		textFieldFechaCompra.setText("");
		textFieldIDEjemplar.setText("");
		textFieldIDPelicula.setText("");
		textFieldTituloPelicula.setText("");
		textFieldIDPelicula.grabFocus();
		textFieldTituloPelicula.setEnabled(true);
		btnAniadirEjemplar.setEnabled(false);
		btnQuitarEjemplar.setEnabled(false);
		// Limpiamos la tabla
		limpiarTabla(tablePeliculas);
		limpiarTabla(tableEjemplares);

	}

	/**
	 * Metodo para limpiar el jTable codigo sacado de la pagina javerosAnonimos
	 * 
	 * @param tabla
	 * @param tableEjemplares
	 */
	public void limpiarTabla(JTable tabla) {

		// Limpiamos tabla peliculas
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

		for (int i = 0; tabla.getRowCount() > i; i++) {
			modelo.removeRow(i);
			i--;
		}

		tabla.setModel(modelo);

	}

	private void limpiarTablaEjemplar(JTable tableEjemplares) {

		// Limpiamos tabla ejemplares
		DefaultTableModel modeloEjemplares = (DefaultTableModel) tableEjemplares.getModel();

		for (int i = 0; tableEjemplares.getRowCount() > i; i++) {
			modeloEjemplares.removeRow(i);
			i--;
		}

		tableEjemplares.setModel(modeloEjemplares);

	}

	/**
	 * Metodo que se ejecuta al hacer clic con el raton en la JTable
	 */
	private void mostrarPeliculas() {

		System.out.println(alPeliculas); // Mensaje para comprobar el contenido del objeto seleccionado de la tabla,
											// borrar al acabar

		int fila = tablePeliculas.getSelectedRow(); // Posicion en el al del cliente seleccionado

		System.out.println("Fila: " + fila);

		Pelicula pelicula = alPeliculas.get(fila);
		// Mostramos los datos
		textFieldIDPelicula.setText(pelicula.getIdpelicula() + "");
		textFieldTituloPelicula.setText(pelicula.getTitulo() + "");

	}

	private void mostrarEjemplares() {

		System.out.println(alEjemplares); // Mensaje para comprobar el contenido del objeto seleccionado de la tabla,
		// borrar al acabar

		int fila = tableEjemplares.getSelectedRow(); // Posicion en el al del cliente seleccionado

		System.out.println("Fila: " + fila);

		Ejemplar ejemplar = alEjemplares.get(fila);
		// Mostramos los datos
		textFieldIDEjemplar.setText(ejemplar.getIdejemplar() + "");
		textFieldFechaCompra.setText(ejemplar.getFechacompra() + "");

	}

	private void listarEjemplares(Pelicula pelicula) {

		DefaultTableModel modeloEjemplares2 = (DefaultTableModel) tableEjemplares.getModel();

		try {

			alEjemplares = per.listadoEjemplares(pelicula);

			Object[] fila = new Object[4];

			if (alEjemplares.size() != 0) {
				for (Ejemplar ejemplar2 : alEjemplares) {

					fila[0] = ejemplar2.getIdejemplar();
					fila[1] = ejemplar2.getPelicula().getIdpelicula();

					modeloEjemplares2.addRow(fila); // Aniadimos el array

				}

				tableEjemplares.setModel(modeloEjemplares2);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
