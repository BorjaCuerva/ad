package bbdd;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.ResultSetMetaData;

import videoClub.Actor;
import videoClub.Cliente;
import videoClub.Ejemplar;
import videoClub.Genero;
import videoClub.Pelicula;
import videoClub.Tarifa;
import vista.GUIActores;

public class PersistenciaMYSQL implements Persistencia {

	Connection con; // Objeto conexion

	///////////////////////////////////////////////// CRUD GENEROS
	///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	/**
	 * Constructor que conecta con la BBDD
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */
	public PersistenciaMYSQL(String tipoPersistencia, String servidor, String baseDatos, String usuario,
			String password) throws SQLException, FileNotFoundException, IOException, ClassNotFoundException {

		// Comprobamos el driver
		Class.forName("com.mysql.jdbc.Driver");

		// Conectamos a la BBDD mysql
		// con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root",
		// "manager");
		con = DriverManager.getConnection("jdbc:" + tipoPersistencia + "://" + servidor + "/" + baseDatos, usuario,
				password);
	}

	public Genero buscarGenero(String filtro) throws SQLException {

		Genero genero = null;
		Integer idgenero;
		String descripcion;

		// String sql = "SELECT * FROM genero WHERE descripcion= '" + "%" + filtro + "%"
		// + "'";
		String sql = "SELECT * FROM genero WHERE descripcion= '" + filtro + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// Guardamos en un objeto de tipo genero todos los datos

		if (rs.next()) {
			genero = new Genero(Integer.valueOf(rs.getInt("idgenero")), rs.getString("descripcion"));
		}

		return genero;

	}

	public void guardarGenero(Genero genero) throws SQLException {

		String sqlInsert = "INSERT INTO genero (descripcion) VALUES (?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setString(1, genero.getDescripcion());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	/**
	 * Metodo para guardar un listado de generos en un arrayList.
	 */
	public ArrayList<Genero> listadoGeneros(String filtro) throws SQLException {

		Genero generoAGuardar = new Genero();
		ArrayList<Genero> listaGeneros = new ArrayList<>();
		String sql = "SELECT * FROM genero WHERE descripcion like '" + "%" + filtro + "%" + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {
			generoAGuardar = new Genero(rs.getInt("idgenero"), rs.getString("Descripcion"));
			listaGeneros.add(generoAGuardar);
		}

		rs.close();
		st.close();
		return listaGeneros;

	}

	@Override
	/**
	 * Metodo para borrar un genero de la BBDD
	 */
	public void borrarGenero(Genero genero) throws SQLException {

		if (genero != null) {
			String sqlDelete = "DELETE FROM genero WHERE descripcion = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setString(1, genero.getDescripcion());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}
	}

	@Override
	public void modificarGenero(Genero genero) throws SQLException {

		if (genero != null) {
			String sql = "UPDATE genero SET descripcion=? WHERE idgenero = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, genero.getDescripcion());
			preparedStatement.setInt(2, genero.getIdgenero());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}

	}

	///////////////////////////////////////////////// CRUD CLIENTES
	///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	@Override
	public Cliente buscarCliente(String filtro) throws SQLException {
		Cliente cliente = null;

		String sql = "SELECT * FROM cliente WHERE email= '" + filtro + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// Guardamos en un objeto de tipo genero todos los datos

		if (rs.next()) {

			cliente = new Cliente(Integer.valueOf(rs.getInt("idcliente")), rs.getString("nombre"),
					rs.getString("apellidos"), rs.getString("email"));
		}

		return cliente;
	}

	@Override
	public void guardarCliente(Cliente cliente) throws SQLException {

		String sqlInsert = "INSERT INTO cliente (nombre,apellidos,email) VALUES (?,?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setString(1, cliente.getNombre());
		preparedStatement.setString(2, cliente.getApellidos());
		preparedStatement.setString(3, cliente.getEmail());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public ArrayList<Cliente> listadoClientes(String filtro) throws SQLException {

		Cliente cliente = new Cliente();
		ArrayList<Cliente> listaClientes = new ArrayList<>();
		String sql = "SELECT * FROM cliente WHERE email like '" + "%" + filtro + "%" + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {
			cliente = new Cliente(Integer.valueOf(rs.getInt("idcliente")), rs.getString("nombre"),
					rs.getString("apellidos"), rs.getString("email"));

			listaClientes.add(cliente);
		}

		return listaClientes;
	}

	@Override
	public void borrarCliente(Cliente cliente) throws SQLException {

		if (cliente != null) {
			String sqlDelete = "DELETE FROM cliente WHERE email = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setString(1, cliente.getEmail());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}

	}

	@Override
	public void modificarCliente(Cliente cliente) throws SQLException {

		String sql = "UPDATE cliente SET nombre=?, apellidos=?, email=? WHERE idcliente = ?";
		PreparedStatement preparedStatement = con.prepareStatement(sql);
		preparedStatement.setString(1, cliente.getNombre());
		preparedStatement.setString(2, cliente.getApellidos());
		preparedStatement.setString(3, cliente.getEmail());
		preparedStatement.setInt(4, cliente.getIdcliente());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	///////////////////////////////////////////////// CRUD ACTORES
	///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	@Override
	public Actor buscarActor(String filtro) throws SQLException {
		Actor actor = null;

		String sql = "SELECT * FROM actor WHERE nombre= '" + filtro + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// Guardamos en un objeto de tipo genero todos los datos

		if (rs.next()) {

			actor = new Actor(rs.getInt("idactor"), rs.getString("nombre"), rs.getDate("fechanac"));
		}

		return actor;
	}

	@Override
	public void guardarActor(Actor actor) throws SQLException {

		java.sql.Date fechaNac = new java.sql.Date(actor.getFechanac().getTime()); // Para pasar la fecha de String a
																					// Date

		String sqlInsert = "INSERT INTO actor (nombre,fechanac) VALUES (?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setString(1, actor.getNombre());
		preparedStatement.setDate(2, fechaNac);
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public ArrayList<Actor> listadoActores(String filtro) throws SQLException {

		Actor actor = new Actor();
		ArrayList<Actor> listaActores = new ArrayList<>();
		String sql = "SELECT * FROM actor WHERE nombre like '" + "%" + filtro + "%" + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {
			actor = new Actor(rs.getInt("idactor"), rs.getString("nombre"), rs.getDate("fechanac"));

			listaActores.add(actor);
		}

		return listaActores;
	}

	@Override
	public void borrarActor(Actor actor) throws SQLException {

		if (actor != null) {
			String sqlDelete = "DELETE FROM actor WHERE nombre = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setString(1, actor.getNombre());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}

	}

	@Override
	public void modificarActor(Actor actor) throws SQLException {

		String sql = "UPDATE actor SET nombre=?, fechanac=? WHERE idactor =?";
		PreparedStatement preparedStatement = con.prepareStatement(sql);
		preparedStatement.setString(1, actor.getNombre());
		preparedStatement.setDate(2, (Date) actor.getFechanac());
		preparedStatement.setInt(3, actor.getIdactor());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	///////////////////////////////////////////////// CRUD PELICULAS
	///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	@Override
	public Pelicula buscarPelicula(String filtro) throws SQLException {

		Pelicula pelicula = null;

		String sql = "SELECT * FROM pelicula WHERE titulo= '" + filtro + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// Guardamos en un objeto de tipo genero todos los datos

		if (rs.next()) {

			pelicula = new Pelicula(rs.getInt("idpelicula"), rs.getString("titulo"), rs.getString("sinopsis"),
					rs.getInt("preciodia"));
		}

		return pelicula;
	}

	@Override
	public void guardarPelicula(Pelicula pelicula) throws SQLException {

		String sqlInsert = "INSERT INTO pelicula (titulo,sinopsis,preciodia) VALUES (?,?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setString(1, pelicula.getTitulo());
		preparedStatement.setString(2, pelicula.getSinopsis());
		preparedStatement.setInt(3, pelicula.getPreciodia());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public ArrayList<Pelicula> listadoPeliculas(String filtro) throws SQLException {

		Pelicula pelicula = new Pelicula();
		ArrayList<Pelicula> listaPeliculas = new ArrayList<>();
		String sql = "SELECT * FROM pelicula WHERE titulo like '" + "%" + filtro + "%" + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {

			pelicula = new Pelicula(rs.getInt("idpelicula"), rs.getString("titulo"), rs.getString("sinopsis"),
					rs.getInt("preciodia"));

			listaPeliculas.add(pelicula);
		}

		return listaPeliculas;
	}

	@Override
	public void borrarPelicula(Pelicula pelicula) throws SQLException {

		if (pelicula != null) {
			String sqlDelete = "DELETE FROM pelicula WHERE titulo = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setString(1, pelicula.getTitulo());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}

	}

	@Override
	public void modificarPelicula(Pelicula pelicula) throws SQLException {

		String sql = "UPDATE pelicula SET titulo=?, sinopsis=?, preciodia=? WHERE idpelicula =?";
		PreparedStatement preparedStatement = con.prepareStatement(sql);
		preparedStatement.setString(1, pelicula.getTitulo());
		preparedStatement.setString(2, pelicula.getSinopsis());
		preparedStatement.setInt(3, pelicula.getPreciodia());
		preparedStatement.setInt(4, pelicula.getIdpelicula());

		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public void perteneceAniadir(Pelicula pelicula, Genero genero) throws SQLException {

		String sqlInsert = "INSERT INTO pertenece (idgenero,idpelicula) VALUES (?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setInt(1, genero.getIdgenero());
		preparedStatement.setInt(2, pelicula.getIdpelicula());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public void perteneceQuitar(Genero genero, Pelicula pelicula) throws SQLException {

		String sqlInsert = "DELETE FROM pertenece WHERE idgenero = ? AND idpelicula = ?";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setInt(1, genero.getIdgenero());
		preparedStatement.setInt(2, pelicula.getIdpelicula());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public void actuaAniadir(Pelicula pelicula, Actor actor) throws SQLException {

		String sqlInsert = "INSERT INTO actua (idactor,idpelicula) VALUES (?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setInt(1, actor.getIdactor());
		preparedStatement.setInt(2, pelicula.getIdpelicula());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public void actuaQuitar(Actor actor, Pelicula pelicula) throws SQLException {

		String sqlInsert = "DELETE FROM actua WHERE idactor = ? AND idpelicula = ?";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setInt(1, actor.getIdactor());
		preparedStatement.setInt(2, pelicula.getIdpelicula());
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	///////////////////////////////////////////////// CRUD TARIFAS
	///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	@Override
	public Tarifa buscarTarifa(String filtro) throws SQLException {

		Tarifa tarifa = null;

		String sql = "SELECT * FROM tarifa WHERE descripcion= '" + filtro + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// Guardamos en un objeto de tipo genero todos los datos

		if (rs.next()) {

			tarifa = new Tarifa(rs.getInt("idtarifa"), rs.getString("descripcion"), rs.getInt("porcentajedto"), rs.getDate("fechadesde"), rs.getDate("fechahasta"));
		}

		return tarifa;
		
	}

	@Override
	public void guardarTarifa(Tarifa tarifa) throws SQLException {

		// Para pasar la fecha de String a Date
		java.sql.Date fechaDesde = new java.sql.Date(tarifa.getFechadesde().getTime());
		java.sql.Date fechaHasta = new java.sql.Date(tarifa.getFechahasta().getTime());

		String sqlInsert = "INSERT INTO tarifa (descripcion,porcentajedto,fechadesde,fechahasta) VALUES (?,?,?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setString(1, tarifa.getDescripcion());
		preparedStatement.setInt(2, tarifa.getPorcentajedto());
		preparedStatement.setDate(3, fechaDesde);
		preparedStatement.setDate(4, fechaHasta);
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}

	@Override
	public ArrayList<Tarifa> listadoTarifas(String filtro) throws SQLException {

		Tarifa tarifa = new Tarifa();
		ArrayList<Tarifa> listaTarifa = new ArrayList<>();
		String sql = "SELECT * FROM tarifa WHERE descripcion like '" + "%" + filtro + "%" + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {

			tarifa = new Tarifa(rs.getInt("idtarifa"), rs.getString("descripcion"), rs.getInt("porcentajedto"), rs.getDate("fechadesde"), rs.getDate("fechahasta"));

			listaTarifa.add(tarifa);
		}

		return listaTarifa;
	}

	@Override
	public void borrarTarifa(Tarifa tarifa) throws SQLException {

		if (tarifa != null) {
			String sqlDelete = "DELETE FROM tarifa WHERE descripcion = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setString(1, tarifa.getDescripcion());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}

	}

	@Override
	public void modificarTarifa(Tarifa tarifa) throws SQLException {
		
		// Para pasar la fecha de String a Date
				java.sql.Date fechaDesde = new java.sql.Date(tarifa.getFechadesde().getTime());
				java.sql.Date fechaHasta = new java.sql.Date(tarifa.getFechahasta().getTime());

		String sql = "UPDATE tarifa SET descripcion=?, porcentajedto=?, fechadesde=?, fechahasta=? WHERE idtarifa =?";
		PreparedStatement preparedStatement = con.prepareStatement(sql);
		preparedStatement.setString(1, tarifa.getDescripcion());
		preparedStatement.setInt(2, tarifa.getPorcentajedto());
		preparedStatement.setDate(3, fechaDesde);
		preparedStatement.setDate(4, fechaHasta);
		preparedStatement.setInt(5, tarifa.getIdtarifa());
		
		preparedStatement.executeUpdate();
		preparedStatement.close();

	}
	
	
///////////////////////////////////////////////// CRUD EJEMPLARES
///////////////////////////////////////////////// ////////////////////////////////////////////////////////////

	@Override
	public ArrayList<Ejemplar> listadoEjemplares(Pelicula pelicula) throws SQLException {
		
		Ejemplar ejemplar = new Ejemplar();
		
		ArrayList<Ejemplar> listaEjemplares = new ArrayList<>();
		String sql = "SELECT * FROM ejemplar WHERE idpelicula= '" + pelicula.getIdpelicula() + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {

			ejemplar = new Ejemplar(rs.getInt("idejemplar"),pelicula, rs.getDate("fechacompra"));
			

			listaEjemplares.add(ejemplar);
		}

		return listaEjemplares;
	}

	@Override
	public void borrarEjemplar(Ejemplar ejemplar) throws SQLException {

		
			String sqlDelete = "DELETE FROM ejemplar WHERE idejemplar = ?";
			PreparedStatement preparedStatement = con.prepareStatement(sqlDelete);
			preparedStatement.setInt(1, ejemplar.getIdejemplar());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		
		
	}

	@Override
	public void aniadirEjemplar(Ejemplar ejemplar, Pelicula pelicula) throws SQLException {

		java.sql.Date fechaCompra;
		
		if(ejemplar.getFechacompra()!=null) {
			fechaCompra = new java.sql.Date(ejemplar.getFechacompra().getTime());
		}else {
			fechaCompra = null;
		}
		
		String sqlInsert = "INSERT INTO ejemplar (fechacompra,idpelicula) VALUES (?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
		preparedStatement.setDate(1, fechaCompra);
		preparedStatement.setInt(2, pelicula.getIdpelicula());
		preparedStatement.executeUpdate();
		preparedStatement.close();
		
	}

}
