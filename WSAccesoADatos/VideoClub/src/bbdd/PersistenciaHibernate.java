package bbdd;

import java.sql.SQLException;
import java.util.ArrayList;

import videoClub.Actor;
import videoClub.Cliente;
import videoClub.Ejemplar;
import videoClub.Genero;
import videoClub.Pelicula;
import videoClub.Tarifa;

public class PersistenciaHibernate implements Persistencia {

	public PersistenciaHibernate(String property) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Genero buscarGenero(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarGenero(Genero genero) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Genero> listadoGeneros(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarGenero(Genero genero) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modificarGenero(Genero genero) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Cliente buscarCliente(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarCliente(Cliente cliente) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Cliente> listadoClientes(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarCliente(Cliente cliente) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modificarCliente(Cliente cliente) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Actor buscarActor(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarActor(Actor actor) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Actor> listadoActores(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarActor(Actor actor) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modificarActor(Actor actor) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Pelicula buscarPelicula(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarPelicula(Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Pelicula> listadoPeliculas(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarPelicula(Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modificarPelicula(Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void perteneceAniadir(Pelicula pelicula, Genero genero) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void actuaAniadir(Pelicula pelicula, Actor actor) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void perteneceQuitar(Genero genero, Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void actuaQuitar(Actor actor, Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public Tarifa buscarTarifa(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarTarifa(Tarifa tarifa) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Tarifa> listadoTarifas(String filtro) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarTarifa(Tarifa tarifa) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modificarTarifa(Tarifa tarifa) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Ejemplar> listadoEjemplares(Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void borrarEjemplar(Ejemplar ejemplar) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aniadirEjemplar(Ejemplar ejemplar, Pelicula pelicula) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
