package bbdd;

import java.sql.SQLException;
import java.util.ArrayList;

import videoClub.Actor;
import videoClub.Cliente;
import videoClub.Ejemplar;
import videoClub.Genero;
import videoClub.Pelicula;
import videoClub.Tarifa;

public interface Persistencia {
	
	/**
	 * Metodos para CRUD generos
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public Genero buscarGenero(String filtro) throws SQLException;
	public void guardarGenero(Genero genero) throws SQLException;
	public ArrayList<Genero> listadoGeneros(String filtro) throws SQLException;
	public void borrarGenero(Genero genero)throws SQLException;
	public void modificarGenero(Genero genero)throws SQLException;
	
	
	/**
	 * Metodos para CRUD clientes
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public Cliente buscarCliente(String filtro) throws SQLException;
	public void guardarCliente(Cliente cliente) throws SQLException;
	public ArrayList<Cliente> listadoClientes(String filtro) throws SQLException;
	public void borrarCliente(Cliente cliente)throws SQLException;
	public void modificarCliente(Cliente cliente)throws SQLException;
	
	
	/**
	 * Metodos para CRUD actores
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public Actor buscarActor(String filtro) throws SQLException;
	public void guardarActor(Actor actor) throws SQLException;
	public ArrayList<Actor> listadoActores(String filtro) throws SQLException;
	public void borrarActor(Actor actor)throws SQLException;
	public void modificarActor(Actor actor)throws SQLException;
	
	/**
	 * Metodos para CRUD peliculas
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public Pelicula buscarPelicula(String filtro) throws SQLException;
	public void guardarPelicula(Pelicula pelicula) throws SQLException;
	public ArrayList<Pelicula> listadoPeliculas(String filtro) throws SQLException;
	public void borrarPelicula(Pelicula pelicula)throws SQLException;
	public void modificarPelicula(Pelicula pelicula)throws SQLException;
	public void perteneceAniadir(Pelicula pelicula, Genero genero)throws SQLException;
	public void perteneceQuitar(Genero genero, Pelicula pelicula)throws SQLException;
	public void actuaAniadir(Pelicula pelicula, Actor actor)throws SQLException;
	public void actuaQuitar(Actor actor,Pelicula pelicula)throws SQLException;
	
	
	
	/**
	 * Metodos para CRUD tarifas
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public Tarifa buscarTarifa(String filtro) throws SQLException;
	public void guardarTarifa(Tarifa tarifa) throws SQLException;
	public ArrayList<Tarifa> listadoTarifas(String filtro) throws SQLException;
	public void borrarTarifa(Tarifa tarifa)throws SQLException;
	public void modificarTarifa(Tarifa tarifa)throws SQLException;
	
	/**
	 * Metodos para CRUD tarifas
	 * @param filtro
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Ejemplar> listadoEjemplares(Pelicula pelicula) throws SQLException; //Le pasamos el idpelicula
	public void borrarEjemplar(Ejemplar ejemplar)throws SQLException;
	public void aniadirEjemplar(Ejemplar ejemplar,Pelicula pelicula)throws SQLException;
	
	

}
