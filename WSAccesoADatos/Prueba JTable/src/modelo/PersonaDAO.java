package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;



public class PersonaDAO {
	
	Conexion conectar = new Conexion();
	Connection con;
	PreparedStatement ps;
	ResultSet rs;
	
	public List listar() {
        List<Genero> datos = new ArrayList<>();
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement("select * from genero");
            rs = ps.executeQuery();
            while (rs.next()) {
            	Genero g = new Genero();
                g.setIdgenero(rs.getInt(1));
                g.setDescripcion(rs.getString(2));
                datos.add(g);
            }
        } catch (Exception e) {
        }
        return datos;
    }
    public int agregar(Genero gen) {  
        int r=0;
        String sql="insert into genero(idgenero,descripcion(?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);            
            ps.setInt(1, gen.getIdgenero());
            ps.setString(2, gen.getDescripcion());
            r=ps.executeUpdate();    
            if(r==1){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception e) {
        }  
        return r;
    }
    public int Actualizar(Genero gen) {  
        int r=0;
        String sql="update genero set idgenero=?,descripcion=?";        
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);            
            ps.setInt(1,gen.getIdgenero());
            ps.setString(2,gen.getDescripcion());
            r=ps.executeUpdate();    
            if(r==1){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception e) {
        }  
        return r;
    }
    public int Delete(int id){
        int r=0;
        String sql="delete from persona where Id="+id;
        try {
            con=conectar.getConnection();
            ps=con.prepareStatement(sql);
            r= ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }
	

}
