package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import modelo.Genero;
import modelo.PersonaDAO;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JPanel panelDatos;
	private JTextField textFieldID;
	private JTextField textFieldDescripcion;
	private JButton btnDelete;
	public JButton btnEditar;
	private JButton btnOK;
	private JButton btnGuardar;
	public JButton btnListar;
	
	DefaultTableModel modelo = new DefaultTableModel();
	PersonaDAO dao = new PersonaDAO();
	Genero g = new Genero();
	private JTable tabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista frame = new Vista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 706, 594);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelDatos = new JPanel();
		panelDatos.setName("");
		panelDatos.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelDatos.setToolTipText("");
		panelDatos.setBounds(10, 11, 670, 184);
		contentPane.add(panelDatos);
		panelDatos.setLayout(null);
		
		JLabel lblNombre = new JLabel("ID");
		lblNombre.setBounds(10, 11, 37, 14);
		panelDatos.add(lblNombre);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(10, 44, 46, 14);
		panelDatos.add(lblNewLabel);
		
		textFieldID = new JTextField();
		textFieldID.setBounds(66, 8, 86, 20);
		panelDatos.add(textFieldID);
		textFieldID.setColumns(10);
		
		textFieldDescripcion = new JTextField();
		textFieldDescripcion.setBounds(66, 41, 86, 20);
		panelDatos.add(textFieldDescripcion);
		textFieldDescripcion.setColumns(10);
		
		//BOTON GUARDAR
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String descripcion = textFieldDescripcion.getText();
				g = new Genero(descripcion);
				dao.agregar(g);
				
			}

			
		});
		btnGuardar.setMnemonic('g');
		btnGuardar.setBounds(179, 7, 188, 23);
		panelDatos.add(btnGuardar);
		
		//BOTON LISTAR
		btnListar = new JButton("Listar");
		btnListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar(tabla);
			}
		});
		btnListar.setMnemonic('L');
		btnListar.setBounds(179, 44, 188, 23);
		panelDatos.add(btnListar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setMnemonic('e');
		btnEditar.setBounds(179, 80, 89, 23);
		panelDatos.add(btnEditar);
		
		btnOK = new JButton("OK");
		btnOK.setMnemonic('o');
		btnOK.setBounds(278, 80, 89, 23);
		panelDatos.add(btnOK);
		
		btnDelete = new JButton("Eliminar");
		btnDelete.setMnemonic('e');
		btnDelete.setBounds(179, 115, 188, 23);
		panelDatos.add(btnDelete);
		
		tabla = new JTable();
		tabla.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"IDGENERO", "DESCRIPCION"
			}
		));
		tabla.setBounds(10, 225, 670, 308);
		contentPane.add(tabla);
	}
	
	
	//METODOS
	
	
	
	private void listar(JTable tabla) {
		
		modelo = (DefaultTableModel) tabla.getModel();
		List<Genero>lista = dao.listar();
		Object[]object = new Object[2];
		for(int i = 0;i<lista.size();i++) {
			object[0]=lista.get(i).getIdgenero();
			object[1]=lista.get(i).getDescripcion();
			modelo.addRow(object);
			
		}
		
		
		tabla.setModel(modelo);
		
	}
	
	
	
	
	
	
}
