package filtroMayusMinus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Transformar {

	public static void main(String[] args) throws IOException {
		
		/**
		 * Metemos por parametro M para transformar a mayusculas o m a minusculas
		 * Hemos exportado el archivo a .jar para probarlo desde consola
		 * Se puede meter el argumento en run configurations
		 */
		
		//Si el argumento introducido es diferente a 1
		if (args.length!=1) {
			System.err.println("¡Falta el argumento! M ---> Mayúsculas m---> Minúsculas."); //Mensaje de error en rojo
			System.exit(-1); //Matamos el programa
		}
		
		//Si el argumento es diferente a M o m
		if (!args[0].matches("[Mm]")) {
			System.err.println("Introduce parametro: M ---> Mayúsculas m---> Minúsculas."); //Mensaje de error en rojo
			System.exit(-1); //Matamos el programa
		}
		
		boolean may = args[0].equals("M");
		
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in)); //Leemos
		String linea;
		
			while ((linea=bfr.readLine())!=null) {
				System.out.println(may?linea.toUpperCase():linea.toLowerCase()); //el ? hace que seleccione uno u otro dependiendo de may. Los separados por :
			}

			bfr.close();

	}

}
