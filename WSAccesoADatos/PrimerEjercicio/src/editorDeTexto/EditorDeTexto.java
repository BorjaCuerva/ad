package editorDeTexto;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class EditorDeTexto extends JFrame {
	
	
	/**
	 * @author Borja
	 * Interfaz grafica sencilla para emular un bloc de notas.
	 * Abre archivos de texto mostrando su contenido.
	 * Guarda el contenido escrito en un documento de texto
	 */

	private JPanel contentPane;
	private JButton btnAbrir;
	private JButton btnGuardar;
	private JTextArea textArea;
	private File ruta;
	private String linea;
	private String texto;
	private JScrollPane scrollPane;
	private JButton btnBorrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditorDeTexto frame = new EditorDeTexto();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditorDeTexto() {
		setTitle("Ruta del fichero");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 884, 616);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(48, 63, 775, 271);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		
		btnAbrir = new JButton("Abrir");
		btnAbrir.addActionListener(new ActionListener() {
			
			
			/**
			 * Evento boton abrir archivo
			 * Trabajar en los actionPerformed con metodos y los metodos
			 * tenerlos abajo en abierto
			 */
			
			public void actionPerformed(ActionEvent e) {
				
				abrir();	
				
			}

			
			
		});
		
		btnAbrir.setBounds(123, 439, 108, 23);
		contentPane.add(btnAbrir);
		
		btnGuardar = new JButton("Guardar");
		
		btnGuardar.addActionListener(new ActionListener() {
			
			
			/**
			 * Evento del boton guardar
			 */
			
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fc = new JFileChooser("/");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY); //Solo files
				fc.setDialogTitle("Seleccione donde quiere guardar el archivo");
				int opcion = fc.showOpenDialog(null);
				if (opcion == JFileChooser.APPROVE_OPTION) {
					ruta = fc.getSelectedFile(); //Guardamos ruta
					try {
						FileWriter fw = new FileWriter(ruta); //Objeto FileWriter para escribir
						fw.write(textArea.getText()); //Capturamos lo que hay en textArea. No guarda los saltos de linea
						
						JOptionPane.showMessageDialog(null, "Archivo guardado correctamente"); //Mensaje informativo
						fw.close(); //Cerramos el stream
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
		});
		btnGuardar.setBounds(654, 439, 108, 23);
		contentPane.add(btnGuardar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			
			/**
			 * Evento del boton borrar
			 * Reseteamos el contenido del textArea
			 */
			
			public void actionPerformed(ActionEvent arg0) {
				
				textArea.setText("");
				contentPane.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
				JOptionPane.showMessageDialog(null, "Contenido del bloc de notas borrado", "Borrar contenido", 1);
				
			}
		});
		btnBorrar.setBounds(392, 438, 117, 25);
		contentPane.add(btnBorrar);
	}
	
	
	public void abrir() {
		
		//Seleccionamos el archivo con JFileChooser			
		JFileChooser fc = new JFileChooser("/");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY); //Solo files
		fc.setDialogTitle("Seleccione archivo a abrir");
		int opcion = fc.showOpenDialog(null);
		if (opcion == JFileChooser.APPROVE_OPTION) {
			
			ruta = fc.getSelectedFile(); //Ruta del archivo
			this.setTitle(ruta.getAbsolutePath()); //Cambiamos titulo
			try {
				BufferedReader bfr = new BufferedReader(new FileReader(ruta)); //BufferedReader para leer el archivo
				
				while ((linea = bfr.readLine())!=null) { //Bucle para leer las lineas hasta que vea un null
					
					texto += linea+ "\n"; //A�adimos al texto lo que lee por cada vuelta y le a�adimos un \n para salto de linea entre lectura y lectura
					
				}
				
				bfr.close(); //Cerramos el Stream
				
				
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "¡No se encuentra el archivo!", "Error", 2);
				e1.printStackTrace();
			}

			textArea.setText(texto); //Imprimimos el contenido al jTextArea
			
		}
		
		
	}
		
	}
	
