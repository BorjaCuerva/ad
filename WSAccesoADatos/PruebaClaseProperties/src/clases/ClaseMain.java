package clases;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ClaseMain {

	public static void main(String[] args) {
		
		String tipoPersistencia;
		String servidor;
		String baseDatos;
		String usuario;
		String password;
		
		//Objeto Properties del que sacaremos toda la informacion de CFG.INI
		Properties prop = new Properties();
		try {
			prop.load(new FileReader("src/CFG.INI")); //Cargamos el archivo CFG.INI
			//Guardamos los datos en las variables
			tipoPersistencia = prop.getProperty("tipoPersistencia");
			servidor = prop.getProperty("mysqlJDBC.servidor"); //En el archivo se llama exactamente asi
			baseDatos = prop.getProperty("mysqlJDBC.baseDatos");
			usuario = prop.getProperty("mysqlJDBC.usuario");
			password = prop.getProperty("mysqlJDBC.password");
			
			/*
			 * Para sacar informacion de la persistencia hibernate
			servidor = prop.getProperty("hibernate.servidor"); //En el archivo se llama exactamente asi
			baseDatos = prop.getProperty("hibernate.baseDatos");
			usuario = prop.getProperty("hibernate.usuario");
			password = prop.getProperty("hibernate.password");
			*/
			
			//Sacamos por consola los datos
			System.out.println("Tipo persistencia ---> "+tipoPersistencia +"\n"
					+"servidor ---> "+servidor+"\n"
					+"base de datos ---> "+baseDatos+"\n"
					+"usuario ---> "+usuario+"\n"
					+"password ---> "+password+"\n"
					);
			
		} catch (FileNotFoundException e) {
			System.out.println("No se encuentra el archivo CFG.INI");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

	}

}
