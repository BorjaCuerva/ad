package ejercicio1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class BuscarPorTamanioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldTamanio;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextPane textPaneRuta;
	private JButton btnSeleccionarCarpeta;
	private JButton btnBuscar;
	private JCheckBox chckbxOcultos;
	private JCheckBox checkBoxSubcarpetas;
	private JRadioButton rdbtnMenores;
	private JRadioButton rdbtnMayores;
	private JTextArea textAreaResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarPorTamanioGUI frame = new BuscarPorTamanioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();					
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public BuscarPorTamanioGUI() {

		setResizable(false);
		setTitle("Buscar por tamaño");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 716, 447);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		/**
		 * Evento buscarCarpeta
		 */

		btnSeleccionarCarpeta = new JButton("Seleccionar Carpeta");
		btnSeleccionarCarpeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				buscarArchivo(); // Metodo para buscar el archivo

			}

		});
		btnSeleccionarCarpeta.setToolTipText("Abre el explorador de archivos");
		btnSeleccionarCarpeta.setBounds(24, 30, 190, 31);
		contentPane.add(btnSeleccionarCarpeta);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(273, 30, 361, 36);
		contentPane.add(scrollPane);

		textPaneRuta = new JTextPane();
		textPaneRuta.setToolTipText("Muestra la ruta del archivo seleccionado");
		textPaneRuta.setEditable(false);
		scrollPane.setViewportView(textPaneRuta);

		JLabel lblNewLabel = new JLabel("Tamanio en bytes:");
		lblNewLabel.setBounds(24, 112, 147, 15);
		contentPane.add(lblNewLabel);

		textFieldTamanio = new JTextField();
		textFieldTamanio.setText("0");
		textFieldTamanio.setToolTipText("Ejemplo 5000");
		textFieldTamanio.setBounds(165, 110, 114, 19);
		contentPane.add(textFieldTamanio);
		textFieldTamanio.setColumns(10);

		rdbtnMayores = new JRadioButton("Mayores");
		rdbtnMayores.setToolTipText("Elementos mayores del tamaño seleccionado en byts");
		buttonGroup.add(rdbtnMayores);
		rdbtnMayores.setSelected(true);
		rdbtnMayores.setBounds(304, 108, 86, 23);
		contentPane.add(rdbtnMayores);

		rdbtnMenores = new JRadioButton("Menores");
		rdbtnMenores.setToolTipText("Elementos menores del tamaño seleccionado en byts");
		buttonGroup.add(rdbtnMenores);
		rdbtnMenores.setBounds(304, 137, 99, 23);
		contentPane.add(rdbtnMenores);

		chckbxOcultos = new JCheckBox("Incluir Ocultos");
		chckbxOcultos.setToolTipText("Incluye archivos ocultos por el sistema");
		chckbxOcultos.setBounds(406, 108, 134, 23);
		contentPane.add(chckbxOcultos);

		checkBoxSubcarpetas = new JCheckBox("Incluir Subcarpetas");
		checkBoxSubcarpetas.setToolTipText("Incluye carpetas dentro de carpetas");
		checkBoxSubcarpetas.setBounds(407, 137, 162, 23);
		contentPane.add(checkBoxSubcarpetas);

		/**
		 * Evento del boton buscar
		 */

		btnBuscar = new JButton("BUSCAR");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					buscarArchivos();
				} catch (Exception e2) {
					Utilidades.notificarError(null, "¡Campo vacío erroneo!", e2, "El campo tamanio en bytes necesita un número."+"\n");
				}				
					
			}

		});
		btnBuscar.setEnabled(false);
		btnBuscar.setToolTipText("Ejecuta la búsqueda");
		btnBuscar.setBounds(565, 107, 117, 25);
		contentPane.add(btnBuscar);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 184, 658, 214);
		contentPane.add(scrollPane_1);

		textAreaResultado = new JTextArea();
		textAreaResultado.setToolTipText("Muestra el resultado de la búsqueda");
		textAreaResultado.setEditable(false);
		scrollPane_1.setViewportView(textAreaResultado);
	}

	/**
	 * Metodo que abre el JFileChooser para seleccionar un archivo Habilita el boton
	 * buscar despues de seleccionar un archivo
	 */

	private void buscarArchivo() {

		File ruta;
		JFileChooser fc = new JFileChooser(".");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); // Solo directorios
		fc.setDialogTitle("Seleccione carpeta donde quiere buscar.");
		int opcion = fc.showOpenDialog(null);
		// JFileChooser para seleccionar el archivo
		if (opcion == JFileChooser.FILES_ONLY) {
			ruta = fc.getSelectedFile();

			// Si la ruta seleccionada es un directorio mostramos la ruta y activamos el
			// boton buscar
			if (ruta.isDirectory()) {
				textPaneRuta.setText(ruta.getAbsolutePath()); // Mostramos la ruta del archivo
				btnBuscar.setEnabled(true); // Activamos el boton buscar

				// Si no es un directorio mostramos mensaje de error
			} else {

				Utilidades.notificarError(null, "�Error de carpeta!", null,
						ruta.getAbsolutePath() + " No es una carpeta valida!");
			}
		}
	}
	
	/**
	 * Metodo para buscar archivos de un tamanio especifico dentro de carpetas
	 */

	private void buscarArchivos() {
		textAreaResultado.setText(""); //Reseteamos por si hay una busqueda nueva
		String resultadoFinalString = "";
		
		
			ArrayList<File> resultadoFinal = Utilidades.buscarArchivosPorTamanio(new File(textPaneRuta.getText()), Long.valueOf(textFieldTamanio.getText()), rdbtnMayores.isSelected()?'+':'-', chckbxOcultos.isSelected(), checkBoxSubcarpetas.isSelected());
			if (resultadoFinal!=null) {
				for (File file : resultadoFinal) {
					resultadoFinalString+=file.getAbsolutePath() + " [ "+file.length()+ " bytes ]"+"\n";
					
				}
				textAreaResultado.setText(resultadoFinalString);
			}
		
		
		
		
	}

}
