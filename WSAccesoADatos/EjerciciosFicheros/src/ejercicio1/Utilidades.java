package ejercicio1;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utilidades {

	/**
	 * Metodo para mostrar al usuario un error de la aplicacion
	 * 
	 * @param padre
	 *            = Frame padre
	 * @param titulo
	 *            = Titulo de la ventana
	 * @param e
	 *            = Tipo de excepcion mostrada al usuario
	 * @param mensaje
	 *            = Mensaje mostrado al usuario
	 */

	public static void notificarError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (mensaje != null)
			contenido += mensaje;
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Tipo y mensaje de la excepci�n
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * 
	 * Metodo que recive parametros para buscar por tamanio Este metodo est� pensado
	 * para la parte de consola de comandos ya que en interfaz grafica se soluciona
	 * con el JFileChooser
	 * 
	 * @param carpetaDondeBuscar
	 *            = Ruta de la carpeta
	 * @param tamEnBytes
	 *            = Tamanio de los archivos a mostrar
	 * @param criterio
	 *            = Indica con + si son mayores al tamanio en bytes o - si son
	 *            menores
	 * @param incluirOcultos
	 *            = Si esta a true busca archivos ocultos
	 * @param incluirSubcarpetas
	 *            = Si esta a true hacemos recursividad
	 * @return listadoFinal = ArrayList con todos los archivos
	 */

	public static ArrayList<File> buscarArchivosPorTamanio(File carpetaDondeBuscar, long tamEnBytes, char criterio,
			boolean incluirOcultos, boolean incluirSubcarpetas) {

		boolean mayorTamanio = true; // Criterio para archivos mayores o menores
		switch (criterio) {
		case '+':
			mayorTamanio = true; // Si es + vamos a buscar archivos mayores
			break;
		case '-':
			mayorTamanio = false; // Si es - vamos a buscar archivos menores
			break;
		}
		
		
		/**
		 * HECHO CON FOR NORMAL POR QUE CON FOREACH NO ME FUNCIONA SUBCARPETAS
		 */
		
		ArrayList<File> listadoFinal = new ArrayList<File>();
		
		File[] listado = carpetaDondeBuscar.listFiles();
		if (listado == null) // Error al listar carpeta: falta de permisos, etc
			return null;

		for (int i = 0; i < listado.length; i++) {
			if (!listado[i].isHidden() || listado[i].isHidden() && incluirOcultos) {
				if (listado[i].isFile()) {
					if (mayorTamanio && listado[i].length() >= tamEnBytes
							|| !mayorTamanio && listado[i].length() <= tamEnBytes)
						listadoFinal.add(listado[i]);
				} else {
					if (incluirSubcarpetas) {

						ArrayList<File> al2 = buscarArchivosPorTamanio(listado[i], tamEnBytes, criterio, incluirOcultos,
								incluirSubcarpetas);
						if (al2 != null)
							listadoFinal.addAll(al2);
					}
				}
			}
		}
		return listadoFinal;

	}

}
