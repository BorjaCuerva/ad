package ejemploClase;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class ListadoDirectorios3 {

	/**
	 * 
	 * Version 3.0 en la que añadimos el metodo recursivo listarCarpeta
	 * 
	 * @param args
	 * @throws IOException
	 */

	public static void main(String[] args) throws IOException {

		/*
		 * Para leer por teclado ruta ---> ejemplo: /media/Datos/Prueba/
		 */

		/**
		 * Para leer por teclado InputStreamReader BufferedReader
		 */

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String ruta;

		System.out.println("Introduzca ruta del fichero.");
		ruta = br.readLine(); // Leemos por teclado la ruta
		File file = new File(ruta); // Creamos el File

		/**
		 * Metodo que lista carpetas y directorios con su nombre
		 */
		listarCarpeta(file);

	}
	
	/**
	 * @author Borja
	 * Metodo para listar carpetas y archivos con su nombre
	 * @param file
	 */
	
	private static void listarCarpeta(File file) {

		File[] lista = file.listFiles(); // listFiles para sacar todos los archivos y directorios a un array

		if (lista != null) { // Si el directorio o archivo no existe evitamos el null pointer exception

			for (File path : lista) {

				// Si es directorio
				if (path.isDirectory()) {
					System.out.println("Directorio " + " - " + path.getName());
					String rutaRecursiva = "";
					rutaRecursiva = path.getAbsolutePath(); //adquirimos la ruta de la carpeta
					File file2 = new File(rutaRecursiva); //metodo recursivo para que busque dentro de la carpeta
					listarCarpeta(file2); //metodo recursivo
				}

				// Si es fichero
				if (path.isFile()) {
					System.out.println("Fichero " + " - " + path.getName());
				}

			}

		} else {

			//JOptionPane.showMessageDialog(null, "¡La ruta no existe!", "Error de ruta", 2);
		}

	}

}
