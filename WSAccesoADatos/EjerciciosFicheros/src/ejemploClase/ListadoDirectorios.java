package ejemploClase;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;

public class ListadoDirectorios {

	public static void main(String[] args) throws IOException {

		/*
		 * Para leer por teclado ruta ---> ejemplo: /tmp
		 */
		
		/**
		 * Para leer por teclado
		 * InputStreamReader
		 * BufferedReader
		 */
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String ruta;

		System.out.println("Introduzca ruta del fichero.");
		ruta = br.readLine(); //Leemos por teclado la ruta
		File file = new File(ruta); // Creamos el File
		
		File [] lista = file.listFiles(); //listFiles para sacar todos los archivos y directorios a un array
		
		if (lista != null) { //Si el directorio o archivo no existe evitamos el null pointer exception		

			for (File path : lista) {
				
				//Si es directorio
				if (path.isDirectory()) {
					System.out.println("Directorio " + " - " + path.getName());
				}
				
				//Si es fichero
				if (path.isFile()) {
					System.out.println("Fichero " + " - " + path.getName());
				}

			}

		}else {
			
			JOptionPane.showMessageDialog(null, "¡La ruta no existe!", "Error de ruta", 2);
		}
	}

}
