package ejemplo01PrimeraPrueba;

import java.util.Scanner;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class MainProfesor {

	public static void main(String[] args) {
		
		/**
		 * Estas tres lineas, hacen que no salgan los mensajes en rojo de hibernate
		 */
		LogManager.getLogManager().reset();
		Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
		globalLogger.setLevel(java.util.logging.Level.OFF);
		
		
		int opcion;
		Profesor profesor = new Profesor(1203, "Borja2", "Cuerva", "Barbero"); // Creamos el objeto

		do {
			System.out.println("Introduce opcion:");
			System.out.println("1-Alta");
			System.out.println("2-Baja");
			System.out.println("3-Modificacion");
			System.out.println("0-Salir");

			Scanner sn = new Scanner(System.in);
			opcion = sn.nextInt();

			if (opcion == 1) {
				altaProfesor(profesor);
			}

			if (opcion == 2) {
				bajaProfesor();
			}

			if (opcion == 3) {
				Profesor profesorMod = new Profesor(1203, "BorjaMod", "Cuerva", "Barbero"); // Creamos el objeto
				modificarProfesor(profesorMod);
			}

		} while (opcion != 0);
		System.out.println("Adios");

	}

	/**
	 * Modificamos el profesor que ya existe
	 * 
	 * @param profesorMod
	 */
	private static void modificarProfesor(Profesor profesorMod) {

		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		//Abrimos sesion para comprobar si existe en la bbdd el profesor
		Session sessionMod = sessionFactory.openSession();
		Profesor profesorCom = (Profesor) sessionMod.get(Profesor.class, 1203); // el 1203 es el codigo de profesor
		/**
		 * Cerramos la sesion. Si no cerramos la sesion, al intentar hacer el sessionMod.update
		 * nos dice que ya hay una sesion iniciada (la de la linea 61)
		 */
		sessionMod.close();
		
		if (profesorCom != null) {
			//Abrimos otra vez la sesion para modificar
			sessionMod = sessionFactory.openSession();
			sessionMod.beginTransaction();
			sessionMod.update(profesorMod);
			sessionMod.getTransaction().commit();
		} else {
			
			System.out.println("El profesor no existe");
		}
		
		
	}

	private static void bajaProfesor() {

		SessionFactory sessionFactory;

		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		Session session = sessionFactory.openSession();

		// Leemos en la bbdd el profesor, comprueba el codigo
		Profesor profesor2 = (Profesor) session.get(Profesor.class, 1203); // el 1203 es el codigo de profesor

		// Si el profesor2 no existe, lo da de alta
		if (profesor2 != null) {

			session.beginTransaction();
			session.delete(profesor2);
			session.getTransaction().commit();
		} else {
			System.out.println("El profesor no existe");
		}
		
		session.close();

	}
	
	/**
	 * Alta de profesor, comprueba si el profesor existe.
	 * @param profesor
	 */
	private static void altaProfesor(Profesor profesor) {

		SessionFactory sessionFactory;

		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		//Abrimos sesion
		Session sessionAlta = sessionFactory.openSession();

		// Leemos en la bbdd el profesor, comprueba el codigo
		Profesor profesor2 = (Profesor) sessionAlta.get(Profesor.class, 1203); // el 1203 es el codigo de profesor

		// Si el profesor2 no existe, lo da de alta
		if (profesor2 == null) {
			sessionAlta.beginTransaction();
			sessionAlta.save(profesor); // <|--- Aqui guardamos el objeto en la base de datos.
			sessionAlta.getTransaction().commit();
		} else {
			System.out.println("El profesor ya existe");
		}
		//cerramos sesion
		sessionAlta.close();

	}

}
