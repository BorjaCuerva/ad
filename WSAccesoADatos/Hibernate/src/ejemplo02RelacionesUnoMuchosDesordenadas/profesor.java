package ejemplo02RelacionesUnoMuchosDesordenadas;

import java.io.Serializable;
import java.util.Set;

public class profesor implements Serializable {
	private int id;
	private String nombre;
	private String ape1;
	private String ape2;
	private Set<correoelectronico> correosElectronicos;

	public profesor() {
	}

	public profesor(int id, String nombre, String ape1, String ape2) {
		this.id = id;
		this.nombre = nombre;
		this.ape1 = ape1;
		this.ape2 = ape2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApe1() {
		return ape1;
	}

	public void setApe1(String ape1) {
		this.ape1 = ape1;
	}

	public String getApe2() {
		return ape2;
	}

	public void setApe2(String ape2) {
		this.ape2 = ape2;
	}

	public Set<correoelectronico> getCorreosElectronicos() {
		return correosElectronicos;
	}

	public void setCorreosElectronicos(Set<correoelectronico> correosElectronicos) {
		this.correosElectronicos = correosElectronicos;
	}


	
	
	
	
	

}