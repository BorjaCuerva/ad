package ejemplo02RelacionesUnoMuchosDesordenadas;

import java.io.Serializable;

public class correoelectronico implements Serializable {
	private int idCorreo;
	private String direccioncorreo;
	public String getDireccioncorreo() {
		return direccioncorreo;
	}

	private profesor profesor;

	public correoelectronico() {

	}

	public correoelectronico(int idCorreo, String direccionCorreo, profesor profesor) {
		this.idCorreo = idCorreo;
		this.direccioncorreo = direccionCorreo;
		this.profesor = profesor;
	}

	public int getIdCorreo() {
		return idCorreo;
	}

	public void setIdCorreo(int idCorreo) {
		this.idCorreo = idCorreo;
	}

	public profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(profesor profesor) {
		this.profesor = profesor;
	}

	public void setDireccioncorreo(String direccioncorreo) {
		this.direccioncorreo = direccioncorreo;
	}

	
	
	
}