package ejemplo02RelacionesUnoMuchosDesordenadas;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class ProfesorMain {

	public static void main(String[] args) {

		/**
		 * Estas tres lineas, hacen que no salgan los mensajes en rojo de hibernate
		 */
		LogManager.getLogManager().reset();
		Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
		globalLogger.setLevel(java.util.logging.Level.OFF);

		/**
		 * Cargamos la configuracion e iniciamos sesion
		 */
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		Session session = sessionFactory.openSession();

		/**
		 * Creamos el nuevo profesor, aniadiendo los correos electronicos
		 * profesor tiene la relacion 1
		 * correoelectronico tiene la relacion n
		 * es una relacion 1..n
		 */
		profesor profesor = new profesor(8, "Borja2", "Cuerva", "Barbero");
		Set<correoelectronico> correosElectronicos=new HashSet<>();
		correosElectronicos.add(new correoelectronico(3, "borja@yahoo.com", profesor));
		correosElectronicos.add(new correoelectronico(2, "borja@hotmail.com", profesor));
		correosElectronicos.add(new correoelectronico(1, "borja@gmail.com", profesor));
		correosElectronicos.add(new correoelectronico(4, "borja@hola.com", profesor));
		profesor.setCorreosElectronicos(correosElectronicos);

		/**
		 * Guardamos en la BBDD al profesor con sus correos electronicos
		 */
		session.beginTransaction();
		session.save(profesor);
		session.getTransaction().commit();
		session.close();

	}

}
