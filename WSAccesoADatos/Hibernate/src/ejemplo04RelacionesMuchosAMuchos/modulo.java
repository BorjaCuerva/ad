package ejemplo04RelacionesMuchosAMuchos;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class modulo implements Serializable {
	private int idModulo;
	private String nombre;
	private Set<profesor> profesores = new HashSet();

	public modulo() {

	}

	public modulo(int idModulo, String nombre) {
		this.idModulo = idModulo;
		this.nombre = nombre;

	}

	public int getIdModulo() {
		return idModulo;
	}

	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<profesor> getProfesores() {
		return profesores;
	}

	public void setProfesores(Set<profesor> profesores) {
		this.profesores = profesores;
	}
	
	
	
	
}