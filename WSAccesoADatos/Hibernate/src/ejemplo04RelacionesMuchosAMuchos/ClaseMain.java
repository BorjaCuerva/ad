package ejemplo04RelacionesMuchosAMuchos;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class ClaseMain {

	public static void main(String[] args) {

		/**
		 * Estas tres lineas, hacen que no salgan los mensajes en rojo de hibernate
		 */
		LogManager.getLogManager().reset();
		Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
		globalLogger.setLevel(java.util.logging.Level.OFF);

		// Id del profesor , nombre, apellido1, apellido2
		profesor profesor1 = new profesor(13, "Borja", "Cuerva", "Barbero");
		profesor profesor2 = new profesor(14, "Borja2", "Cuerva2", "Barbero2");

		// Id modulo y nombre del modulo
		modulo modulo1 = new modulo(1, "DI");
		modulo modulo2 = new modulo(2, "AD");
		modulo modulo3 = new modulo(3, "SGE");

		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		Session session = sessionFactory.openSession();

		// Primero hace un getModulos para aniadir los modulos que hay en el Set
		// Luego aniade el nuevo modulo
		profesor1.getModulos().add(modulo1);
		profesor1.getModulos().add(modulo2);
		profesor2.getModulos().add(modulo3);

		// Primero hace un getProfesores para aniadir los profesores que hay en el Set
		// Luego aniade el nuevo profesor
		// El profesor1 va a impartir el modulo1 y el modulo2
		// El profesor2 va a impartir el modulo3
		modulo1.getProfesores().add(profesor1);
		modulo2.getProfesores().add(profesor1);
		modulo3.getProfesores().add(profesor2);

		session.beginTransaction();

		session.save(profesor1);
		session.save(profesor2);

		session.getTransaction().commit();
		session.close();

	}

}
