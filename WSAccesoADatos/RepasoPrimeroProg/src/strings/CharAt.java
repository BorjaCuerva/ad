package strings;

public class CharAt {

	public static void main(String[] args) {

		System.out.println(
				"Introduce cadena y te dir� cuantas vocales y cuantas frases tiene. Las frases separadas por .");
		String cadena = utilidades.Entrada.cadena();

		System.out.println("Contar vocales: ");
		contarVocales(cadena);

		System.out.println("Contar frases separadas por .");
		contarFrases(cadena);

		System.out.println("Longitud de la cadena");
		longitudCadena(cadena);

		System.out.println("Cuarta y quinta letra");
		cuartaYQuintaLetra(cadena);

		System.out.println("Separar palabras");
		separarPalabras(cadena);

		System.out.println("Remplazar las a por e");
		remplazarAPorE(cadena);

		System.out.println("Posicion de las a");
		posicionA(cadena);

		System.out.println("Camello");
		camello(cadena);

	}

	private static void camello(String cadena) {

		for (int i = 0; i < cadena.length(); i++) {
			
			if (i % 2 == 0) {
				System.out.print((cadena.charAt(i) + "").toUpperCase());
			} else {
				System.out.print((cadena.charAt(i) + "").toLowerCase());
			}
		}
	}

	private static void posicionA(String cadena) {

		int pos = cadena.indexOf('a');
		System.out.println("Hay una a en la posicion " + pos);
		pos = cadena.indexOf('a', pos + 1);
		System.out.println("Hay una a en la posicion " + pos);
	}

	private static void remplazarAPorE(String cadena) {

		String frase = cadena.replace('a', 'e');
		System.out.println(frase);

	}

	private static void separarPalabras(String cadena) {

		String palabras[] = cadena.split(" ");
		for (int i = 0; i < palabras.length; i++) {
			System.out.println(palabras[i]);
		}

	}

	private static void cuartaYQuintaLetra(String cadena) {

		String cuarta;
		String quinta;

		if (cadena.length() < 4) {
			System.out.println("La cadena tiene menos de 4 letras");
		} else {
			cuarta = cadena.substring(3, 4);
			System.out.println("Cuarta letra= " + cuarta);
		}

		if (cadena.length() < 5) {
			System.out.println("La cadena tiene menos de 5 letras");
		} else {
			quinta = cadena.substring(4, 5);
			System.out.println("Quinta letra= " + quinta);
		}

	}

	private static void longitudCadena(String cadena) {

		int cnt = 0;
		int espacios = 0;
		for (int i = 0; i < cadena.length(); i++) {

			if (!(cadena.charAt(i) == ' ')) {
				cnt++;
			} else {
				espacios++;
			}

		}
		System.out.println("La cadena tiene: " + cnt + " letras. Y " + espacios + " espacios en blanco");
	}

	private static void contarFrases(String cadena) {

		int cnt = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == '.') {
				cnt++;
			}
		}
		System.out.println("Hay " + cnt + " frases.");
	}

	private static void contarVocales(String cadena) {
		int cnt = 0;
		for (int i = 0; i < cadena.length(); i++) {

			if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'o'
					|| cadena.charAt(i) == 'u') {
				cnt++;
			}

		}
		System.out.println("La frase tiene: " + cnt + " vocales.");

	}

}
