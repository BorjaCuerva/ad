package hashMap;

import java.util.HashMap;
import java.util.Map;

public class EjercicioHashMap {

	public static void main(String[] args) {

		boolean salir = false;

		String opcion; // opcion seleccionada del usuario

		String nombre;
		int telefono;

		HashMap<String, Integer> agenda = new HashMap<>();

		while (!salir) {
			System.out.println("Selecciona opci�n, pulsa 6 para salir: ");
			System.out.println("1. A�adir contacto");
			System.out.println("2. Listar contactos");
			System.out.println("3. Buscar contactos");
			System.out.println("4. Existe contacto");
			System.out.println("5. Eliminar contacto");
			System.out.println("6. Salir");

			opcion = utilidades.Entrada.cadena();
			
			switch (opcion) {
			case "1":
				System.out.println("Introduce nombre del contacto: ");
				nombre = utilidades.Entrada.cadena();
				System.out.println("Introduce el n�mero de tel�fono: ");
				telefono = utilidades.Entrada.entero();
				crearContacto(nombre, telefono, agenda);
				break;
			case "2":

				if (agenda.isEmpty()) {
					System.out.println("No hay contactos.");
				} else {
					listarContactos(agenda);

				}

				break;
			case "3":
				
				System.out.println("Introduce nombre de contacto.");
				nombre = utilidades.Entrada.cadena();
				
				buscarContacto(nombre,agenda);
				
				break;
			case "4":
				System.out.println("Introduce nombre de contacto.");
				nombre = utilidades.Entrada.cadena();
				
				existeContacto(nombre,agenda);
				
				break;
			case "5":
				System.out.println("Introduce nombre de contacto.");
				nombre = utilidades.Entrada.cadena();
				
				borrarContacto(nombre,agenda);
				
				break;
			case "6":
				salir = true;
				System.out.println("Fin del programa.");
				break;
			default:
				System.out.println("N�mero erroneo. Introduce n�mero del 1 al 6\n");
			}

		}

	}

	private static void borrarContacto(String nombre, HashMap<String, Integer> agenda) {

		if (agenda.containsKey(nombre)) {
			agenda.remove(nombre);
			System.out.println("Contacto "+nombre+" borrado.");
		}else {
			System.out.println("El contacto con nombre "+nombre+" no existe.");
		}
		
	}

	private static void existeContacto(String nombre, HashMap<String, Integer> agenda) {

		if(agenda.containsKey(nombre)) {
			System.out.println("El contacto con nombre "+nombre+" existe");
		}else {
			System.out.println("El contacto con nombre "+nombre+" no existe");
		}
		
	}

	private static void buscarContacto(String nombre, HashMap<String, Integer> agenda) {

		if (agenda.containsKey(nombre)) {
			System.out.println("n�mero de tel�fono de "+nombre+" : "+agenda.get(nombre));
		}else {
			System.out.println("El tel�fono no existe.");
		}
		
	}


	private static void listarContactos(HashMap<String, Integer> agenda) {
		for (Map.Entry<String, Integer> entry : agenda.entrySet()) { // Map.Entry es una interfaz
			System.out.println("Nombre del contacto: " + entry.getKey() + " // n�mero de tel�fono: " + entry.getValue());
		}

	}

	private static void crearContacto(String nombre, int telefono, HashMap<String, Integer> agenda) {

		if (agenda.containsKey(nombre)) {
			System.out.println("El contacto con el nombre " + nombre + " ya existe.");
		} else {
			agenda.put(nombre, telefono);
			System.out.println("Contacto a�adido.");
		}

	}

}
