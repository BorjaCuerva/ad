package tartaglia;

public class Tartaglia {

	public static void main(String[] args) {

		int nfilas = 10;
        int[] triangulo = new int[1];
        
        for (int i = 1; i <= nfilas; i++) {
        	
            int[] x = new int[i];
            
            for (int j = 0; j < i; j++) {
            	
                if (j == 0 || j == (i - 1)) {
                    x[j] = 1;
                } else {
                	
                    x[j] = triangulo[j] + triangulo[j - 1];
                }
                System.out.print(x[j] + " ");
            }
            triangulo = x;
            System.out.println();
        }
    }

	}


