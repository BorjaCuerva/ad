package matrices;

import java.util.Random;

public class SumaMatriz {

	public static void main(String[] args) {
		
		
		System.out.println("Introduce numero de filas: ");
		int filas = utilidades.Entrada.entero();
		
		System.out.println("Introduce numero de columnas");
		int columnas = utilidades.Entrada.entero();
		
		int matriz1[][] = new int [filas][columnas];
		int matriz2[][] = new int [filas][columnas];
		int matrizResultado[][] = new int [filas][columnas];
		
		
		rellenarMatriz(matriz1);
		rellenarMatriz(matriz2);
		
		System.out.println("Matriz 1:");
		pintarMatriz(matriz1);
		
		System.out.println("");
		System.out.println("Matriz 2:");
		pintarMatriz(matriz2);
		
		System.out.println("");
		System.out.println("Resultado de la suma de las dos matrices: ");
		pintarMatrizResultado(matriz1,matriz2,matrizResultado);
		
		System.out.println("");
		System.out.println("Matriz ordenada");
		pintarMatrizOrdenada(matriz1);
	}
	
	/**
	 * MEtodo para sacar por pantalla el resultado de la suma de las dos matrices
	 * @param matriz1
	 * @param matriz2
	 * @param matrizResultado
	 */
	private static void pintarMatrizResultado(int[][] matriz1, int[][] matriz2, int[][] matrizResultado) {
		
		//Calculamos el valor
		for (int i = 0; i < matriz2.length; i++) {
			for (int j = 0; j < matriz2[0].length; j++) {
				matrizResultado[i][j] = matriz1[i][j]+matriz2[i][j];
			}
		}
		
		//Lo pintamos
		System.out.println("Matriz resultado");
		pintarMatriz(matrizResultado);
		
	}
	
	private static void pintarMatrizOrdenada(int[][] matriz1) {

		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1.length; j++) {
				for (int x = 0; x < matriz1.length; x++) {
					for (int y = 0; y < matriz1.length; y++) {
						if (matriz1[i][j]<matriz1[x][y]) {
							int aux = matriz1[i][j];
							matriz1[i][j] = matriz1[x][y];
							matriz1[x][y] = aux;
						}
						
						
					}
				}
			}
		}
		pintarMatriz(matriz1);
	}

	/**
	 * Metodo para sacar por pantalla la matriz rellena con numeros aleatorios
	 * @param matriz1
	 */
	private static void pintarMatriz(int[][] matriz1) {

		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1[0].length; j++) {
				System.out.print(matriz1[i][j]+" ");
			}
			System.out.println("");
		}
		
	}
	
	/**
	 * Metodo para rellenar una matriz con numeros aleatorios
	 * @param matriz1
	 */
	private static void rellenarMatriz(int[][] matriz1) {

		Random r = new Random();
		int valorDado; 
		
		
		for (int i = 0; i < matriz1.length; i++) {
			for (int j = 0; j < matriz1[0].length; j++) {
				valorDado = r.nextInt(9)+1;
				matriz1[i][j] = valorDado;
			}
		}
		
	}

}
