package matrices;

public class Ejercicio1 {

	public static void main(String[] args) {

		int matriz [][] = new int[3][3]; //Primer numero filas y segundo columnas
		
		//En cada vuelta, i toma el valor de 0,1,2
		for (int i = 0; i < matriz.length; i++) {
			//En cada vuelta, j toma el valor de 0,1,2
			for (int j = 0; j < matriz[0].length; j++) { //Se pone matriz[0].length por si las columnas son diferentes a las filas
				matriz[i][j]=(i*matriz[0].length)+(j+1); //i * 3 + j+1 //matriz[0].length es el total de longitud = 3
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println("");
		}

	}

}
