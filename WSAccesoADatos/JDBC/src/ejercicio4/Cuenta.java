package ejercicio4;

public class Cuenta {
	
	int id; //Clave primaria de la BBDD
	int saldo;
	
	
	
	public Cuenta(int saldo) {
		super();
		this.saldo = saldo;
	}

	//CONSTRUCTOR
	public Cuenta(int id, int saldo) {
		super();
		this.id = id;
		this.saldo = saldo;
	}
	
	//GETTERS AND SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	

}
