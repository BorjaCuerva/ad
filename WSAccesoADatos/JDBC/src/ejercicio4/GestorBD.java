package ejercicio4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ejercicio1.Persona;

public class GestorBD {

	Connection con; // Objeto conexion

	/**
	 * Constructor que conecta con la BBDD
	 * @throws SQLException
	 */
	public GestorBD() throws SQLException {

		try {
			// Comprobamos el driver
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "no se encuentra el driver.\n");
		}
		// Conectamos a la BBDD mysql
		con = DriverManager.getConnection("jdbc:mysql://localhost/minibanco", "root", "manager");
		//poner autocommit a false
		con.setAutoCommit(false);
	}

	/**
	 * Metodo para consultar cuenta.
	 * 
	 * @param id
	 *            --> Clave primaria de la tabla clientes
	 * @return --> Retorna un cliente con todos los datos
	 * @throws Exception
	 */
	public Cuenta consultarCuenta(int id) throws Exception {

		int saldo = 0;
		Cuenta c1 = null;

		String sqlBusqueda = "SELECT * FROM cuenta WHERE id=" + "'" + id + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			
			id = rs.getInt("id");
			saldo = rs.getInt("saldo");
			c1 = new Cuenta(id,saldo);

		}

		return c1;
	}
	
	
	public ArrayList<Movimiento> consultarExtracto(String id, String fechaDesde, String fechaHasta) throws Exception {
		
		String sqlBusqueda = "SELECT * FROM movimiento WHERE id_cuenta=" + "'" + id + "' and f_h between '"+fechaDesde+" 00:00:00' and '"+fechaHasta+" 23:59:59'";               
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		//select * from movimiento where id_cuenta=1 and f_h between '2019-12-13' and '2019-12-17';
		ArrayList<Movimiento> al = new ArrayList<Movimiento>();
		
		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			
			al.add(new Movimiento(rs.getInt(1),rs.getString(2),rs.getInt(3), rs.getInt(4)));

		}
		rs.close();
		st.close();
		return al;
	}
	
	
	/**
	 * Metodo para crear las cuentas.
	 * El id entra como null, se rellena automaticamente en la BBDD al tener auto_increment
	 * @param c --> Objeto de tipo Cuenta
	 * @throws Exception
	 */
	public void crearCuenta(Cuenta c) throws Exception {

		Cuenta cuenta = consultarCuenta(c.getId());
		
		//Si la cuenta no existe, se crea
		if (cuenta == null) {
			String sqlInsert = "INSERT INTO cuenta VALUES (?,?)";
			PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
			preparedStatement.setInt(1, c.getId()); //
			preparedStatement.setInt(2, c.getSaldo());
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
		}

	}
	
	/**
	 * Metodo para ingresar en la cuenta.
	 * Actualiza el saldo total de la tabla cuenta.
	 * Actualiza la tabla movimiento. Con un id de movimiento, fecha y hora, importe y id_cuenta
	 * @param idCuenta -->Id de la cuenta del usuario
	 * @param ingreso --> Cantidad de dinero a ingresar
	 * @throws Exception
	 */
	
	public void ingreso(int idCuenta,int ingreso) throws Exception {
		
		//Actualizamos la tabla cuenta
		
		//Consultamos por el id si la cuenta existe
		Cuenta cuenta = consultarCuenta(idCuenta);
		int saldoAnterior = cuenta.getSaldo();
		int saldoFinal = ingreso+saldoAnterior;
		
		try {
			
			String sqlCuenta = "UPDATE cuenta SET saldo=? WHERE id="+idCuenta;
			PreparedStatement ps = con.prepareStatement(sqlCuenta);
			ps.setInt(1, saldoFinal);
			ps.executeUpdate();
			ps.close();
			
			//Actualizamos la tabla ingresos	
			//Orden de insert: id,f_h,importe,id_cuenta
			String sqlMovimientos = "INSERT INTO movimiento (f_h,importe,id_cuenta) VALUES (?,?,?)";
			
			PreparedStatement psMovimientos = con.prepareStatement(sqlMovimientos);
	        java.util.Date utilDate = new java.util.Date();
	        java.sql.Timestamp sqlTS = new java.sql.Timestamp(utilDate.getTime());

			psMovimientos.setTimestamp(1, sqlTS);
			//psMovimientos.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now())); //la hora la pone a 00:00;00
			psMovimientos.setInt(2, ingreso); 
			psMovimientos.setInt(3, idCuenta);
			psMovimientos.executeUpdate();
			psMovimientos.close();
			con.commit();
			
		}catch(Exception e) {
			con.rollback();
		}
			
	}
	
	/**
	 * Metodo para quitar dinero en la cuenta.
	 * Actualiza el saldo total de la tabla cuenta.
	 * Actualiza la tabla movimiento. Con un id de movimiento, fecha y hora, importe(en negativo) y id_cuenta
	 * @param idCuenta -->Id de la cuenta del usuario
	 * @param ingreso --> Cantidad de dinero a ingresar
	 * @throws Exception
	 */
	
	public void reintegro(int idCuenta,int ingreso) throws Exception {
	//Actualizamos la tabla cuenta
		
		//Consultamos por el id si la cuenta existe
		Cuenta cuenta = consultarCuenta(idCuenta);
		int saldoAnterior = cuenta.getSaldo();
		int saldoFinal = saldoAnterior-ingreso;
		
		try {
			
			String sqlCuenta = "UPDATE cuenta SET saldo=? WHERE id="+idCuenta;
			PreparedStatement ps = con.prepareStatement(sqlCuenta);
			ps.setInt(1, saldoFinal);
			ps.executeUpdate();
			ps.close();
			
			//Actualizamos la tabla ingresos		
			//Orden de insert: id,f_h,importe,id_cuenta
			String sqlMovimientos = "INSERT INTO movimiento (f_h,importe,id_cuenta) VALUES (?,?,?)";
			
			PreparedStatement psMovimientos = con.prepareStatement(sqlMovimientos);
			
	        java.util.Date utilDate = new java.util.Date(); //Objeto java Date
	        java.sql.Timestamp sqlTS = new java.sql.Timestamp(utilDate.getTime()); //objeto sql Timestamp
	        
			psMovimientos.setTimestamp(1, sqlTS);
			psMovimientos.setInt(2, -ingreso); 
			psMovimientos.setInt(3, idCuenta);
			psMovimientos.executeUpdate();
			psMovimientos.close();
			con.commit();
			
		}catch(Exception e) {
			con.rollback();
		}
		
		
	}
	
	
	/**
	 * Consulta existencia de una cuenta
	 * @param id de la cuenta
	 * @return boolean //true si existe, false no existe
	 * @throws Exception
	 */
	public boolean consultarExistencia(int id) throws Exception {
		
		boolean ex = false;

		String sqlBusqueda = "SELECT * FROM cuenta";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			if (rs.getString(1).equals(String.valueOf(id))) {
				ex = true;
			}
		}
		rs.close();
		st.close();

		return ex;
	}
	
	/**
	 * Consulta el saldo de una cuenta.
	 * @param id de la cuenta a consultar
	 * @return el saldo de la cuenta
	 * @throws Exception
	 */
	public int consultarSaldo(int id) throws Exception {
		
		int saldo = 0;

		String sqlBusqueda = "SELECT * FROM cuenta";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			if (rs.getString(1).equals(String.valueOf(id))) {
				saldo = Integer.valueOf(rs.getString(2));
			}
		}
		rs.close();
		st.close();

		return saldo;
	}
	
	
}
