package ejercicio2;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class PersistenciaDB {
	
	Connection con;
	DatabaseMetaData metadatos;
	ArrayList<String> listado = new ArrayList<>();
	
	/**
	 * Conectar a la BBDD mysql
	 * @param IP -->Ip del servidor
	 * @param usu --> Usuario
	 * @param pass --> Contrasenia
	 */
	public void conectar(String IP, String usu, String pass){
		// Comprobamos el driver de mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://"+IP+"/", usu, pass);
		} catch (ClassNotFoundException ex) {
			utilidades.Utilidades.notificarError(null, "BBDD", ex, "Error al conectar a la BBDD");
		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(null, "SQL", e, "Error con el driver");
			e.printStackTrace();
		}
	}
	
	/**
	 * Listamos las BBDD que hay en el sistema mysql
	 * @return --> un ArrayList con el nombre de las BBDD
	 * @throws SQLException
	 */
	public ArrayList<String> listarBases() throws SQLException {
		//Limpiamos el ArrayList de otras consultas
		listado.clear();
		//Objeto tipo metadatos
		metadatos = con.getMetaData();
		//Hacemos el resultSet
		ResultSet rs = metadatos.getCatalogs();
		
		//Guardamos en el ArrayList todas las BBDD
		while(rs.next()) {
			listado.add(rs.getString("TABLE_CAT")); //TABLE_CAT --> las BBDD
		}
		
		//return el ArrayList con todas las BBDD
		return listado;
		
	}
	
	public ArrayList<String> listarTablas(String base) throws SQLException {
		
		listado.clear();
		
		metadatos = con.getMetaData();
		ResultSet rs = metadatos.getTables(base, null, "%", null); //el % indica que muestre todo, es como el *
		
		while(rs.next()) {
			listado.add(rs.getString("TABLE_NAME")); //el 3 indica TABLE_NAME
		}
		
		return listado;
		
	}
	
	
	
	
	
	
	

}
