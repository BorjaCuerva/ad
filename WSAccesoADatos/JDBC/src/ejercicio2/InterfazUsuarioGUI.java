package ejercicio2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JComboBox;

public class InterfazUsuarioGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnListadoDeBbdd;
	private JButton btnListarTablas;
	private JButton btnDetallesTabla;
	
	static PersistenciaDB db = new PersistenciaDB();
	static BufferedReader bfr;
	static String ip = "";
	static String usuario = "";
	static String contrasenia = "";
	
	private JTextArea textArea;
	private JComboBox<String> comboBoxBBDD;
	private JComboBox comboBoxTablas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		/**
		 * Cargamos todos los datos y conectamos a la BBDD
		 */
		try {
			bfr = new BufferedReader(new FileReader(new File("CFG2.INI")));
			ip = bfr.readLine();
			usuario = bfr.readLine();
			contrasenia = bfr.readLine();
			bfr.close();
			
			//Conectamos a la BBDD
			db.conectar(ip, usuario, contrasenia);
		} catch (FileNotFoundException e1) {
			utilidades.Utilidades.notificarError(null, "File", e1, "No se ha encontrado el fichero");
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnListadoDeBbdd = new JButton("Listado de BBDD");
		btnListadoDeBbdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				listarBBDD();
				
			}
		});
		btnListadoDeBbdd.setMnemonic('l');
		btnListadoDeBbdd.setBounds(12, 12, 151, 25);
		contentPane.add(btnListadoDeBbdd);
		
		btnListarTablas = new JButton("Listar Tablas");
		btnListarTablas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listarTablas();
			}

			
		});
		btnListarTablas.setBounds(175, 12, 136, 25);
		contentPane.add(btnListarTablas);
		
		btnDetallesTabla = new JButton("Detalles Tabla");
		btnDetallesTabla.setBounds(323, 12, 151, 25);
		contentPane.add(btnDetallesTabla);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(12, 123, 574, 261);
		contentPane.add(textArea);
		
		comboBoxBBDD = new JComboBox<String>();
		comboBoxBBDD.setBounds(12, 61, 151, 24);
		contentPane.add(comboBoxBBDD);
		
		comboBoxTablas = new JComboBox();
		comboBoxTablas.setBounds(175, 61, 136, 24);
		contentPane.add(comboBoxTablas);
	}
	
	private void listarBBDD() {
		
		ArrayList<String> listaBBDD = new ArrayList<>();
		
		try {
			
			listaBBDD = db.listarBases();
			
			for (String bbdd : listaBBDD) {
				// Cargamos las BBDD en el comboBox
				comboBoxBBDD.addItem(bbdd.toString());
			}

		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(null, "BBDD", e, "Error al conectar a la BBDD");
			e.printStackTrace();
		}

	}
	
	
	private void listarTablas() {
		String base = (String) comboBoxBBDD.getSelectedItem();
		ArrayList<String>listadoTablas = new ArrayList<>();
		
		try {
			listadoTablas = db.listarTablas(base);
			comboBoxTablas.removeAllItems();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
