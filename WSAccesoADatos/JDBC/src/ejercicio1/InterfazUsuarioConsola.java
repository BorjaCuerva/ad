package ejercicio1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JOptionPane;

public class InterfazUsuarioConsola {

	public static void main(String[] args) {
		
		Persistencia bd = null;
		String tabla = null;
		String baseDatos = null;
		String ip = null;
		String usuario = null;
		String contrasenia = "";
		String tipoBD = null;

		//Para conectar necesitamos tipoBD, ip, nombre de la BD ,usuario , contrasenia
		try {
			BufferedReader bfr = new BufferedReader(new FileReader(new File("CFG.INI")));
			tabla = bfr.readLine();
			baseDatos = bfr.readLine();
			ip = bfr.readLine();
			usuario = bfr.readLine();
			contrasenia = bfr.readLine();
			tipoBD = bfr.readLine();
			bfr.close();
		} catch (FileNotFoundException e1) {
			System.out.println("No se encuentra CFG.INI");
			e1.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error al leer CFG.INI");
			e.printStackTrace();
		}
		
		//CONEXION A LA BBDD
		 try {
			//Connection conexion = DriverManager.getConnection("jdbc:"+tipoBD+"://"+ip+"/"+baseDatos,usuario,contrasenia);
			//En windows conectamos asi, el campo contrasenia tiene que estar vacio
			bd = new PersistenciaDB(tipoBD, ip, usuario, "", baseDatos);
			
			//En linux conectamos asi, tiene contrasenia
			//bd = new PersistenciaDB(tipoBD, ip, usuario, contrasenia, baseDatos);
			JOptionPane.showMessageDialog(null, "Conexion a la BBDD "+baseDatos+" realizada con exito."); //Mensaje informativo
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "no se pudo conectar a la BBDD "+baseDatos);
			e.printStackTrace();
		}
		 
		 //CONSULTA POR EMAIL
		 System.out.println("Introduce email"); //preguntamos email al usuario
		 Scanner sc = new Scanner(System.in);
		 String email = sc.next();
		 try {
			 //LLamamos al metodo consultarPersona que nos devuelve una persona
			 //le damos como parametro el nombre de la tabla y el email que es el filtro
			 Persona p = bd.consultarPersona(tabla, email);
			 //Mostramos por pantalla el menu
			 System.out.format("%1S%6S%10S%11S", "NOMBRE","CP","PAIS","EMAIL");
			 System.out.println(""); //Salto de linea
			 //Mostramos los datos de la persona
			 System.out.format("%1S%10S%10S%20S", p.getNombre(),p.getCP(),p.getPais(),p.getEmail());
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		 
		 
		 
		 
		 
		
	}

}
