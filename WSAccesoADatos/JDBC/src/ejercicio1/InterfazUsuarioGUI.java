package ejercicio1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.print.attribute.standard.PDLOverrideSupported;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class InterfazUsuarioGUI extends JFrame {

	private JPanel contentPane;
	private JButton jButtonConectarBD;
	private JButton jButtonSalirBD;
	private JButton jButtonGuardarPersona;
	private JButton jButtonEliminarPersona;
	private JButton jButtonConsultarPersona;
	private JButton jButtonSalir;
	private JTextArea textArea;
	
	Persistencia bd = null;
	String tabla = null;
	String baseDatos = null;
	String ip = null;
	String usuario = null;
	String contrasenia = "";
	String tipoBD = null;
	ArrayList<Persona> lista = new ArrayList<>();
	private JScrollPane scrollPane;
	private JButton jButtonListadoPersonas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 926, 559);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/**
		 * BOTON CONECTAR A LA BBDD
		 */
		jButtonConectarBD = new JButton("Conectar BBDD");
		jButtonConectarBD.setMnemonic('c');
		jButtonConectarBD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Para conectar necesitamos tipoBD, ip, nombre de la BD ,usuario , contrasenia
				try {
					BufferedReader bfr = new BufferedReader(new FileReader(new File("CFG.INI")));
					tabla = bfr.readLine();
					baseDatos = bfr.readLine();
					ip = bfr.readLine();
					usuario = bfr.readLine();
					contrasenia = bfr.readLine();
					tipoBD = bfr.readLine();
					bfr.close();
				} catch (FileNotFoundException e1) {
					System.out.println("No se encuentra CFG.INI");
					e1.printStackTrace();
				} catch (IOException ex) {
					System.out.println("Error al leer CFG.INI");
					ex.printStackTrace();
				}
				
				
				try {
					//Conectar a la BBDD mysql
					//bd = new PersistenciaDB(tipoBD, ip, usuario, contrasenia, baseDatos);
					
					//Conectar a la BBDD derby
					//bd = new PersistenciaDB(tipoBD, ip);
					bd =new PersistenciaDB(tipoBD,ip );
					activarBotones();
					
				} catch (Exception e1) {
					utilidades.Utilidades.notificarError(null, "Conexion BBDD", e1, "Error al conectar a la BBDD ");
					e1.printStackTrace();
				}
			}

			
		});
		jButtonConectarBD.setBounds(10, 468, 154, 23);
		contentPane.add(jButtonConectarBD);
		
		/**
		 * BOTON SALIR DE LA BD
		 */
		jButtonSalirBD = new JButton("Salir de la BBDD");
		jButtonSalirBD.setMnemonic('s');
		jButtonSalirBD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					bd.desconectar();
					estadoInicial();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			
		});
		jButtonSalirBD.setEnabled(false);
		jButtonSalirBD.setBounds(174, 468, 156, 23);
		contentPane.add(jButtonSalirBD);
		
		/**
		 * BOTON SALIR
		 */
		jButtonSalir = new JButton("Salir");
		jButtonSalir.setMnemonic('s');
		jButtonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		jButtonSalir.setBounds(823, 468, 89, 23);
		contentPane.add(jButtonSalir);
		
		/**
		 * BOTON GUARDAR PERSONA
		 */
		jButtonGuardarPersona = new JButton("Guardar persona");
		jButtonGuardarPersona.setMnemonic('g');
		jButtonGuardarPersona.setEnabled(false);
		jButtonGuardarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String tabla = JOptionPane.showInputDialog("Introduce tabla en la que quieres guardar.");
				String nombre = JOptionPane.showInputDialog("Introduce nombre para guardar en la BBDD");
				String cp = JOptionPane.showInputDialog("Introduce CP para guardar en la BBDD");
				String pais = JOptionPane.showInputDialog("Introduce pais para guardar en la BBDD");
				String email = JOptionPane.showInputDialog("Introduce email para guardar en la BBDD");;
				
				guardarPersona(tabla,nombre,cp,pais,email);
			}

		});
		jButtonGuardarPersona.setBounds(342, 468, 159, 23);
		contentPane.add(jButtonGuardarPersona);
		
		/**
		 * BOTON ELIMINAR PERSONA
		 */
		jButtonEliminarPersona = new JButton("Eliminar persona");
		jButtonEliminarPersona.setMnemonic('e');
		jButtonEliminarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String email = JOptionPane.showInputDialog("Introduce el correo electronico, para mostrar los datos de la persona");
				
				try {
					bd.borrarPersona(tabla, email);
				} catch (Exception e1) {
					utilidades.Utilidades.notificarError(null, "Eliminar persona", e1, "Error al eliminar la persona, el email "+email+" no existe.");
					e1.printStackTrace();
				}
				
			}
		});
		jButtonEliminarPersona.setEnabled(false);
		jButtonEliminarPersona.setBounds(513, 468, 173, 23);
		contentPane.add(jButtonEliminarPersona);
		
		/**
		 * CONSULTAR PERSONA
		 */
		jButtonConsultarPersona = new JButton("Consultar");
		jButtonConsultarPersona.setMnemonic('n');
		jButtonConsultarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Guardamos el email introducido por el usuario, con el que vamos a filtrar la consulta
				String email = JOptionPane.showInputDialog("Introduce el correo electronico, para mostrar los datos de la persona");
				try {
					 //LLamamos al metodo consultarPersona que nos devuelve una persona
					 //le damos como parametro el nombre de la tabla y el email que es el filtro
					 Persona p = bd.consultarPersona(tabla, email);
				    //Mostramos por pantalla el menu
					String consultaFormateada = String.format("%1S%6S%10S%11S", "NOMBRE","CP","PAIS","EMAIL");
					consultaFormateada += String.format("\n");
					consultaFormateada += String.format("%1S%10S%10S%20S", p.getNombre(),p.getCP(),p.getPais(),p.getEmail());
					textArea.setText(consultaFormateada);
					 
				} catch (Exception ex) {
					utilidades.Utilidades.notificarError(null, "Error SQL", ex, "el email "+email+" no existe!\n");
					ex.printStackTrace();
				}
				
			}
		});
		jButtonConsultarPersona.setEnabled(false);
		jButtonConsultarPersona.setBounds(692, 468, 110, 23);
		contentPane.add(jButtonConsultarPersona);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 890, 410);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEnabled(false);
		textArea.setEditable(false);
			
		/**
		 * BOTON LISTADO PERSONAS
		 */
		jButtonListadoPersonas = new JButton("Listado personas");
		jButtonListadoPersonas.setEnabled(false);
		jButtonListadoPersonas.setMnemonic('l');
		jButtonListadoPersonas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Preguntamos al usuario como quiere ordenar la tabla.
				String orderBy = JOptionPane.showInputDialog("Como quieres ordenar el listado? [nombre] [cp] [pais] [email]");
				mostrarListado(orderBy);

			}

			
		});
		jButtonListadoPersonas.setBounds(648, 503, 173, 25);
		contentPane.add(jButtonListadoPersonas);
	}
	
	/**
	 * Activamos los botones
	 */
	private void activarBotones() {

		jButtonConectarBD.setEnabled(false);
		jButtonSalirBD.setEnabled(true);
		jButtonGuardarPersona.setEnabled(true);
		jButtonEliminarPersona.setEnabled(true);
		jButtonConsultarPersona.setEnabled(true);
		jButtonListadoPersonas.setEnabled(true);
	
	}
	
	/**
	 * Estado inicial de la aplicacion
	 */
	private void estadoInicial() {

		jButtonConectarBD.setEnabled(true);
		jButtonSalirBD.setEnabled(false);
		jButtonGuardarPersona.setEnabled(false);
		jButtonEliminarPersona.setEnabled(false);
		jButtonConsultarPersona.setEnabled(false);
		jButtonListadoPersonas.setEnabled(false);
		
	}
	
	/**
	 * Metodo para mostrarListado de todas las personas
	 * ordenado como quiera el usuario
	 * @param orderBy --> Como ordenar la tabla
	 */
	private void mostrarListado(String orderBy) {
		textArea.setText("");
		
		lista = bd.listadoPersonas(tabla, orderBy);
		
		textArea.setText(String.format("%1S%12S%22S%22S", "NOMBRE","CP","PAIS","EMAIL"+"\n"));
		for (Persona persona : lista) {
			textArea.setText(textArea.getText() + persona.toString() + "\n");
		
	}
		}
	
	/**
	 * Guardamos una persona en la BBDD. Los parametros los introduce el usuario.
	 * @param tabla --> Nombre de la tabla
	 * @param nombre --> Nombre de la persona
	 * @param cp --> Codigo postal
	 * @param pais --> Pais
	 * @param email --> email de la persona
	 */
	private void guardarPersona(String tabla,String nombre, String cp, String pais, String email) {
		
		Persona p = new Persona(nombre, cp, pais, email);
		try {
			bd.guardarPersona(tabla, p);
			
		} catch (Exception e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "No se ha podido guardar la persona "+p.getNombre());
			e.printStackTrace();
		}
		
		
	}
	
	
}
