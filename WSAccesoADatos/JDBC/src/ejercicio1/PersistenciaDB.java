package ejercicio1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class PersistenciaDB implements Persistencia {

	String tipoDB; // Tipo de la BBDD, esta en CFG.INI
	Connection con; // Objeto conexion

	/**
	 * Constructor para conectar a la BBDD mysql
	 * 
	 * @param tipoBD
	 *            --> Tipo de BBDD a la que conectar (mysql, derby..)
	 * @param IP
	 *            --> Ip mysql localhost // derby la ruta de la carpeta
	 * @param usu
	 *            --> Usuario
	 * @param pass
	 *            --> Contraseña
	 * @param bd
	 *            --> Nombre de la base de datos
	 * @throws Exception
	 */
	public PersistenciaDB(String tipoBD, String IP, String usu, String pass, String bd) throws Exception {

		try {

			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "no se encuentra el driver.\n");
			e.printStackTrace();
		}
		// Conectamos a la BBDD mysql
		con = DriverManager.getConnection("jdbc:" + tipoBD + "://" + IP + "/" + bd, usu, pass);

	}

	/**
	 * Conexion a la BBDD derby. Para que funcione no puede estar abierto derby en
	 * ningun otro sitio como por ejemplo DBeaver.
	 * 
	 * @param tipoBD
	 * @param IP
	 * @throws SQLException
	 */
	public PersistenciaDB(String tipoBD, String IP) throws SQLException {

		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");

		} catch (ClassNotFoundException e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "no se encuentra el driver.\n");
			e.printStackTrace();
		}
		// Conectamos a la BBDD derby
		con = DriverManager.getConnection("jdbc:" + tipoBD + ":" + IP);

	}

	/**
	 * Metodo para desconectar de la BBDD Informa al usuario con un JOptionPane
	 */
	@Override
	public void desconectar() throws Exception {
		con.close();
		JOptionPane.showMessageDialog(null, "Desconectando de la BBDD");

	}

	/**
	 * Listado de todas las personas de la BBDD, guardada en un ArrayList
	 */
	@Override
	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) {

		String sql = "SELECT * FROM " + tabla + " ORDER BY " + orderBy;
		ArrayList<Persona> lista = new ArrayList<>();

		try {
			// Preparamos la consulta
			Statement preparedStatement = con.createStatement();
			ResultSet rs;
			rs = preparedStatement.executeQuery(sql);
			// Recorremos la BBDD
			while (rs.next()) {
				// Aniadimos al ArrayList las personas
				lista.add(new Persona(rs.getString("NOMBRE"), rs.getString("CP"), rs.getString("PAIS"),
						rs.getString("EMAIL")));
			}
			// Cerramos Streams
			rs.close();
			preparedStatement.close();
			return lista;

		} catch (SQLException e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "La columna " + orderBy + " no existe!\n");
			e.printStackTrace();
		}
		return lista;

	}

	/**
	 * Guardar persona --> Si el email no existe Modificar persona --> Si el email
	 * existe
	 */
	@Override
	public void guardarPersona(String tabla, Persona p) throws Exception {

		Persona persona = consultarPersona(tabla, p.getEmail());

		// Si no existe la persona, la creamos
		if (persona == null) {
			String sqlInsert = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";
			PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
			preparedStatement.setString(1, p.getNombre());
			preparedStatement.setString(2, p.getCP());
			preparedStatement.setString(3, p.getPais());
			preparedStatement.setString(4, p.getEmail());
			preparedStatement.executeUpdate();
			preparedStatement.close();

			// Si la persona existe, la modificamos
		} else {
			String sql = "UPDATE " + tabla + " SET nombre=?, CP=?, pais=? WHERE email=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getCP());
			ps.setString(3, p.getPais());
			ps.setString(4, p.getEmail());
			ps.executeUpdate();
			ps.close();
		}

	}

	/**
	 * Metodo que borra una persona
	 */
	@Override
	public void borrarPersona(String tabla, String email) throws Exception {
		// Creamos el objeto persona
		Persona persona = consultarPersona(tabla, email);
		if (persona != null) {
			String sql = "DELETE FROM " + tabla + " WHERE email= ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.executeUpdate();
			ps.close();
		} else {
			utilidades.Utilidades.notificarError(null, "Eliminar persona", null,
					"La persona no existe o el email " + email + " es incorrecto.");
		}

	}

	/**
	 * Consultamos una unica persona por el email
	 */
	@Override
	public Persona consultarPersona(String tabla, String email) throws Exception {

		String nombre;
		String cp;
		String pais;
		Persona p1 = null;
		// Cambiar el persona por tabla
		String sqlBusqueda = "SELECT * FROM " + tabla + " WHERE EMAIL=" + "'" + email + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo persona todos los datos
		while (rs.next()) {
			nombre = rs.getString("nombre");
			cp = rs.getString("CP");
			pais = rs.getString("pais");
			p1 = new Persona(nombre, cp, pais, email);

		}

		return p1;
	}

}
